<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);

	return $exception;
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';



// DEV GLOBALS
function generateAvatar($size)
{
	$json = file_get_contents('http://uifaces.com/api/v1/random');
	$obj = json_decode($json)->image_urls;

	if ($size == 'epic')
	{
		return $obj->epic;
	}
	else if ($size == 'bigger')
	{
		return $obj->bigger;
	}
	else if ($size == 'mini')
	{
		return $obj->mini;
	}
	else {
		return $obj->normal;
	}
}

function image_name()
{
	$num = rand(0, 51);

	if ($num == 0)
	{
		return '01';
	}
	else if ($num < 10)
	{
		return '0'.$num;
	}
	else
	{
		return strval($num);
	}
}

function parameterize($string)
{
	$lowercase_string = strtolower($string);
	$trimmed_string = trim($lowercase_string);
	$replaced_string = str_replace(' ', '-', $trimmed_string);
	return preg_replace('/[^\w-]/', '', $replaced_string);
}

function humanize($string)
{
	$splitted_string = explode('_', $string);
	return ucwords(implode(' ', $splitted_string));
}
