<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});

// Services
Route::get('services', function()
{
	return View::make('services');
});
Route::get('services-drilldown', function()
{
	return View::make('services-drilldown');
});

// Shop
Route::get('shop', function()
{
	return View::make('shop');
});
Route::get('shop-drilldown', function()
{
	return View::make('shop-drilldown');
});

// Articles
Route::get('articles', function()
{
	return View::make('articles');
});
Route::get('articles-drilldown', function()
{
	return View::make('articles-drilldown');
});

// Conversations
Route::get('conversations', function()
{
	return View::make('conversations');
});
Route::get('conversations-drilldown', function()
{
	return View::make('conversations-drilldown');
});

// User Dashboard
Route::get('user-dashboard', function()
{
	return View::make('user-dashboard');
});
Route::get('user-dashboard-services', function()
{
	return View::make('user-dashboard-services');
});
Route::get('user-dashboard-orders', function()
{
	return View::make('user-dashboard-orders');
});
Route::get('user-dashboard-purchases', function()
{
	return View::make('user-dashboard-purchases');
});

Route::get('user-dashboard-inbox', function()
{
	return View::make('user-dashboard-inbox');
});
Route::get('user-dashboard-thread', function()
{
	return View::make('user-dashboard-thread');
});

Route::get('user-dashboard-listings', function()
{
	return View::make('user-dashboard-listings');
});
Route::get('user-dashboard-listings-add', function()
{
	return View::make('user-dashboard-listings-add');
});
Route::get('user-dashboard-listings-drilldown', function()
{
	return View::make('user-dashboard-listings-drilldown');
});
Route::get('user-dashboard-listings-drilldown-photos', function()
{
	return View::make('user-dashboard-listings-drilldown-photos');
});
Route::get('user-dashboard-listings-drilldown-products', function()
{
	return View::make('user-dashboard-listings-drilldown-products');
});

Route::get('user-dashboard-albums', function()
{
	return View::make('user-dashboard-albums');
});
Route::get('user-dashboard-albums-drilldown', function()
{
	return View::make('user-dashboard-albums-drilldown');
});
Route::get('user-dashboard-albums-drilldown-list', function()
{
	return View::make('user-dashboard-albums-drilldown-list');
});
Route::get('user-dashboard-favorites', function()
{
	return View::make('user-dashboard-favorites');
});
Route::get('user-dashboard-favorites-drilldown', function()
{
	return View::make('user-dashboard-favorites-drilldown');
});
Route::get('user-dashboard-favorites-articles', function()
{
	return View::make('user-dashboard-favorites-articles');
});
Route::get('user-dashboard-favorites-people', function()
{
	return View::make('user-dashboard-favorites-people');
});

Route::get('user-dashboard-storefront', function()
{
	return View::make('user-dashboard-storefront');
});
Route::get('user-dashboard-storefront-about', function()
{
	return View::make('user-dashboard-storefront-about');
});
Route::get('user-dashboard-storefront-policies', function()
{
	return View::make('user-dashboard-storefront-policies');
});

Route::get('user-dashboard-profile', function()
{
	return View::make('user-dashboard-profile');
});
Route::get('user-dashboard-reviews', function()
{
	return View::make('user-dashboard-reviews');
});
Route::get('user-dashboard-profile-trust', function()
{
	return View::make('user-dashboard-profile-trust');
});

// User profile
Route::get('user-profile', function()
{
	return View::make('user-profile');
});
Route::get('user-profile-albums', function()
{
	return View::make('user-profile-albums');
});
Route::get('user-profile-favorite', function()
{
	return View::make('user-profile-favorite');
});

// Seller page
Route::get('seller-page', function()
{
	return View::make('seller-page');
});
Route::get('seller-page-about', function()
{
	return View::make('seller-page-about');
});
Route::get('seller-page-policies', function()
{
	return View::make('seller-page-policies');
});
Route::get('seller-page-reviews', function()
{
	return View::make('seller-page-reviews');
});
Route::get('seller-page-suggestions', function()
{
	return View::make('seller-page-suggestions');
});
