@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="profile-nav">
          <section>
            <ul>
              <li>
                <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
              </li>
              <li>
                <a href="/user-dashboard">Dashboard</a>
              </li>
              <li>
                <a href="/user-dashboard-inbox">Inbox</a>
              </li>
              <li>
                <a href="/user-dashboard-listings">Listings</a>
              </li>
              <li>
                <a class="active" href="/user-dashboard-albums">Albums</a>
              </li>
              <li>
                <a href="/user-dashboard-storefront">Storefront</a>
              </li>
              <li>
                <a href="/user-dashboard-profile">Profile</a>
              </li>
            </ul>
          </section>
        </div>
        <div class="fixed-nav">
          <div id="seller-nav-unfixed">
          </div>
          <div id="seller-nav">
          </div>
        </div>
        <div class="drilldown-full" style="padding-top:4em;">
          <h4 style="text-align:center;">
            My Favorites
          </h4>
        </div>
        <div class="drilldown-full">
          <section class="main">
            <div class="grid-3" data-columns="">
              <div class="card">
                <a href="/user-dashboard-favorites-drilldown">
                  <div class="card-image card-large card-favorites" style="background-image:url('/images/photos/prev12.jpg');"></div>
                </a>
                <div class="card-images">
                  <div class="card-image" style="background-image:url('/images/photos/prev15.jpg');"></div>
                  <div class="card-image" style="background-image:url('/images/photos/prev19.jpg');"></div>
                  <div class="card-image" style="background-image:url('/images/photos/prev44.jpg');">
                    <div class="card-images-overlay">
                      <h5>
                        5
                      </h5>
                      <p>
                        items
                      </p>
                    </div>
                  </div>
                </div>
                <div class="card-details">
                  <div class="user" style="padding:0.5em 0.25em;width:100%;">
                    <div class="user-info">
                      <a class="name" href="/user-dashboard-favorites-drilldown"><span>Services</span><i class="icon ion-ios-arrow-right pull-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-favorites-drilldown">
                  <div class="card-image card-large card-favorites" style="background-image:url('/images/products/prev14.jpg');"></div>
                </a>
                <div class="card-images">
                  <div class="card-image" style="background-image:url('/images/products/prev16.jpg');"></div>
                  <div class="card-image" style="background-image:url('/images/products/prev28.jpg');"></div>
                  <div class="card-image" style="background-image:url('/images/products/prev33.jpg');">
                    <div class="card-images-overlay">
                      <h5>
                        19
                      </h5>
                      <p>
                        items
                      </p>
                    </div>
                  </div>
                </div>
                <div class="card-details">
                  <div class="user" style="padding:0.5em 0.25em;width:100%;">
                    <div class="user-info">
                      <a class="name" href="/user-dashboard-favorites-drilldown"><span>Items</span><i class="icon ion-ios-arrow-right pull-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-favorites-articles">
                  <div class="card-image card-large card-favorites" style="background-image:url('/images/photos/prev48.jpg');"></div>
                </a>
                <div class="card-images">
                  <div class="card-image" style="background-image:url('/images/photos/prev26.jpg');"></div>
                  <div class="card-image" style="background-image:url('/images/photos/prev41.jpg');"></div>
                  <div class="card-image" style="background-image:url('/images/photos/prev19.jpg');">
                    <div class="card-images-overlay">
                      <h5>
                        20
                      </h5>
                      <p>
                        items
                      </p>
                    </div>
                  </div>
                </div>
                <div class="card-details">
                  <div class="user" style="padding:0.5em 0.25em;width:100%;">
                    <div class="user-info">
                      <a class="name" href="/user-dashboard-favorites-articles"><span>Articles</span><i class="icon ion-ios-arrow-right pull-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-favorites-people">
                  <div class="card-image card-large card-favorites" style="background-image:url('https://res.cloudinary.com/relay-foods/image/upload/v1403548282/naturespath_lq26v4.jpg');"></div>
                </a>
                <div class="card-images">
                  <div class="card-image" style="background-image:url('https://res.cloudinary.com/relay-foods/image/upload/v1403548255/honest_uw37mr.jpg');"></div>
                  <div class="card-image" style="background-image:url('https://res.cloudinary.com/relay-foods/image/upload/v1403548282/naturespath_lq26v4.jpg');"></div>
                  <div class="card-image" style="background-image:url('http://static.bfads.net/stores/logo/Lowes_square_large.png');">
                    <div class="card-images-overlay">
                      <h5>
                        7
                      </h5>
                      <p>
                        items
                      </p>
                    </div>
                  </div>
                </div>
                <div class="card-details">
                  <div class="user" style="padding:0.5em 0.25em;width:100%;">
                    <div class="user-info">
                      <a class="name" href="/user-dashboard-favorites-people"><span>PROs</span><i class="icon ion-ios-arrow-right pull-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-favorites-people">
                  <div class="card-image card-large card-favorites" style="background-image:url('http://vector-magz.com/wp-content/uploads/2013/07/ace-hardware-logo-vector.png');"></div>
                </a>
                <div class="card-images">
                  <div class="card-image" style="background-image:url('http://static.bfads.net/stores/logo/Lowes_square_large.png');"></div>
                  <div class="card-image" style="background-image:url('https://res.cloudinary.com/relay-foods/image/upload/v1403548255/honest_uw37mr.jpg');"></div>
                  <div class="card-image" style="background-image:url('https://res.cloudinary.com/relay-foods/image/upload/v1403548282/naturespath_lq26v4.jpg');">
                    <div class="card-images-overlay">
                      <h5>
                        6
                      </h5>
                      <p>
                        items
                      </p>
                    </div>
                  </div>
                </div>
                <div class="card-details">
                  <div class="user" style="padding:0.5em 0.25em;width:100%;">
                    <div class="user-info">
                      <a class="name" href="/user-dashboard-favorites-people"><span>Sellers</span><i class="icon ion-ios-arrow-right pull-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </section>
    </div>
  </div>
@stop
