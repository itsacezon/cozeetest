@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="profile-nav">
          <section>
            <ul>
              <li>
                <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
              </li>
              <li>
                <a href="/user-dashboard">Dashboard</a>
              </li>
              <li>
                <a href="/user-dashboard-inbox">Inbox</a>
              </li>
              <li>
                <a href="/user-dashboard-listings">Listings</a>
              </li>
              <li>
                <a class="active" href="/user-dashboard-albums">Albums</a>
              </li>
              <li>
                <a href="/user-dashboard-storefront">Storefront</a>
              </li>
              <li>
                <a href="/user-dashboard-profile">Profile</a>
              </li>
            </ul>
          </section>
        </div>
        <div class="fixed-nav">
          <div id="seller-nav-unfixed">
          </div>
          <div id="seller-nav">
            <div class="seller-section simple slide-up">
              <div class="user">
                <div class="user-thumb smaller">
                  <img src="/images/forenheit.jpg" />
                </div>
                <div class="user-info">
                  <ul>
                    <li>
                      <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                    </li>
                    <li>
                      <a class="location" href="/edit-profile">Edit profile</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="drilldown-full" style="text-align:center;">
            <h4>
              My Album Name
            </h4>
            <a class="button pewter pull-right" href="/user-dashboard-albums-drilldown-list" style="margin-top:-1px;"><i class="icon ion-android-menu"></i></a><a class="button light pull-right" href="/user-dashboard-albums-drilldown" style="margin-right:0.5em;"><i class="icon ion-grid"></i></a><button class="light pull-left" type="button"><span>Most Popular</span><i class="icon ion-ios-arrow-down"></i></button>
          </div>
          <div class="drilldown-full">
            <section class="main">
              <div class="cards">
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/products/prev20.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header">
                      <span class="cut-half" style="margin-bottom:0;">Est molestiae nihil sed id</span><span class="price pull-right">₱ 13446</span>
                    </div>
                    <a class="card-author" href="#">Kaden Norman</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/products/prev28.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header">
                      <span class="cut-half" style="margin-bottom:0;">Quo harum officiis harum illo</span><span class="price pull-right">₱ 12595</span>
                    </div>
                    <a class="card-author" href="#">Anahi Griffin</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/photos/prev32.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header full" style="margin-bottom:0;">
                      Expedita magnam dolor et hic
                    </div>
                    <a class="card-author" href="#">Sheryl Vick</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/photos/prev01.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header full" style="margin-bottom:0;">
                      Perspiciatis in cupiditate magni rem
                    </div>
                    <a class="card-author" href="#">Tricia Stout</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/photos/prev02.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header full" style="margin-bottom:0;">
                      Accusantium fugit consequatur at explicabo
                    </div>
                    <a class="card-author" href="#">Angela James</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/photos/prev28.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header full" style="margin-bottom:0;">
                      Modi inventore iusto molestiae eligendi
                    </div>
                    <a class="card-author" href="#">Dwayne Sutton</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/products/prev22.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header">
                      <span class="cut-half" style="margin-bottom:0;">Dolorem consequuntur quis ut sequi</span><span class="price pull-right">₱ 6156</span>
                    </div>
                    <a class="card-author" href="#">Allen Johnston</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/products/prev39.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header">
                      <span class="cut-half" style="margin-bottom:0;">Ipsa ut corrupti in unde</span><span class="price pull-right">₱ 8861</span>
                    </div>
                    <a class="card-author" href="#">Noelle Lucas</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/photos/prev19.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header full" style="margin-bottom:0;">
                      Quis ut quo ullam impedit
                    </div>
                    <a class="card-author" href="#">Ida Palmer</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/products/prev28.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header">
                      <span class="cut-half" style="margin-bottom:0;">Id magnam consequuntur quaerat deleniti</span><span class="price pull-right">₱ 6503</span>
                    </div>
                    <a class="card-author" href="#">Bart Case</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/photos/prev38.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header full" style="margin-bottom:0;">
                      Sit sit maiores maiores nisi
                    </div>
                    <a class="card-author" href="#">Sydney Zhang</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/products/prev05.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header">
                      <span class="cut-half" style="margin-bottom:0;">Voluptatibus consectetur autem cumque pariatur</span><span class="price pull-right">₱ 1903</span>
                    </div>
                    <a class="card-author" href="#">Judith Justice</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/photos/prev20.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header full" style="margin-bottom:0;">
                      Odit explicabo et qui dolor
                    </div>
                    <a class="card-author" href="#">Constance Jennings</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/photos/prev18.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header full" style="margin-bottom:0;">
                      Optio possimus sapiente suscipit assumenda
                    </div>
                    <a class="card-author" href="#">Peggy Hinton</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/photos/prev20.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header full" style="margin-bottom:0;">
                      Aut id eos et sint
                    </div>
                    <a class="card-author" href="#">Kendrick Nichols</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card card-full">
                  <a class="card-image card-small" href="/shop-drilldown" style="background-image:url('/images/products/prev23.jpg');"></a>
                  <div class="card-details" style="padding-left:0;">
                    <div class="card-header">
                      <span class="cut-half" style="margin-bottom:0;">Et in ut consequuntur et</span><span class="price pull-right">₱ 507</span>
                    </div>
                    <a class="card-author" href="#">Eoin Carver</a>
                    <div class="card-copy" style="padding-top:0.75em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="2" style="font-size:14px;" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
