@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="profile-nav">
          <section>
            <ul>
              <li>
                <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
              </li>
              <li>
                <a href="/user-dashboard">Dashboard</a>
              </li>
              <li>
                <a href="/user-dashboard-inbox">Inbox</a>
              </li>
              <li>
                <a href="/user-dashboard-listings">Listings</a>
              </li>
              <li>
                <a class="active" href="/user-dashboard-albums">Albums</a>
              </li>
              <li>
                <a href="/user-dashboard-storefront">Storefront</a>
              </li>
              <li>
                <a href="/user-dashboard-profile">Profile</a>
              </li>
            </ul>
          </section>
        </div>
        <div class="fixed-nav">
          <div id="seller-nav-unfixed">
          </div>
          <div id="seller-nav">
          </div>
        </div>
        <div class="drilldown-full" style="padding-top:4em;">
          <section class="main no-top">
            <div class="grid-4" data-columns="">
              <div class="card">
                <div class="card-add-album">
                  <i class="icon ion-ios-plus-outline"></i>
                  <h5>
                    Create album
                  </h5>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-favorites">
                  <div class="card-image" style="background-image:url('/images/photos/prev41.jpg');">
                    <button class="pewter" style="position:absolute;right:1em;top:1em;" type="button">32 items</button>
                  </div>
                </a>
                <div class="card-details">
                  <section style="padding-left:0;">
                    <div class="card-header">
                      <a href="/user-dashboard-favorites">Favorites</a>
                    </div>
                  </section>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-albums-drilldown">
                  <div class="card-image" style="background-image:url('/images/photos/prev27.jpg');">
                    <button class="pewter" style="position:absolute;right:1em;top:1em;" type="button">46 items</button>
                  </div>
                </a>
                <div class="card-details">
                  <section style="padding-left:0;">
                    <div class="card-header">
                      <a href="/user-dashboard-albums-drilldown">Aut non animi a a</a>
                    </div>
                  </section>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-albums-drilldown">
                  <div class="card-image" style="background-image:url('/images/photos/prev45.jpg');">
                    <button class="pewter" style="position:absolute;right:1em;top:1em;" type="button">27 items</button>
                  </div>
                </a>
                <div class="card-details">
                  <section style="padding-left:0;">
                    <div class="card-header">
                      <a href="/user-dashboard-albums-drilldown">Qui dolores magnam voluptate qui</a>
                    </div>
                  </section>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-albums-drilldown">
                  <div class="card-image" style="background-image:url('/images/photos/prev43.jpg');">
                    <button class="pewter" style="position:absolute;right:1em;top:1em;" type="button">17 items</button>
                  </div>
                </a>
                <div class="card-details">
                  <section style="padding-left:0;">
                    <div class="card-header">
                      <a href="/user-dashboard-albums-drilldown">Nihil laudantium deserunt sed ad</a>
                    </div>
                  </section>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-albums-drilldown">
                  <div class="card-image" style="background-image:url('/images/photos/prev14.jpg');">
                    <button class="pewter" style="position:absolute;right:1em;top:1em;" type="button">12 items</button>
                  </div>
                </a>
                <div class="card-details">
                  <section style="padding-left:0;">
                    <div class="card-header">
                      <a href="/user-dashboard-albums-drilldown">Inventore nobis qui quam nisi</a>
                    </div>
                  </section>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-albums-drilldown">
                  <div class="card-image" style="background-image:url('/images/photos/prev38.jpg');">
                    <button class="pewter" style="position:absolute;right:1em;top:1em;" type="button">18 items</button>
                  </div>
                </a>
                <div class="card-details">
                  <section style="padding-left:0;">
                    <div class="card-header">
                      <a href="/user-dashboard-albums-drilldown">Rerum eos quas doloribus quia</a>
                    </div>
                  </section>
                </div>
              </div>
              <div class="card">
                <a href="/user-dashboard-albums-drilldown">
                  <div class="card-image" style="background-image:url('/images/photos/prev06.jpg');">
                    <button class="pewter" style="position:absolute;right:1em;top:1em;" type="button">33 items</button>
                  </div>
                </a>
                <div class="card-details">
                  <section style="padding-left:0;">
                    <div class="card-header">
                      <a href="/user-dashboard-albums-drilldown">Esse aut laboriosam accusantium pariatur</a>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          </section>
        </div>
      </section>
    </div>
  </div>
@stop
