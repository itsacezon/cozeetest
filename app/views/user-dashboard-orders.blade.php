@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Your dashboard</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-services">Services</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-orders">Orders</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-purchases">Purchases<span class="number">41</span></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <div class="drilldown-full">
              <section class="main no-top">
                <h5 style="margin-bottom:1em;">
                  Summary of Orders
                </h5>
                <button class="call-to-action pull-right" style="margin-bottom:1em;" type="button">Download a PDF summary</button><button class="light" style="margin-bottom:1em;" type="button"><span style="margin-right:0;font-weight:600;">View by</span> <span style="font-weight:500;">Transaction</span><i class="icon ion-ios-arrow-down"></i></button>
                <table class="ui table" style="margin-bottom:2em;font-size:14px;">
                  <thead>
                    <tr>
                      <th style="font-weight:600;">
                        Order ID
                      </th>
                      <th style="font-weight:600;">
                        Status
                      </th>
                      <th style="font-weight:600;">
                        Buyer
                      </th>
                      <th style="font-weight:600;">
                        Quantity
                      </th>
                      <th style="font-weight:600;">
                        Date
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        E61A7989201B3
                      </td>
                      <td><button class="light" style="color:red;">Returned</button>
                      </td>
                      <td>
                        Lorenzo Bowling
                      </td>
                      <td>
                        8
                      </td>
                      <td>
                        Sat Jan 26, 1991
                      </td>
                    </tr>
                    <tr>
                      <td>
                        5F6D5D1356033
                      </td>
                      <td><button class="light" style="color:red;">Returned</button>
                      </td>
                      <td>
                        Maggie Adkins
                      </td>
                      <td>
                        11
                      </td>
                      <td>
                        Fri May 08, 2009
                      </td>
                    </tr>
                    <tr>
                      <td>
                        78B3E04D22BD9
                      </td>
                      <td><button class="light" style="color:red;">Returned</button>
                      </td>
                      <td>
                        Trisha Christensen
                      </td>
                      <td>
                        39
                      </td>
                      <td>
                        Wed Nov 17, 2004
                      </td>
                    </tr>
                    <tr>
                      <td>
                        5687DAB51AAEB
                      </td>
                      <td><button class="light" style="color:red;">Returned</button>
                      </td>
                      <td>
                        Alvin Gallagher
                      </td>
                      <td>
                        14
                      </td>
                      <td>
                        Sun Apr 06, 2008
                      </td>
                    </tr>
                    <tr>
                      <td>
                        1E1335632F064
                      </td>
                      <td>
                        <button class="light" style="color:green;">Delivered</button>
                      </td>
                      <td>
                        Janet Fischer
                      </td>
                      <td>
                        1
                      </td>
                      <td>
                        Tue Jul 01, 2003
                      </td>
                    </tr>
                  </tbody>
                </table>
              </section>
              <section class="main">
                <h5 style="margin-bottom:1em;">
                  Recently sold
                </h5>
                <div class="grid-4" data-columns="">
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-3.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        <span class="cut-half">Labore illum nisi corporis et</span><span class="price pull-right">₱ 6879</span>
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Randal Hill</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-12.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        <span class="cut-half">Sunt nulla natus repellendus aperiam</span><span class="price pull-right">₱ 1933</span>
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Kerry Heath</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-2.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        <span class="cut-half">Hic aspernatur quos vel aspernatur</span><span class="price pull-right">₱ 7453</span>
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Hector Allen</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-2.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        <span class="cut-half">Reiciendis voluptate eveniet et consequatur</span><span class="price pull-right">₱ 8358</span>
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Allyson Moore</a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
