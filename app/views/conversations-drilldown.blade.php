@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section>
        <div class="drilldown-full">
          <div class="drilldown-breadcrumbs" style="margin-bottom:1.5em;">
            <ul>
              <li>
                <a href="/conversations">Conversations</a>
              </li>
              <li>
                <a href="/conversations-category">Advice</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="drilldown-full">
          <div class="drilldown-misc">
            <div class="drilldown-card">
              <div class="drilldown-content">
                <div class="drilldown-gallery">
                  <ul id="image-gallery">
                    <li class="image" data-thumb="/images/photos/prev37.jpg" style="background-image:url(/images/photos/prev37.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev39.jpg" style="background-image:url(/images/photos/prev39.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev11.jpg" style="background-image:url(/images/photos/prev11.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev18.jpg" style="background-image:url(/images/photos/prev18.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev09.jpg" style="background-image:url(/images/photos/prev09.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev26.jpg" style="background-image:url(/images/photos/prev26.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev45.jpg" style="background-image:url(/images/photos/prev45.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev06.jpg" style="background-image:url(/images/photos/prev06.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev08.jpg" style="background-image:url(/images/photos/prev08.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev15.jpg" style="background-image:url(/images/photos/prev15.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev17.jpg" style="background-image:url(/images/photos/prev17.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev09.jpg" style="background-image:url(/images/photos/prev09.jpg);"></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="drilldown-misc-2">
            <div class="drilldown-card">
              <div class="drilldown-content">
                <section>
                  <h5>
                    Need more help with frustrating small bedroom.
                  </h5>
                  <p>
                    I'm just wondering if I should paint over my brick wall to the cream colour and make the green wall my feature wall. I painted it so long ago and it just looks silly with two different types of walls. I ws also wondering if by painting the brick wall cream it'd make the room look bigger than it is. I've got a lot of other designing things to do in the room but figured I'd start with the wall. To paint the brick or leave it?
                  </p>
                </section>
              </div>
              <div class="drilldown-content">
                <section>
                  <span class="heading">Asked by</span>
                  <div class="user">
                    <div class="user-thumb">
                      <img src="http://uifaces.com/faces/_twitter/kirkouimet_120.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="/seller-page">Brock Baker</a>
                      <p class="location">
                        Makati, PH
                      </p>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="panel panel-light">
      <section>
        <div class="drilldown-full">
          <div class="drilldown-main">
            <h4 style="margin-bottom:1em;">
              10 <span>answers</span>
            </h4>
            <div class="drilldown-full">
              <div class="comments">
                <div class="comment">
                  <div class="comment-image">
                    <img src="http://uifaces.com/faces/_twitter/OskarLevinson_120.jpg" />
                  </div>
                  <div class="comment-content">
                    <h2>
                      Don Bowman
                    </h2>
                    <p class="comment-detail">
                      Thu Nov 11, 1993
                    </p>
                    <p>
                      Enim nisi ipsum voluptatem qui mollitia odio voluptas. suscipit quia omnis et sed eligendi accusamus. sapiente ea et non sint at doloribus vero corporis et distinctio voluptas
                    </p>
                  </div>
                </div>
                <div class="comment">
                  <div class="comment-image">
                    <img src="http://uifaces.com/faces/_twitter/iamjamie_120.jpg" />
                  </div>
                  <div class="comment-content">
                    <h2>
                      Pete James
                    </h2>
                    <p class="comment-detail">
                      Tue Jan 18, 2000
                    </p>
                    <p>
                      Autem voluptates dolore aut placeat quo numquam natus rerum unde libero autem optio quaerat. voluptatum atque est placeat velit quaerat repudiandae voluptatibus molestiae eos et quam amet soluta et. aut animi nisi molestiae est explicabo nostrum. itaque autem ab dignissimos sapiente nemo laudantium fugit itaque possimus quo dolores. sint animi repudiandae perferendis dolorum excepturi in qui illo placeat cum aut ut
                    </p>
                  </div>
                </div>
                <div class="comment">
                  <div class="comment-image">
                    <img src="http://uifaces.com/faces/_twitter/nckjrvs_120.jpg" />
                  </div>
                  <div class="comment-content">
                    <h2>
                      Nia Cooper
                    </h2>
                    <p class="comment-detail">
                      Sat Jun 14, 2008
                    </p>
                    <p>
                      Ut ut ipsa reiciendis rem ipsa et velit asperiores cupiditate voluptas vel. illo consequatur eligendi qui assumenda reprehenderit dolor voluptatem ad. non at nulla est facilis tempore explicabo voluptas et ut facilis qui nobis. saepe iusto officia et. eius aspernatur deleniti asperiores nam aut voluptatem blanditiis. sit possimus ad molestias officia sapiente. voluptatem enim et amet quae in natus deleniti
                    </p>
                  </div>
                </div>
                <div class="comment">
                  <div class="comment-image">
                    <img src="http://uifaces.com/faces/_twitter/NastyaVZ_120.jpg" />
                  </div>
                  <div class="comment-content">
                    <h2>
                      Hamish Moon
                    </h2>
                    <p class="comment-detail">
                      Mon Nov 05, 2001
                    </p>
                    <p>
                      Veniam laudantium provident ea voluptate et a ut blanditiis. veritatis ut sequi praesentium reprehenderit nesciunt. laborum aut nulla blanditiis sapiente. aut nulla ea deleniti assumenda voluptas rerum dolorem illum ipsum aut et ducimus temporibus
                    </p>
                  </div>
                </div>
                <div class="comment">
                  <div class="comment-image">
                    <img src="http://uifaces.com/faces/_twitter/adn_120.jpg" />
                  </div>
                  <div class="comment-content">
                    <h2>
                      Kellie Neal
                    </h2>
                    <p class="comment-detail">
                      Sun Apr 18, 1999
                    </p>
                    <p>
                      Accusamus nostrum alias libero. rerum in alias autem suscipit aut sint et ducimus quisquam dolores soluta repellendus et aut. necessitatibus alias non mollitia ipsam aut nihil. illo dignissimos quisquam perferendis delectus quis omnis perferendis esse laudantium corporis commodi id veniam quas
                    </p>
                  </div>
                </div>
                <div class="comment">
                  <div class="comment-image">
                    <img src="http://uifaces.com/faces/_twitter/FreelanceNathan_120.jpg" />
                  </div>
                  <div class="comment-content">
                    <h2>
                      Caitlyn Goldman
                    </h2>
                    <p class="comment-detail">
                      Sun Jan 15, 1995
                    </p>
                    <p>
                      Vel cum ut quia aperiam aut natus qui dolores qui. aut perspiciatis fugit sapiente alias consectetur officia dolorem qui deleniti in. dicta eum explicabo repudiandae saepe soluta sequi voluptas et dolorem. earum et aut et quia ea molestiae veritatis ipsum aliquid sint autem esse. rerum ipsam veritatis officiis fugit velit doloremque nihil similique voluptatibus iusto fugiat nisi dolore ut. ab non quia ut officiis placeat debitis cupiditate quia. quam provident rerum incidunt est qui consequatur blanditiis distinctio id reprehenderit sint delectus architecto delectus
                    </p>
                  </div>
                </div>
                <div class="comment">
                  <div class="comment-image">
                    <img src="http://uifaces.com/faces/_twitter/kristijantaken_120.jpg" />
                  </div>
                  <div class="comment-content">
                    <h2>
                      Danielle Byrd
                    </h2>
                    <p class="comment-detail">
                      Tue Jun 24, 1997
                    </p>
                    <p>
                      Est eius et eligendi. ipsam dolorem ab eum nisi magni tempore distinctio dignissimos asperiores. optio est sunt vel impedit aut dolores
                    </p>
                  </div>
                </div>
                <div class="comment">
                  <div class="comment-image">
                    <img src="http://uifaces.com/faces/_twitter/gerwitz_120.jpg" />
                  </div>
                  <div class="comment-content">
                    <h2>
                      Collin Lloyd
                    </h2>
                    <p class="comment-detail">
                      Thu Jun 26, 2003
                    </p>
                    <p>
                      Eligendi nulla minima consequatur laudantium ipsam aut consequatur aut nemo ipsum dolores optio tempore. eos reiciendis voluptas mollitia eos aspernatur distinctio totam qui repellat facilis facere. consequuntur eos quasi in ut voluptates repellat dolorum. et et ut itaque voluptatem maiores qui modi repudiandae dolor vel et praesentium qui. consequuntur sit autem nemo tempora recusandae in possimus sint nostrum magnam. omnis aut illo et eius iste voluptates et illum numquam amet explicabo. et beatae quis recusandae deserunt et atque nulla quaerat earum
                    </p>
                  </div>
                </div>
                <div class="comment">
                  <div class="comment-image">
                    <img src="http://uifaces.com/faces/_twitter/eldelentes_120.jpg" />
                  </div>
                  <div class="comment-content">
                    <h2>
                      Luis Walton
                    </h2>
                    <p class="comment-detail">
                      Wed Mar 03, 1993
                    </p>
                    <p>
                      Sint excepturi omnis deserunt qui sunt voluptas aut. necessitatibus molestias similique et distinctio et suscipit rerum quis sit. molestias iste voluptatem minus fuga expedita. vero molestias et placeat iure explicabo fugiat quaerat nostrum voluptatem qui. sunt quia mollitia magni. ut dolore laboriosam modi cumque. et et qui non minima velit rerum nihil quae
                    </p>
                  </div>
                </div>
                <div class="comment">
                  <div class="comment-image">
                    <img src="http://uifaces.com/faces/_twitter/benpeck_120.jpg" />
                  </div>
                  <div class="comment-content">
                    <h2>
                      Dirk Reid
                    </h2>
                    <p class="comment-detail">
                      Tue Mar 21, 2000
                    </p>
                    <p>
                      Autem rerum nihil sit libero laudantium temporibus maxime voluptatem id voluptates suscipit repudiandae. ullam sapiente voluptas aperiam at et at ut est itaque quaerat maiores enim voluptatem. illum quo dolorem officiis voluptatem praesentium quo fugiat eveniet qui dignissimos quis beatae. sed vitae natus veniam et. rerum labore cupiditate voluptatem ut ut sed ut. adipisci explicabo non quos rem dolores eius sit sint
                    </p>
                  </div>
                </div>
                <div class="comment">
                  <div class="comment-image">
                    <img src="/images/seller-page-profile.jpg" /><a href="/user-profile">Artery</a>
                  </div>
                  <div class="comment-content">
                    <div class="ui form">
                      <form>
                        <div class="field">
                          <textarea name="body" placeholder="Write your comment here." rows="4" type="text"></textarea>
                        </div>
                        <div class="field">
                          <button class="call-to-action" type="submit">Post comment</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="drilldown-secondary">
            <div class="drilldown-card simple">
              <span class="heading">Related articles</span>
              <div style="margin-top:1em;">
                <div class="comment">
                  <div class="comment-content" style="padding-left:0;">
                    <a href="/articles-drilldown">We Can Dream: An Expansive Tennessee Farmhouse on 750 Acres</a>
                    <p class="comment-detail">
                      Sun May 25, 2008
                    </p>
                  </div>
                  <a class="comment-image" href="/articles-drilldown" style="padding-right:0;padding-left:1em;"><img class="square" src="/images/photos/prev41.jpg" style="height:72px;width:72px;" /></a>
                </div>
                <div class="comment">
                  <div class="comment-content" style="padding-left:0;">
                    <a href="/articles-drilldown">Houzz Tour: Clever DIY Tricks Warm a Rustic Rental Cottage</a>
                    <p class="comment-detail">
                      Mon Nov 07, 2005
                    </p>
                  </div>
                  <a class="comment-image" href="/articles-drilldown" style="padding-right:0;padding-left:1em;"><img class="square" src="/images/photos/prev50.jpg" style="height:72px;width:72px;" /></a>
                </div>
                <div class="comment">
                  <div class="comment-content" style="padding-left:0;">
                    <a href="/articles-drilldown">Room of the Day: A Master Bath Embraces the River View</a>
                    <p class="comment-detail">
                      Wed May 08, 1991
                    </p>
                  </div>
                  <a class="comment-image" href="/articles-drilldown" style="padding-right:0;padding-left:1em;"><img class="square" src="/images/photos/prev17.jpg" style="height:72px;width:72px;" /></a>
                </div>
                <div class="comment">
                  <div class="comment-content" style="padding-left:0;">
                    <a href="/articles-drilldown">5 Ways to Put Fall Leaves to Work in Your Garden</a>
                    <p class="comment-detail">
                      Sat Sep 18, 1993
                    </p>
                  </div>
                  <a class="comment-image" href="/articles-drilldown" style="padding-right:0;padding-left:1em;"><img class="square" src="/images/photos/prev49.jpg" style="height:72px;width:72px;" /></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="panel">
      <section>
        <div class="drilldown-full">
          <div class="drilldown-heading">
            <section class="main no-bot">
              <h4 class="pull-left">
                Related conversations
              </h4>
            </section>
          </div>
        </div>
        <div class="drilldown-related">
          <div class="grid-2" data-columns="">
            <div class="card">
              <a href="#">
                <div class="card-image" style="background-image:url('/images/photos/prev09.jpg');"></div>
              </a>
              <div class="card-details">
                <section>
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="http://uifaces.com/faces/_twitter/motherfuton_120.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="/conversations-drilldown" style="line-height:1.5em;"><span>How will I be able to buy products with selle...</span></a><br /><a class="location" href="/seller-page">Jordyn Bowman</a>
                    </div>
                  </div>
                </section>
              </div>
            </div>
            <div class="card">
              <a href="#">
                <div class="card-image" style="background-image:url('/images/photos/prev04.jpg');"></div>
              </a>
              <div class="card-details">
                <section>
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="http://uifaces.com/faces/_twitter/toodlenoodle_120.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="/conversations-drilldown" style="line-height:1.5em;"><span>Kitchen and (me) need help for this frustrati...</span></a><br /><a class="location" href="/seller-page">Maisie Waters</a>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <div class="landing-view-all" style="text-align:center;">
            <a class="button light" href="/conversations" style="padding-left:2em;padding-right:2em;"><span>See all conversations</span><i class="icon ion-ios-arrow-right"></i></a>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop

@section('scripts')
  @parent

  <script src="/javascripts/vendor/lightslider.js" type="text/javascript"></script>
  <script src="/javascripts/vendor/slick.js" type="text/javascript"></script>
  <script type="text/javascript">
  $('#image-gallery').lightSlider({
      gallery: true,
      item: 1,
      slideMargin: 0,
      currentPagerPosition: 'left',
      thumbItem: 8,
      loop: true
    });
  </script>
@stop
