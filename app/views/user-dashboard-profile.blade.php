@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div class="seller-section simple">
                <div class="user seller">
                  <div class="user-thumb">
                    <img class="square" src="/images/forenheit.jpg" />
                  </div>
                  <div class="user-info">
                    <a class="name header" href="/seller-page">Forenheit Studio/Architecture</a>
                  </div>
                </div>
                <ul class="seller-actions">
                  <li>
                    <a class="seller-link" href="/user-profile">View profile</a>
                  </li>
                </ul>
              </div>
              <div id="seller-nav-unfixed">
                <div class="seller-section">
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-profile">Edit profile</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-profile-trust">Trust and verification</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-reviews">Reviews<span class="number">23</span></a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <section class="main no-top">
              <div class="drilldown-full">
                <h5 style="margin-bottom:1em;">
                  Edit your profile
                </h5>
                <div class="drilldown-card">
                  <div class="drilldown-content">
                    <div class="ui form">
                      <form>
                        <div class="field" style="width:240px;">
                          <label style="font-weight:500;">Profile Avatar</label>
                          <div class="card card-form upload-image" style="border-color:rgba(39, 41, 43, 0.15);">
                            <div class="card-image" style="width:100%;">
                              <p style="padding-top:0.5em;">
                                Click to upload
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="three fields">
                          <div class="field">
                            <label style="font-weight:500;">First Name</label><input placeholder="Enter your first name" type="text" />
                          </div>
                          <div class="field">
                            <label style="font-weight:500;">Last Name</label><input placeholder="Enter your last name" type="text" />
                          </div>
                          <div class="field">
                            <label style="font-weight:500;">Gender</label>
                            <div class="ui search dropdown simple selection" style="font-size:0.85em;">
                              <input name="gender" type="hidden" /><i class="dropdown icon"></i>
                              <div class="default text">
                                Select gender
                              </div>
                              <div class="menu">
                                <div class="item">
                                  Male
                                </div>
                                <div class="item">
                                  Female
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="two fields">
                          <div class="field">
                            <label style="font-weight:500;">Birthdate</label><input placeholder="Enter your birth date" type="text" />
                          </div>
                          <div class="field">
                            <label style="font-weight:500;">Email address</label><input placeholder="Enter your email address" type="text" />
                          </div>
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Location</label><input placeholder="Enter your location" type="text" />
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Describe yourself</label><textarea name="body" placeholder="Tell us about yourself so other Cozee users would about your dream house!" rows="3" type="text"></textarea>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <button class="call-to-action">Save</button>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
