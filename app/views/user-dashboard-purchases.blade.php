@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Your dashboard</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-services">Services</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-orders">Orders</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-purchases">Purchases<span class="number">6</span></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <section class="main no-top">
              <h5 style="margin-bottom:1em;">
                Transaction History
              </h5>
              <table class="ui table" style="margin-bottom:2em;font-size:14px;">
                <thead>
                  <tr>
                    <th style="font-weight:600;">
                      Order ID
                    </th>
                    <th class="eight wide" style="font-weight:600;">
                      Product
                    </th>
                    <th style="font-weight:600;">
                      Quantity
                    </th>
                    <th style="font-weight:600;">
                      Date
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      83C5A65F5851D
                    </td>
                    <td>
                      <div class="card" style="width:108px;box-shadow:none;margin-bottom:0;float:left;margin-right:1em;">
                        <a href="/shop-drilldown">
                          <div class="card-image" style="border:none;height:72px;background-image:url('/images/products/prev37.jpg');"></div>
                        </a>
                      </div>
                      <span>Ipsa non eaque eum qui</span>
                      <p class="price" style="margin-bottom:0;">
                        ₱ 11357
                      </p>
                    </td>
                    <td>
                      24
                    </td>
                    <td>
                      Sun Jun 22, 2008
                    </td>
                  </tr>
                  <tr>
                    <td>
                      2660AEF473ADC
                    </td>
                    <td>
                      <div class="card" style="width:108px;box-shadow:none;margin-bottom:0;float:left;margin-right:1em;">
                        <a href="/shop-drilldown">
                          <div class="card-image" style="border:none;height:72px;background-image:url('/images/products/prev06.jpg');"></div>
                        </a>
                      </div>
                      <span>Molestias laboriosam ...</span>
                      <p class="price" style="margin-bottom:0;">
                        ₱ 2096
                      </p>
                    </td>
                    <td>
                      10
                    </td>
                    <td>
                      Wed Jan 22, 1997
                    </td>
                  </tr>
                  <tr>
                    <td>
                      BF2A7348B7EB1
                    </td>
                    <td>
                      <div class="card" style="width:108px;box-shadow:none;margin-bottom:0;float:left;margin-right:1em;">
                        <a href="/shop-drilldown">
                          <div class="card-image" style="border:none;height:72px;background-image:url('/images/products/prev47.jpg');"></div>
                        </a>
                      </div>
                      <span>Libero molestiae aliq...</span>
                      <p class="price" style="margin-bottom:0;">
                        ₱ 997
                      </p>
                    </td>
                    <td>
                      46
                    </td>
                    <td>
                      Tue Nov 24, 2009
                    </td>
                  </tr>
                  <tr>
                    <td>
                      E6EC7094F8DA0
                    </td>
                    <td>
                      <div class="card" style="width:108px;box-shadow:none;margin-bottom:0;float:left;margin-right:1em;">
                        <a href="/shop-drilldown">
                          <div class="card-image" style="border:none;height:72px;background-image:url('/images/products/prev22.jpg');"></div>
                        </a>
                      </div>
                      <span>Et neque blanditiis f...</span>
                      <p class="price" style="margin-bottom:0;">
                        ₱ 4504
                      </p>
                    </td>
                    <td>
                      22
                    </td>
                    <td>
                      Thu Apr 11, 1996
                    </td>
                  </tr>
                  <tr>
                    <td>
                      B71C580AF488F
                    </td>
                    <td>
                      <div class="card" style="width:108px;box-shadow:none;margin-bottom:0;float:left;margin-right:1em;">
                        <a href="/shop-drilldown">
                          <div class="card-image" style="border:none;height:72px;background-image:url('/images/products/prev08.jpg');"></div>
                        </a>
                      </div>
                      <span>Omnis totam non reici...</span>
                      <p class="price" style="margin-bottom:0;">
                        ₱ 7869
                      </p>
                    </td>
                    <td>
                      17
                    </td>
                    <td>
                      Tue Oct 13, 1992
                    </td>
                  </tr>
                </tbody>
              </table>
            </section>
            <section class="main">
              <div class="drilldown-full">
                <h5 style="margin-bottom:0.75em;">
                  Purchases
                </h5>
                <h5 class="pull-right" style="margin-top:0.15em;">
                  14 <span>products</span>
                </h5>
                <button class="light" style="margin-bottom:1.5em;" type="button"><span style="margin-right:0;">Sort by:</span> <span>Price</span><i class="icon ion-ios-arrow-down"></i></button>
              </div>
              <div class="drilldown-full">
                <div class="grid-4" data-columns="">
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev24.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Qui dolor quo ut molestiae
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">inn Christian</a><span class="price pull-right">₱ 2392</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev22.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Corporis quisquam enim perspiciatis similique
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Lexi Rollins</a><span class="price pull-right">₱ 12925</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev02.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Adipisci provident animi aut ut
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Jacquelyn Cain</a><span class="price pull-right">₱ 2956</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev06.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Laudantium provident laboriosam aliquid aut
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Summer Bender</a><span class="price pull-right">₱ 12823</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev05.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Totam ut aut est architecto
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Uriel Weiner</a><span class="price pull-right">₱ 10910</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev33.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Nulla vitae dolorem voluptate et
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Laila Schroeder</a><span class="price pull-right">₱ 8581</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev15.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Id eius dolores modi a
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Marsha McLean</a><span class="price pull-right">₱ 6875</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev32.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Autem necessitatibus voluptas expedita sapiente
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Rafael Chang</a><span class="price pull-right">₱ 8013</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev48.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Rerum facilis sint quo est
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Angelo Jones</a><span class="price pull-right">₱ 2971</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev28.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Aut sit nihil sit eum
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Lori Hewitt</a><span class="price pull-right">₱ 8186</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev28.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Expedita reprehenderit distinctio harum dolorem
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Dalton Petersen</a><span class="price pull-right">₱ 8176</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev02.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Magnam id veritatis qui eum
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">inley Sykes</a><span class="price pull-right">₱ 6179</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev17.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Libero dolore modi recusandae nemo
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Melinda Walton</a><span class="price pull-right">₱ 9568</span>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/products/prev17.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Numquam voluptatum odit numquam odio
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Isabelle Golden</a><span class="price pull-right">₱ 3546</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
