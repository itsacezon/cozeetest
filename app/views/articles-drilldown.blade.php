@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section>
        <div class="drilldown-full">
          <div class="drilldown-breadcrumbs">
            <ul>
              <li>
                <a href="/articles">Articles</a>
              </li>
              <li>
                <a href="/articles-category">Bedroom</a>
              </li>
              <li>
                <a href="/articles-category">Closets</a>
              </li>
            </ul>
          </div>
          <div class="drilldown-heading">
            <div class="drilldown-content">
              <h3>
                Build a Better Bedroom: Inspiring Walk-in Closets
              </h3>
              <p style="margin-bottom:0;">
                <span>by</span> <a href="/seller-page">Real Living PH</a>
              </p>
            </div>
          </div>
        </div>
        <div class="drilldown-full">
          <div class="drilldown-main">
            <div class="drilldown-card secondary" id="social">
              <div class="drilldown-content">
                <div class="actions pull-right">
                  <button class="light"><span>Favorite</span><i class="icon ion-android-favorite"></i></button>
                </div>
                <div class="actions pull-left">
                  <button class="light" style="background-color:#3B5998;color:white;margin-right:0.5em;"><span>Share</span><i class="icon ion-social-facebook"></i></button><button class="light" style="background-color:#55ACEE;color:white;margin-right:0.5em;"><span>Tweet</span><i class="icon ion-social-twitter"></i></button><button class="light" style="background-color:#C92228;color:white;margin-right:0.5em;"><span>Pin it</span><i class="icon ion-social-pinterest"></i></button>
                </div>
              </div>
            </div>
            <div class="drilldown-card">
              <div class="drilldown-content">
                <div class="drilldown-gallery drilldown-card">
                  <div class="image" style="background-image:url('/images/photos/prev06.jpg');background-size:cover;"></div>
                </div>
                <div class="span-6" style="margin-bottom:1.5em;">
                  <p style="font-size:14px;">
                    After work had sent them around the globe, the owners of this California wine country house wanted to put down roots in their retirement. They turned to their vacation house, which had served as a constant during years of relocations. It was a turn-of-the-20th-century cottage on a piece of land that had been part of a large, long-gone fig and walnut farm. The problem was that the place was fine for a family weekend but not big enough for an extended stay. "I wanted to have things like closets and a decent bathroom for a house I would live in full-time," says one of the owners. "But I was so nervous about changing it &ndash; I wanted to be careful to preserve its charm." She hired architect Nick Noyes to help design a house that would respect its past and be ready for the future.
                  </p>
                </div>
                <div class="drilldown-gallery drilldown-card">
                  <div class="image" style="background-image:url('/images/photos/prev07.jpg');background-size:cover;"></div>
                </div>
                <div class="span-6" style="margin-bottom:1.5em;">
                  <p style="font-size:14px;">
                    "Houses are like people. Inherent in any structure, there are limits, constrictions and challenges. It is the house holder's job to create magic out of the flaw &ndash; to transcend the problem and make something beautiful out of it," says the owner, who professes a weakness for difficult-to-renovate properties. "Our family loved the house before the remodel, but it was funky. It hadn't been touched since it was built &ndash; except for some bad touches that happened in the 1970s."
                  </p>
                  <p style="font-size:14px;">
                    Noyes had a problem-solving plan, the essence of which was simple: Grow the basement and the attic levels and reconfigure the middle floor for better flow and better access to the deep raised porch that hugged the building.
                  </p>
                  <p style="font-size:14px;">
                    "There was a low-ceilinged basement that was hard to stand up in, and the attic was simply storage space," he says. "We raised the house to make the lowest level usable. It now holds an entry, utilities area and a guest room. The remodeled attic has a taller roofline and six dormers that give it more headroom and make it livable."
                  </p>
                </div>
                <div class="drilldown-gallery drilldown-card">
                  <div class="image" style="background-image:url('/images/photos/prev19.jpg');background-size:cover;"></div>
                </div>
                <div class="span-6">
                  <p style="font-size:14px;">
                    These are big changes that have made a big impact, but that doesn't mean the old house is gone. Code restrictions said that the porch railing had to be higher and tighter, but the design is a version of what was there. The brackets on top of the columns were simply removed from the old house and reinstalled on the colonnade. What's new in the design is a little Big Easy flavor.
                  </p>
                </div>
              </div>
            </div>
            <div class="drilldown-card simple">
              <div class="drilldown-content">
                <span class="heading">About</span>
                <div class="user" style="margin:1em 0;">
                  <div class="user-thumb">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548166/eden_sanmoh.jpg" />
                  </div>
                  <div class="user-info">
                    <a class="name">Ace Hardware</a>
                    <p class="location">
                      Makati, PH
                    </p>
                  </div>
                </div>
                <p style="font-size:14px;">
                  Fuga blanditiis est itaque consequatur quos reiciendis praesentium libero ut impedit excepturi dolor ullam. laborum quam autem aut neque voluptatem ipsa et odit voluptatibus. commodi a cum et officia magnam reiciendis et consequatur repellat accusantium officia dicta
                </p>
              </div>
            </div>
          </div>
          <div class="drilldown-secondary">
            <div class="drilldown-card secondary" id="more-articles">
              <div class="drilldown-content">
                <section>
                  <span class="heading">More from Real Living PH</span>
                  <div style="margin-top:1em;">
                    <div class="comment">
                      <div class="comment-content" style="padding-left:0;">
                        <a href="/articles-drilldown">We Can Dream: An Expansive Tennessee Farmhouse on 750 Acres</a>
                        <p class="comment-detail">
                          Mon Apr 02, 2001
                        </p>
                      </div>
                      <a class="comment-image" href="/articles-drilldown" style="padding-right:0;padding-left:1em;"><img class="square" src="/images/photos/prev20.jpg" style="height:72px;width:72px;" /></a>
                    </div>
                    <div class="comment">
                      <div class="comment-content" style="padding-left:0;">
                        <a href="/articles-drilldown">Houzz Tour: Clever DIY Tricks Warm a Rustic Rental Cottage</a>
                        <p class="comment-detail">
                          Tue Jun 04, 1996
                        </p>
                      </div>
                      <a class="comment-image" href="/articles-drilldown" style="padding-right:0;padding-left:1em;"><img class="square" src="/images/photos/prev50.jpg" style="height:72px;width:72px;" /></a>
                    </div>
                    <div class="comment">
                      <div class="comment-content" style="padding-left:0;">
                        <a href="/articles-drilldown">Room of the Day: A Master Bath Embraces the River View</a>
                        <p class="comment-detail">
                          Sun Nov 29, 1998
                        </p>
                      </div>
                      <a class="comment-image" href="/articles-drilldown" style="padding-right:0;padding-left:1em;"><img class="square" src="/images/photos/prev41.jpg" style="height:72px;width:72px;" /></a>
                    </div>
                    <div class="comment">
                      <div class="comment-content" style="padding-left:0;">
                        <a href="/articles-drilldown">5 Ways to Put Fall Leaves to Work in Your Garden</a>
                        <p class="comment-detail">
                          Sun Dec 03, 2000
                        </p>
                      </div>
                      <a class="comment-image" href="/articles-drilldown" style="padding-right:0;padding-left:1em;"><img class="square" src="/images/photos/prev05.jpg" style="height:72px;width:72px;" /></a>
                    </div>
                    <div class="comment">
                      <div class="comment-content" style="padding-left:0;">
                        <a href="/articles-drilldown">Once a Barren Rooftop, Now a Serene City Getaway</a>
                        <p class="comment-detail">
                          Wed Jul 10, 2002
                        </p>
                      </div>
                      <a class="comment-image" href="/articles-drilldown" style="padding-right:0;padding-left:1em;"><img class="square" src="/images/photos/prev23.jpg" style="height:72px;width:72px;" /></a>
                    </div>
                    <div class="comment">
                      <div class="comment-content" style="padding-left:0;">
                        <a href="/articles-drilldown">Not a Big Cook? These Fun Kitchen Ideas Are for You</a>
                        <p class="comment-detail">
                          Mon Jan 29, 1996
                        </p>
                      </div>
                      <a class="comment-image" href="/articles-drilldown" style="padding-right:0;padding-left:1em;"><img class="square" src="/images/photos/prev29.jpg" style="height:72px;width:72px;" /></a>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="panel panel-light" id="comments">
      <section>
        <div class="drilldown-full">
          <div class="drilldown-main">
            <h4 style="margin-bottom:1em;">
              <span>5</span> comments
            </h4>
            <ul class="comments">
              <li class="comment">
                <div class="comment-image">
                  <img src="http://uifaces.com/faces/_twitter/wrightmartin_120.jpg" /><a href="/user-profile">Randal</a>
                </div>
                <div class="comment-content">
                  <p class="comment-detail">
                    Mon Nov 01, 1999
                  </p>
                  <p>
                    Itaque qui voluptas repellendus. distinctio dicta neque sint doloremque consequuntur similique. ut excepturi placeat inventore consequatur maxime assumenda ut saepe adipisci non. debitis nemo eos temporibus sed suscipit. earum eveniet excepturi sit saepe vitae eligendi vel laboriosam ab iure natus. ea est sunt eum et vel odit
                  </p>
                </div>
              </li>
              <li class="comment">
                <div class="comment-image">
                  <img src="http://uifaces.com/faces/_twitter/acoops__120.jpg" /><a href="/user-profile">Alberto</a>
                </div>
                <div class="comment-content">
                  <p class="comment-detail">
                    Fri Apr 24, 1998
                  </p>
                  <p>
                    Voluptatibus veritatis sunt fuga alias et qui ratione deleniti sed. veritatis voluptas occaecati est facere facere libero. quia non aut ducimus sequi numquam consequatur at vel vitae aliquid doloribus sunt. qui eaque quia aut sit ut sit commodi et rerum eius quisquam. sit in minus consequatur sit. sit doloribus minus et odio facilis dolorem temporibus aut quia aut saepe voluptates. officiis id minima quia quae mollitia
                  </p>
                </div>
              </li>
              <li class="comment">
                <div class="comment-image">
                  <img src="http://uifaces.com/faces/_twitter/Fubaruba_120.jpg" /><a href="/user-profile">Angelo</a>
                </div>
                <div class="comment-content">
                  <p class="comment-detail">
                    Thu Dec 14, 1995
                  </p>
                  <p>
                    Eos harum dignissimos voluptatibus sit veniam accusantium sed nostrum. in odit mollitia at perferendis ex deserunt architecto exercitationem deleniti reiciendis. repudiandae qui dolores eum quis corporis et dolore enim incidunt quis et. officia nulla natus molestiae assumenda. alias saepe in qui dolores cumque perspiciatis necessitatibus itaque eaque sed
                  </p>
                </div>
              </li>
              <li class="comment">
                <div class="comment-image">
                  <img src="http://uifaces.com/faces/_twitter/owlfurty_120.jpg" /><a href="/user-profile">Eoin</a>
                </div>
                <div class="comment-content">
                  <p class="comment-detail">
                    Thu May 10, 1990
                  </p>
                  <p>
                    Voluptates debitis quis exercitationem nam architecto et est labore eius sed esse in similique unde. reprehenderit molestiae ut ratione omnis omnis velit. aliquam minus fuga quibusdam rerum quaerat dolor consequuntur ut rerum ipsa placeat enim quisquam
                  </p>
                </div>
              </li>
              <li class="comment">
                <div class="comment-image">
                  <img src="http://uifaces.com/faces/_twitter/mambows_120.jpg" /><a href="/user-profile">Adrianna</a>
                </div>
                <div class="comment-content">
                  <p class="comment-detail">
                    Sat Feb 14, 1998
                  </p>
                  <p>
                    Voluptas harum porro veritatis dolorem eum aut consequuntur tempore et veniam. optio cum quos suscipit et iste amet eos libero ut. qui in omnis nemo. saepe ducimus amet illum. debitis impedit assumenda deserunt explicabo recusandae eum natus deserunt. sapiente adipisci dolor quaerat ut voluptas vel dolor mollitia voluptas et molestiae
                  </p>
                </div>
              </li>
              <li class="comment">
                <div class="comment-image">
                  <img src="/images/seller-page-profile.jpg" /><a href="/user-profile">Artery</a>
                </div>
                <div class="comment-content">
                  <div class="ui form">
                    <form>
                      <div class="field">
                        <textarea name="body" placeholder="Write your comment here." rows="4" type="text"></textarea>
                      </div>
                      <div class="field">
                        <button class="call-to-action" type="submit">Post comment</button>
                      </div>
                    </form>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </section>
    </div>
    <div class="panel" id="related">
      <section>
        <div class="drilldown-full">
          <div class="drilldown-heading">
            <section class="main no-bot">
              <h4 class="pull-left">
                Related articles
              </h4>
            </section>
          </div>
        </div>
        <div class="drilldown-related">
          <div class="grid-2" data-columns="">
            <div class="card">
              <a href="#">
                <div class="card-image" style="background-image:url('/images/photos/prev27.jpg');"></div>
              </a>
              <div class="card-details">
                <section>
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="http://goodlogo.com/images/logos/home_depot_logo_2776.gif" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;"><span>Room of the Day: A Master Bath Embraces the R...</span></a><br /><a class="location" href="/seller-page">Ace Hardware</a>
                    </div>
                  </div>
                </section>
              </div>
            </div>
            <div class="card">
              <a href="#">
                <div class="card-image" style="background-image:url('/images/photos/prev25.jpg');"></div>
              </a>
              <div class="card-details">
                <section>
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="http://goodlogo.com/images/logos/home_depot_logo_2776.gif" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;"><span>Not a Big Cook? These Fun Kitchen Ideas Are f...</span></a><br /><a class="location" href="/seller-page">Ace Hardware</a>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <div class="landing-view-all" style="text-align:center;">
            <a class="button light" href="/articles" style="padding-left:2em;padding-right:2em;"><span>See all articles</span><i class="icon ion-ios-arrow-right"></i></a>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop

@section('scripts')
  @parent

  <script src="/javascripts/vendor/slick.js" type="text/javascript"></script>
  <script src="/javascripts/vendor/scrolltofixed.js" type="text/javascript"></script>
    <script type="text/javascript">
      $('.articles-carousel').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
      });

      $('#more-articles').scrollToFixed({
        limit: $('#related').offset().top - $('#more-articles').outerHeight(true),
        preFixed: function() {
        },
        postFixed: function() {
        },
        preAbsolute: function() {
        }
      });

      $('#social').scrollToFixed({
        limit: $('#comments').offset().top - $('#social').outerHeight(true),
        preFixed: function() {
        },
        postFixed: function() {
        },
        preAbsolute: function() {
        }
      });
    </script>
@stop
