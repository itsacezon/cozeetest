@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Info and Appearance</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-storefront">Edit storefront</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-storefront-about">About your shop</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-storefront-policies">Policies</a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Options</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="#">Storefront options</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Vacation Mode</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Web Analytics</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Close shop</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="assets/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <section class="main no-top">
              <div class="drilldown-full">
                <h5 style="margin-bottom:1em;">
                  Your shop policies
                </h5>
                <div class="drilldown-card">
                  <div class="drilldown-content">
                    <div class="ui form">
                      <form>
                        <div class="field">
                          <label style="font-weight:500;">Payment</label><textarea name="body" placeholder="Payment methods, terms, deadlines, taxes, cancellation policy, etc." rows="4" type="text"></textarea>
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Shipping</label><textarea name="body" placeholder="Shipping methods, upgrades, deadlines, insurance, confirmation, international customs, etc." rows="4" type="text"></textarea>
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Refunds and Exchanges</label><textarea name="body" placeholder="Terms, eligible items, damages, losses, etc." rows="4" type="text"></textarea>
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Additional Policies and FAQs</label><textarea name="body" placeholder="Additional policies, FAQs, custom orders, wholesale & consignment, guarantees, etc." rows="4" type="text"></textarea>
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Seller Information</label><textarea name="body" placeholder="Some countries require seller information such as your name, physical address, contact email address and, where applicable, tax identification number. See this FAQ for more information." rows="4" type="text"></textarea>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <button class="call-to-action">Save</button>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
