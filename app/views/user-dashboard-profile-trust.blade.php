@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div class="seller-section simple">
                <div class="user seller">
                  <div class="user-thumb">
                    <img class="square" src="/images/forenheit.jpg" />
                  </div>
                  <div class="user-info">
                    <a class="name header" href="/seller-page">Forenheit Studio/Architecture</a>
                  </div>
                </div>
                <ul class="seller-actions">
                  <li>
                    <a class="seller-link" href="/user-profile">View profile</a>
                  </li>
                </ul>
              </div>
              <div id="seller-nav-unfixed">
                <div class="seller-section">
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-profile">Edit profile</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-profile-trust">Trust and verification</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-reviews">Reviews<span class="number">33</span></a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <section class="main no-top">
              <div class="drilldown-full">
                <div class="drilldown-related enclosed" style="padding:0;margin-bottom:1.5em;">
                  <section>
                    <p style="margin-bottom:0;">
                      Excepturi ut nihil dolor quia aliquid doloremque quis quas incidunt minus neque. Debitis sapiente minus aut sit cumque distinctio distinctio voluptas fugiat nesciunt blanditiis placeat sunt. Velit atque nemo quas et omnis. Voluptatem quod accusantium amet aut nam omnis veritatis similique ut asperiores fugit minus minima. Ipsum nisi animi exercitationem et repudiandae aut veritatis occaecati cupiditate minus asperiores autem saepe id
                    </p>
                  </section>
                </div>
              </div>
              <div class="drilldown-full">
                <h5 style="margin-bottom:1em;">
                  Verify your identity
                </h5>
                <div class="drilldown-card">
                  <div class="drilldown-content">
                    <div class="ui form">
                      <form>
                        <div class="field">
                          <label style="font-weight:500;"><i class="icon ion-checkmark-circled" style="color:#3fb34f;margin-right:0.5em;"></i><span>Phone Number</span></label><input placeholder="Rest assured, your number is only shared with another Cozee member once you have a confirmed transaction." type="text" />
                        </div>
                        <div class="field">
                          <label style="font-weight:500;"><i class="icon ion-close-circled" style="color:#e00007;margin-right:0.5em;"></i><span>Email verification</span></label><input placeholder="Enter your email again" type="text" />
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <div class="drilldown-card">
                  <div class="drilldown-content">
                    <div class="ui form">
                      <form>
                        <div class="field">
                          <label style="font-weight:500;">Facebook</label>
                          <div class="two fields">
                            <div class="field">
                              <p style="font-size:14px;">
                                Enim voluptatem quam voluptatem maiores voluptatem consequuntur autem et harum sequi est eligendi. Molestias modi mollitia qui quas laudantium
                              </p>
                            </div>
                            <div class="field">
                              <button class="block call-to-action" style="padding:0.75em;font-size:14px;background-color:#3B5998;">Link account</button>
                            </div>
                          </div>
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Google</label>
                          <div class="two fields">
                            <div class="field">
                              <p style="font-size:14px;">
                                Cum officia quo et ipsa aut dolorum aut alias quis dolore. Vel eum cupiditate et adipisci
                              </p>
                            </div>
                            <div class="field">
                              <button class="block call-to-action" style="padding:0.75em;font-size:14px;background-color:#dd4b39;">Link account</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
