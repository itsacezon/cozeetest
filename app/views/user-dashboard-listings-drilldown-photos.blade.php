@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
                <div class="seller-section highlighted">
                  <div class="seller-section-header">
                    <span>Edit listing</span>
                  </div>
                  <div class="seller-content">
                    <div class="user" style="padding:0 0.75em;padding-bottom:0.5em;">
                      <div class="user-thumb smaller">
                        <img class="square" src="/images/photos/prev37.jpg" />
                      </div>
                      <div class="user-info">
                        <a class="name" href="/services-drilldown" style="font-size:13px;">Minima non animi inventore voluptates</a>
                      </div>
                    </div>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-listings-drilldown">Description</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-listings-drilldown-photos">Photos</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-listings-drilldown-products">Products</a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Help</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="#">Qui non quas</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Et minima quia</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Cum nesciunt quidem</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Magni laborum dolore</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Voluptatem eaque rerum</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <form action="/pro-page-my-services" method="POST">
              <section class="main no-top">
                <div class="drilldown-full">
                  <a class="button light pull-right" href="/user-dashboard-listings-add" style="margin-bottom:1.5em;">Upload photos</a>
                  <h5 class="pull-right" style="margin-top:0.15em;margin-right:1em;">
                    4 <span>photos</span>
                  </h5>
                  <h5 style="margin-bottom:1em;">
                    Photos
                  </h5>
                </div>
                <div class="drilldown-full">
                  <div class="grid-4" data-columns="">
                    <div class="card">
                      <div class="card-ribbon">
                        <i class="icon ion-android-star"></i>
                      </div>
                      <div class="card-image card-small" style="border-bottom:4px white solid;background-image:url('/images/photos/prev16.jpg');">
                        <div class="card-hover">
                          <button class="light favorite" type="button"><i class="icon ion-android-delete"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-image card-small" style="border-bottom:4px white solid;background-image:url('/images/photos/prev34.jpg');">
                        <div class="card-hover">
                          <button class="light favorite" type="button"><i class="icon ion-android-delete"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-image card-small" style="border-bottom:4px white solid;background-image:url('/images/photos/prev10.jpg');">
                        <div class="card-hover">
                          <button class="light favorite" type="button"><i class="icon ion-android-delete"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-image card-small" style="border-bottom:4px white solid;background-image:url('/images/photos/prev22.jpg');">
                        <div class="card-hover">
                          <button class="light favorite" type="button"><i class="icon ion-android-delete"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </form>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
