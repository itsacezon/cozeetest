@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section>
        <div class="drilldown-full">
          <div class="drilldown-breadcrumbs">
            <ul>
              <li>
                <a href="/services">Services</a>
              </li>
              <li>
                <a href="/services-category">Lighting</a>
              </li>
              <li>
                <a href="/services-category">Indoor Lighting</a>
              </li>
            </ul>
          </div>
          <div class="drilldown-heading">
            <div class="drilldown-content">
              <h3>
                Artery Decor and Lighting
              </h3>
              <p style="margin-bottom:0;">
                <span>by</span> <a href="/seller-page">Artery</a>
              </p>
            </div>
          </div>
        </div>
        <div class="drilldown-full">
          <div class="drilldown-misc">
            <div class="drilldown-card">
              <div class="drilldown-content">
                <div class="drilldown-gallery">
                  <button class="light favorite"><i class="icon ion-android-favorite"></i></button><button class="light add"><i class="icon ion-plus"></i></button>
                  <ul id="image-gallery">
                    <li class="image" data-thumb="/images/photos/prev34.jpg" style="background-image:url(/images/photos/prev34.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev13.jpg" style="background-image:url(/images/photos/prev13.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev35.jpg" style="background-image:url(/images/photos/prev35.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev39.jpg" style="background-image:url(/images/photos/prev39.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev12.jpg" style="background-image:url(/images/photos/prev12.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev05.jpg" style="background-image:url(/images/photos/prev05.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev04.jpg" style="background-image:url(/images/photos/prev04.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev47.jpg" style="background-image:url(/images/photos/prev47.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev44.jpg" style="background-image:url(/images/photos/prev44.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev12.jpg" style="background-image:url(/images/photos/prev12.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev22.jpg" style="background-image:url(/images/photos/prev22.jpg);"></li>
                    <li class="image" data-thumb="/images/photos/prev07.jpg" style="background-image:url(/images/photos/prev07.jpg);"></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="drilldown-related">
              <span class="heading">Behind this Project</span>
              <div class="grid-4" data-columns="" style="margin-top:1em;padding:0 1em;">
                <div class="person">
                  <div class="comment no-border" style="margin-bottom:0;">
                    <div class="comment-image">
                      <img src="http://uifaces.com/faces/_twitter/imfine_thankyou_120.jpg" /><a class="name" href="/seller-page">Brenden McLean</a>
                      <p>
                        Designer
                      </p>
                    </div>
                  </div>
                  <div class="toggleable">
                    <div class="user-bio">
                      <p>
                        Deleniti et repellendus qui nemo eligendi ipsum asperiores architecto quia corrupti eligendi velit
                      </p>
                      <a href="/seller-page">View profile</a>
                    </div>
                  </div>
                </div>
                <div class="person">
                  <div class="comment no-border" style="margin-bottom:0;">
                    <div class="comment-image">
                      <img src="http://uifaces.com/faces/_twitter/bradenhamm_120.jpg" /><a class="name" href="/seller-page">Rogelio Matthews</a>
                      <p>
                        Manager
                      </p>
                    </div>
                  </div>
                  <div class="toggleable">
                    <div class="user-bio">
                      <p>
                        Id omnis qui iusto pariatur soluta
                      </p>
                      <a href="/seller-page">View profile</a>
                    </div>
                  </div>
                </div>
                <div class="person">
                  <div class="comment no-border" style="margin-bottom:0;">
                    <div class="comment-image">
                      <img src="http://uifaces.com/faces/_twitter/gerwitz_120.jpg" /><a class="name" href="/seller-page">Kaleigh Heath</a>
                      <p>
                        Manager
                      </p>
                    </div>
                  </div>
                  <div class="toggleable">
                    <div class="user-bio">
                      <p>
                        Nostrum aut expedita possimus
                      </p>
                      <a href="/seller-page">View profile</a>
                    </div>
                  </div>
                </div>
                <div class="person">
                  <div class="comment no-border" style="margin-bottom:0;">
                    <div class="comment-image">
                      <img src="http://uifaces.com/faces/_twitter/motherfuton_120.jpg" /><a class="name" href="/seller-page">Kellie Whitehead</a>
                      <p>
                        Designer
                      </p>
                    </div>
                  </div>
                  <div class="toggleable">
                    <div class="user-bio">
                      <p>
                        Minus est delectus aut enim expedita commodi expedita quis magni dignissimos quidem et
                      </p>
                      <a href="/seller-page">View profile</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="drilldown-misc-2">
            <div class="drilldown-tabs">
              <nav>
                <ul class="drilldown-tabs-nav">
                  <li class="service">
                    <a class="selected" data-content="description" href="#0">Description</a>
                  </li>
                  <li class="service">
                    <a data-content="reviews" href="#0">Reviews</a>
                  </li>
                  <li class="service">
                    <a data-content="products" href="#0">Products</a>
                  </li>
                </ul>
              </nav>
              <ul class="drilldown-tabs-content">
                <li class="selected" data-content="description">
                  <p>
                    Explicabo perspiciatis rerum soluta nesciunt. deserunt est molestias itaque ducimus aut ut aut sed aspernatur voluptatem possimus reiciendis et aut. eveniet velit libero excepturi sequi pariatur commodi nemo quae libero. culpa est non et rerum neque quis sed quis eum fuga sit. impedit nisi corporis quia aut. non laboriosam eum similique esse voluptates eos fugiat aut
                  </p>
                  <p>
                    Illum mollitia iusto alias veniam recusandae aliquid et provident velit suscipit sint ut aut sint. tempore nam non sit enim consequatur labore dignissimos modi ut corporis facilis. ut aliquam quia eos. eveniet alias perferendis quia quis tenetur accusamus. maxime quae aut dignissimos. quia officiis et consequatur qui quia commodi molestiae laudantium a delectus quod veritatis. quis non officiis consequatur omnis ducimus aspernatur
                  </p>
                  <div class="drilldown-card bordered" style="margin-bottom:0;margin-top:1.5em;">
                    <div class="drilldown-content">
                      <section>
                        <div class="ui form">
                          <div class="two fields">
                            <div class="field">
                              <label style="margin-bottom:0;font-weight:600;">Style</label>
                              <p style="margin-bottom:0;font-size:14px;">
                                Traditional
                              </p>
                            </div>
                            <div class="field">
                              <label style="margin-bottom:0;font-weight:600;">Category</label>
                              <p style="margin-bottom:0;font-size:14px;">
                                Kitchen
                              </p>
                            </div>
                          </div>
                          <div class="field actions">
                            <button class="block call-to-action">Request a quote</button>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                </li>
                <li data-content="reviews">
                  <h5>
                    6 reviews
                  </h5>
                  <ul class="comments">
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/alagoon_120.jpg" /><a href="/user-profile">Camila</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
                        </div>
                        <p>
                          Aut laudantium fuga laudantium. Eligendi optio illum omnis quo exercitationem sed eius
                        </p>
                      </div>
                    </li>
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/CrafterSama_120.jpg" /><a href="/user-profile">Kendrick</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                        </div>
                        <p>
                          Et autem et voluptatibus facere accusamus est cumque veniam vel quis qui. Facilis non consequatur accusantium autem repudiandae
                        </p>
                      </div>
                    </li>
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/gilbertglee_120.jpg" /><a href="/user-profile">Reid</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
                        </div>
                        <p>
                          Est tenetur laboriosam accusantium repudiandae quas nostrum animi repudiandae vero voluptas sit. Dolor dolor adipisci et
                        </p>
                      </div>
                    </li>
                  </ul>
                  <div class="landing-view-all" style="text-align:center;">
                    <a class="button light" href="/seller-page-reviews" id="view-reviews-button" style="margin-top:2em;padding-left:2em;padding-right:2em;"><span>View all reviews</span><i class="icon ion-ios-arrow-right"></i></a>
                  </div>
                </li>
                <li class="grid-2" data-columns="" data-content="products">
                  <div class="product">
                    <div class="comment no-border">
                      <div class="comment-image">
                        <img class="square" src="/images/products/prev45.jpg" /><a href="/shop-drilldown">Sed ipsam dolores</a>
                        <p class="location price">
                          Php 11146
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="product">
                    <div class="comment no-border">
                      <div class="comment-image">
                        <img class="square" src="/images/products/prev24.jpg" /><a href="/shop-drilldown">Nam vel maxime</a>
                        <p class="location price">
                          Php 10128
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="product">
                    <div class="comment no-border">
                      <div class="comment-image">
                        <img class="square" src="/images/products/prev25.jpg" /><a href="/shop-drilldown">Veniam ipsam mollitia</a>
                        <p class="location price">
                          Php 2132
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="product">
                    <div class="comment no-border">
                      <div class="comment-image">
                        <img class="square" src="/images/products/prev20.jpg" /><a href="/shop-drilldown">Ipsam rerum nulla</a>
                        <p class="location price">
                          Php 9899
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="product">
                    <div class="comment no-border">
                      <div class="comment-image">
                        <img class="square" src="/images/products/prev15.jpg" /><a href="/shop-drilldown">Quo accusantium eligendi</a>
                        <p class="location price">
                          Php 2296
                        </p>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div class="drilldown-card secondary">
              <section>
                <div class="actions">
                  <button class="light" style="background-color:#3B5998;color:white;margin-right:0.5em;"><span>Share</span><i class="icon ion-social-facebook"></i></button><button class="light" style="background-color:#55ACEE;color:white;margin-right:0.5em;"><span>Tweet</span><i class="icon ion-social-twitter"></i></button><button class="light" style="background-color:#C92228;color:white;margin-right:0.5em;"><span>Pin it</span><i class="icon ion-social-pinterest"></i></button>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="panel panel-light">
      <section>
        <div class="drilldown-full">
          <div class="drilldown-heading">
            <section class="main no-bot">
              <h5 class="pull-left">
                More from Artery
              </h5>
            </section>
          </div>
        </div>
        <div class="drilldown-full">
          <div class="drilldown-related">
            <div class="grid-3" data-columns="">
              <div class="card">
                <div class="cards-carousel">
                  <div class="carousel-overlay" data-content="carousel-0">
                    <div class="card-image" style="background-image:url('/images/photos/prev28.jpg');"></div>
                    <div class="card-image" style="background-image:url('/images/photos/prev28.jpg');"></div>
                    <div class="card-image" style="background-image:url('/images/photos/prev03.jpg');"></div>
                  </div>
                </div>
                <div class="card-details">
                  <section>
                    <div class="user">
                      <div class="user-info" style="width:100%;">
                        <a class="name" href="/services-drilldown">Quis officia aut sit atque</a><br /><a class="location" href="/services-drilldown">10 photos</a>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
              <div class="card">
                <div class="cards-carousel">
                  <div class="carousel-overlay" data-content="carousel-1">
                    <div class="card-image" style="background-image:url('/images/photos/prev01.jpg');"></div>
                    <div class="card-image" style="background-image:url('/images/photos/prev23.jpg');"></div>
                    <div class="card-image" style="background-image:url('/images/photos/prev15.jpg');"></div>
                  </div>
                </div>
                <div class="card-details">
                  <section>
                    <div class="user">
                      <div class="user-info" style="width:100%;">
                        <a class="name" href="/services-drilldown">Rerum dolor corrupti eos ex</a><br /><a class="location" href="/services-drilldown">15 photos</a>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
              <div class="card">
                <div class="cards-carousel">
                  <div class="carousel-overlay" data-content="carousel-2">
                    <div class="card-image" style="background-image:url('/images/photos/prev09.jpg');"></div>
                    <div class="card-image" style="background-image:url('/images/photos/prev05.jpg');"></div>
                    <div class="card-image" style="background-image:url('/images/photos/prev34.jpg');"></div>
                  </div>
                </div>
                <div class="card-details">
                  <section>
                    <div class="user">
                      <div class="user-info" style="width:100%;">
                        <a class="name" href="/services-drilldown">Est magnam magni dignissimo...</a><br /><a class="location" href="/services-drilldown">12 photos</a>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
            <div class="landing-view-all" style="text-align:center;">
              <a class="button light" href="/articles" style="padding-left:2em;padding-right:2em;"><span>View more items</span><i class="icon ion-ios-arrow-right"></i></a>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="panel">
      <section class="no-bot">
        <div class="drilldown-full">
          <div class="drilldown-heading">
            <section class="main no-bot">
              <h4 class="pull-left">
                Related
              </h4>
            </section>
          </div>
        </div>
        <div class="drilldown-full">
          <div class="list-side-by-side">
            <ul>
              <li>
                <button class="tag">House Cleaning</button>
              </li>
              <li>
                <button class="tag">TV Mounting</button>
              </li>
              <li>
                <button class="tag">Handyman</button>
              </li>
              <li>
                <button class="tag">Junk Removal</button>
              </li>
              <li>
                <button class="tag">TV Mounting</button>
              </li>
              <li>
                <button class="tag">Interior Design</button>
              </li>
              <li>
                <button class="tag">House Cleaning</button>
              </li>
              <li>
                <button class="tag">House Cleaning</button>
              </li>
              <li>
                <button class="tag">TV Mounting</button>
              </li>
              <li>
                <button class="tag">Handyman</button>
              </li>
              <li>
                <button class="tag">TV Mounting</button>
              </li>
              <li>
                <button class="tag">Handyman</button>
              </li>
            </ul>
          </div>
        </div>
      </section>
      <section class="no-bot">
        <div class="drilldown-full">
          <div class="drilldown-heading">
            <p class="pull-left">
              Articles
            </p>
          </div>
        </div>
        <div class="drilldown-related">
          <div class="grid-2" data-columns="">
            <div class="card">
              <a href="#">
                <div class="card-image" style="background-image:url('/images/photos/prev45.jpg');"></div>
              </a>
              <div class="card-details">
                <section>
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="http://www.campbridge.com/images/cwhomedepot-logo.png" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;"><span>5 Ways to Put Fall Leaves to Work in Your Garden</span></a><br /><a class="location" href="/seller-page">Lowes</a>
                    </div>
                  </div>
                </section>
              </div>
            </div>
            <div class="card">
              <a href="#">
                <div class="card-image" style="background-image:url('/images/photos/prev06.jpg');"></div>
              </a>
              <div class="card-details">
                <section>
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="http://vector-magz.com/wp-content/uploads/2013/07/ace-hardware-logo-vector.png" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;"><span>5 Ways to Put Fall Leaves to Work in Your Garden</span></a><br /><a class="location" href="/seller-page">RealLiving PH</a>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <div class="landing-view-all" style="text-align:center;">
            <a class="button light" href="/articles" style="padding-left:2em;padding-right:2em;"><span>See more articles</span><i class="icon ion-ios-arrow-down"></i></a>
          </div>
        </div>
      </section>
      <section>
        <div class="drilldown-full">
          <div class="drilldown-heading">
            <p class="pull-left">
              Services
            </p>
            <button class="light pull-right" type="button"><span>Quezon City</span><i class="icon ion-android-close"></i></button><button class="light pull-right" style="margin-right:1em;" type="button"><span>Sort by</span><i class="icon ion-ios-arrow-down"></i></button>
          </div>
        </div>
        <div class="drilldown-related">
          <div class="grid-4" data-columns="">
            <div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev10.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Unde dolores quo et expedita</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">inley Sharpe</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev07.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Assumenda in recusandae esse ipsa</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Bianca Rich</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev14.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Voluptas quae enim et corporis</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Bart McNamara</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev19.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Assumenda maiores et in enim</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Esmeralda Middleton</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev39.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Animi minima rerum numquam aut</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Brendon Perkins</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev17.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Rerum maiores illum et deleniti</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Bryce McKenzie</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev34.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Autem harum assumenda sed asperiores</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Kelvin Eason</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev34.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Aut iste expedita est cumque</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Nadia Diaz</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev06.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Sint eveniet ut quia est</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Margarita Chandler</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev49.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Quisquam quis debitis ut natus</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Jared Starr</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev02.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Sed dicta facilis necessitatibus omnis</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Denis Watts</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev12.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Et cupiditate est minus voluptas</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Angelo Mangum</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev29.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Est minima sit autem perferendis</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Stephanie Holmes</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev19.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Cumque quidem omnis officia blanditiis</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Harvey Goldman</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev23.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Magni consequuntur assumenda nulla molestiae</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Mya Lucas</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev08.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Omnis nisi ratione qui earum</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Kelly Chung</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev30.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Velit veniam optio ut voluptatem</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">inn Graves</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev35.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Qui eius in voluptas dolor</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Jerald Norman</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev28.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Sed perferendis vitae aperiam optio</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Yehudi Finch</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev24.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Sit officia id in consequatur</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Danielle Love</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev18.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Deserunt provident ullam voluptates ex</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Noelle Hobbs</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev14.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Praesentium perferendis dolorem velit minima</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Abel Singer</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev16.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Et cum necessitatibus quia exercitationem</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Abigail Connolly</a>
                </div>
              </div>
            </div><div class="card">
              <a href="/services-drilldown">
                <div class="card-image" style="background-image:url('/images/photos/prev23.jpg');">
                  <div class="card-hover">
                    <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                  </div>
                </div>
              </a>
              <div class="card-details">
                <div class="card-header">
                  <span>Corporis eius saepe impedit debitis</span>
                </div>
                <div class="card-author">
                  <a href="/seller-page">Connor Siegel</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop

@section('scripts')
  @parent
  <script src="/javascripts/vendor/lightslider.js" type="text/javascript"></script>
  <script src="/javascripts/tabs.js" type="text/javascript"></script>
  <script src="/javascripts/vendor/slick.js" type="text/javascript"></script>
  <script src="/javascripts/dropdown.js" type="text/javascript"></script>

  <script type="text/javascript">
    $('#image-gallery').lightSlider({
      gallery: true,
      item: 1,
      slideMargin: 0,
      currentPagerPosition: 'left',
      thumbItem: 8,
      loop: true
    });
    $('.carousel-overlay').slick({
      //arrows: false,
      dots: true,
    });
  </script>
@stop
