@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Your dashboard</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-services">Services</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-orders">Orders</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-purchases">Purchases<span class="number">39</span></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <div class="drilldown-full">
              <section class="main no-top">
                <h5 style="margin-bottom:1em;">
                  Outstanding Quotes
                </h5>
                <table class="ui table" style="margin-bottom:2em;font-size:14px;">
                  <thead>
                    <tr>
                      <th style="font-weight:600;">
                        Service category
                      </th>
                      <th style="font-weight:600;">
                        User
                      </th>
                      <th style="font-weight:600;">
                        Status
                      </th>
                      <th colspan="2" style="font-weight:600;">
                        Date
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr onclick="document.location = 'user-dashboard-thread';">
                      <td class="link">
                        House Cleaning
                      </td>
                      <td>
                        Billy Bender
                      </td>
                      <td><button class="light" style="color:red;">Declined</button>
                      </td>
                      <td>
                        Wed Nov 13, 2002
                      </td>
                      <td class="right aligned">
                        <button class="pewter" style="margin-right:0.5em;" type="button">Message</button><button class="light" type="button"><i class="icon ion-android-delete"></i></button>
                      </td>
                    </tr>
                    <tr onclick="document.location = 'user-dashboard-thread';">
                      <td class="link">
                        Landscaping
                      </td>
                      <td>
                        Harvey James
                      </td>
                      <td><button class="light" style="color:red;">Declined</button>
                      </td>
                      <td>
                        Fri Mar 24, 1995
                      </td>
                      <td class="right aligned">
                        <button class="pewter" style="margin-right:0.5em;" type="button">Message</button><button class="light" type="button"><i class="icon ion-android-delete"></i></button>
                      </td>
                    </tr>
                    <tr onclick="document.location = 'user-dashboard-thread';">
                      <td class="link">
                        Interior Design
                      </td>
                      <td>
                        Harriet Rosenthal
                      </td>
                      <td><button class="light" style="color:red;">Declined</button>
                      </td>
                      <td>
                        Tue Apr 04, 2006
                      </td>
                      <td class="right aligned">
                        <button class="pewter" style="margin-right:0.5em;" type="button">Message</button><button class="light" type="button"><i class="icon ion-android-delete"></i></button>
                      </td>
                    </tr>
                    <tr onclick="document.location = 'user-dashboard-thread';">
                      <td class="link">
                        Handyman
                      </td>
                      <td>
                        Deana Stout
                      </td>
                      <td>
                        <button class="light" style="color:green;">Approved</button>
                      </td>
                      <td>
                        Thu Nov 07, 1991
                      </td>
                      <td class="right aligned">
                        <button class="pewter" style="margin-right:0.5em;" type="button">Message</button><button class="light" type="button"><i class="icon ion-android-delete"></i></button>
                      </td>
                    </tr>
                    <tr onclick="document.location = 'user-dashboard-thread';">
                      <td class="link">
                        Electrical
                      </td>
                      <td>
                        Kellie Houston
                      </td>
                      <td>
                        <button class="light" style="color:green;">Approved</button>
                      </td>
                      <td>
                        Fri Mar 23, 2001
                      </td>
                      <td class="right aligned">
                        <button class="pewter" style="margin-right:0.5em;" type="button">Message</button><button class="light" type="button"><i class="icon ion-android-delete"></i></button>
                      </td>
                    </tr>
                    <tr onclick="document.location = 'user-dashboard-thread';">
                      <td class="link">
                        TV Mounting
                      </td>
                      <td>
                        Antonio Siegel
                      </td>
                      <td><button class="light" style="color:red;">Declined</button>
                      </td>
                      <td>
                        Fri Jun 09, 1995
                      </td>
                      <td class="right aligned">
                        <button class="pewter" style="margin-right:0.5em;" type="button">Message</button><button class="light" type="button"><i class="icon ion-android-delete"></i></button>
                      </td>
                    </tr>
                    <tr onclick="document.location = 'user-dashboard-thread';">
                      <td class="link">
                        Junk Removal
                      </td>
                      <td>
                        Trisha Weinstein
                      </td>
                      <td>
                        <button class="light" style="color:green;">Approved</button>
                      </td>
                      <td>
                        Tue Dec 21, 1999
                      </td>
                      <td class="right aligned">
                        <button class="pewter" style="margin-right:0.5em;" type="button">Message</button><button class="light" type="button"><i class="icon ion-android-delete"></i></button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </section>
              <section class="main">
                <h5 style="margin-bottom:1em;">
                  Recent services
                </h5>
                <div class="grid-4" data-columns="">
                  <div class="card">
                    <a href="/services-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/photos/prev06.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        <span>Eos voluptates sed ipsa quae</span>
                      </div>
                      <div class="card-author">
                        <a href="/seller-page">Geraldine McKenzie</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/services-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/photos/prev32.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        <span>Sit consequatur commodi explicabo qui</span>
                      </div>
                      <div class="card-author">
                        <a href="/seller-page">Constance Chan</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/services-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/photos/prev33.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        <span>Non sint unde adipisci dolores</span>
                      </div>
                      <div class="card-author">
                        <a href="/seller-page">Caitlyn Goldman</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/services-drilldown">
                      <div class="card-image card-small" style="background-image:url('/images/photos/prev23.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        <span>Enim et nesciunt excepturi ea</span>
                      </div>
                      <div class="card-author">
                        <a href="/seller-page">Billie Goldstein</a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="main">
                <h5 style="margin-bottom:1em;">
                  Favorite PROs
                </h5>
                <div class="drilldown-related">
                  <div class="grid-4" data-columns="">
                    <div class="person">
                      <div class="comment no-border" style="margin-bottom:0;">
                        <div class="comment-image">
                          <img class="square" src="http://uifaces.com/faces/_twitter/ogvidius_120.jpg" /><a class="name" href="/seller-page">Liliana Monroe</a>
                          <p>
                            43 projects
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="person">
                      <div class="comment no-border" style="margin-bottom:0;">
                        <div class="comment-image">
                          <img class="square" src="http://uifaces.com/faces/_twitter/ckor_120.jpg" /><a class="name" href="/seller-page">Pete Bowman</a>
                          <p>
                            17 projects
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="person">
                      <div class="comment no-border" style="margin-bottom:0;">
                        <div class="comment-image">
                          <img class="square" src="http://uifaces.com/faces/_twitter/russellsmith21_120.jpg" /><a class="name" href="/seller-page">Joann Heath</a>
                          <p>
                            30 projects
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="person">
                      <div class="comment no-border" style="margin-bottom:0;">
                        <div class="comment-image">
                          <img class="square" src="http://uifaces.com/faces/_twitter/odaymashalla_120.jpg" /><a class="name" href="/seller-page">Kerry Barton</a>
                          <p>
                            45 projects
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
