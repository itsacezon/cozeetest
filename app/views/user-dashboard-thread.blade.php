@extends('layouts.master')

@section('content')
<div id="content-wrap">
  <div class="panel">
    <section class="main">
      <div class="profile-nav">
        <section>
          <ul>
            <li>
              <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
            </li>
            <li>
              <a href="/user-dashboard">Dashboard</a>
            </li>
            <li>
              <a class="active" href="/user-dashboard-inbox">Inbox</a>
            </li>
            <li>
              <a href="/user-dashboard-listings">Listings</a>
            </li>
            <li>
              <a href="/user-dashboard-albums">Albums</a>
            </li>
            <li>
              <a href="/user-dashboard-storefront">Storefront</a>
            </li>
            <li>
              <a href="/user-dashboard-profile">Profile</a>
            </li>
          </ul>
        </section>
      </div>
      <div class="fixed-nav">
        <div id="seller-nav-unfixed">
        </div>
        <div id="seller-nav">
          <div class="seller-section simple slide-up">
            <div class="user">
              <div class="user-thumb smaller">
                <img src="/images/forenheit.jpg" />
              </div>
              <div class="user-info">
                <ul>
                  <li>
                    <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                  </li>
                  <li>
                    <a class="location" href="/edit-profile">Edit profile</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="drilldown-full" style="padding-top:4em;">
        <div class="drilldown-main">
          <section class="main no-top">
            <div class="drilldown-full">
              <h4 style="margin-bottom:1em;">
                Service Inbox Thread
              </h4>
              <div class="card">
                <section>
                  <div class="comment no-border" style="margin-bottom:0;">
                    <section>
                      <div class="comment-content" style="padding-left:0;padding-bottom:0;border:1px #ececec solid;border-radius:4px;background-color:#f8f8f8;">
                        <section>
                          <div class="rating-content">
                            <div class="rating-comment">
                              <p class="comment-detail">
                                Tue Feb 02, 1993
                              </p>
                              <p>
                                Et dolorem ducimus et ut officiis ut rerum reprehenderit hic aspernatur rerum voluptates. Et incidunt aut facere unde excepturi voluptatem ut dolorem quo
                              </p>
                            </div>
                          </div>
                        </section>
                      </div>
                      <div class="comment-image" style="padding-left:1em;padding-right:0;">
                        <img src="http://uifaces.com/faces/_twitter/alek_djuric_120.jpg" /><a href="/seller-page">Summer</a>
                      </div>
                    </section>
                  </div>
                  <div class="comment no-border" style="margin-bottom:0;">
                    <section>
                      <div class="comment-image">
                        <img src="/images/forenheit.jpg" /><a href="/seller-page">Forenheit Studio/Architecture</a>
                      </div>
                      <div class="comment-content" style="padding-left:0;padding-bottom:0;border:1px #ececec solid;border-radius:4px;background-color:#f8f8f8;">
                        <section>
                          <div class="rating-content">
                            <div class="rating-comment">
                              <p class="comment-detail">
                                Fri Feb 24, 1995
                              </p>
                              <p>
                                Reprehenderit quasi quia dignissimos voluptatem dignissimos. Possimus cumque libero laudantium
                              </p>
                            </div>
                          </div>
                        </section>
                      </div>
                    </section>
                  </div>
                  <div class="comment no-border" style="margin-bottom:0;">
                    <section>
                      <div class="comment-content" style="padding-left:0;padding-bottom:0;border:1px #ececec solid;border-radius:4px;background-color:#f8f8f8;">
                        <section>
                          <div class="rating-content">
                            <div class="rating-comment">
                              <p class="comment-detail">
                                Thu Aug 26, 1993
                              </p>
                              <p>
                                Voluptate omnis facilis et numquam dolore vero quasi enim. Ab quas sequi numquam adipisci veniam dolores
                              </p>
                            </div>
                          </div>
                        </section>
                      </div>
                      <div class="comment-image" style="padding-left:1em;padding-right:0;">
                        <img src="http://uifaces.com/faces/_twitter/alek_djuric_120.jpg" /><a href="/seller-page">Summer</a>
                      </div>
                    </section>
                  </div>
                  <div class="comment no-border" style="margin-bottom:0;">
                    <section>
                      <div class="comment-image">
                        <img src="/images/forenheit.jpg" /><a href="/seller-page">Forenheit Studio/Architecture</a>
                      </div>
                      <div class="comment-content" style="padding-left:0;padding-bottom:0;border:1px #ececec solid;border-radius:4px;background-color:#f8f8f8;">
                        <section>
                          <div class="rating-content">
                            <div class="rating-comment">
                              <p class="comment-detail">
                                Sat Jun 08, 1991
                              </p>
                              <p>
                                Eos porro harum est omnis sed sint aspernatur rem aliquam quia omnis temporibus aliquid. Quasi aut et ut velit aut dolores facilis minus laboriosam quae fuga et quae rerum
                              </p>
                            </div>
                          </div>
                        </section>
                      </div>
                    </section>
                  </div>
                </section>
                <section>
                  <div class="drilldown-content">
                    <div class="drilldown-heading">
                      <section class="main no-top">
                        <h5 class="pull-left">
                          Reply to this conversation
                        </h5>
                      </section>
                    </div>
                    <div class="card card-form reply" style="box-shadow:none;">
                      <div class="card-details">
                        <div class="comment no-border">
                          <div class="comment-image" style="padding-right:0;">
                            <img src="/images/forenheit.jpg" />
                          </div>
                          <div class="comment-content">
                            <form>
                              <textarea name="body" placeholder="Write your message here!" rows="2" type="text"></textarea><button class="call-to-action" type="submit">Send message</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </section>
        </div>
        <div class="drilldown-secondary">
          <div class="drilldown-card secondary">
            <div class="drilldown-content">
              <section>
                <h5>
                  About the user
                </h5>
                <div class="user">
                  <div class="user-thumb">
                    <img class="square" src="http://uifaces.com/faces/_twitter/alek_djuric_120.jpg" />
                  </div>
                  <div class="user-info">
                    <a class="name header" href="/seller-page">Summer</a>
                    <p class="location">
                      Makati, PH
                    </p>
                  </div>
                </div>
              </section>
            </div>
          </div>
          <div class="drilldown-card simple">
            <div class="drilldown-content">
              <section>
                <h5>
                  Help
                </h5>
                <ul>
                  <li>
                    <a href="#" style="font-weight:400;">Quae qui vel blanditiis</a>
                  </li>
                  <li>
                    <a href="#" style="font-weight:400;">Officia aspernatur corporis inventore</a>
                  </li>
                  <li>
                    <a href="#" style="font-weight:400;">Quibusdam error expedita perspiciatis</a>
                  </li>
                  <li>
                    <a href="#" style="font-weight:400;">Excepturi explicabo illo harum</a>
                  </li>
                  <li>
                    <a href="#" style="font-weight:400;">Nam repellat accusamus in</a>
                  </li>
                </ul>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
@stop
