@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
                <div class="seller-section highlighted">
                  <div class="seller-section-header">
                    <span>Edit listing</span>
                  </div>
                  <div class="seller-content">
                    <div class="user" style="padding:0 0.75em;padding-bottom:0.5em;">
                      <div class="user-thumb smaller">
                        <img class="square" src="/images/photos/prev22.jpg" />
                      </div>
                      <div class="user-info">
                        <a class="name" href="/services-drilldown" style="font-size:13px;">Fugit rem et delectus earum</a>
                      </div>
                    </div>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-listings-drilldown">Description</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-listings-drilldown-photos">Photos</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-listings-drilldown-products">Products</a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Help</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="#">Velit et laboriosam</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Necessitatibus cupiditate illo</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Necessitatibus dolor facere</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Error tenetur sit</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Natus alias odit</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <form action="/pro-page-my-services" method="POST">
              <section class="main no-top">
                <div class="drilldown-full">
                  <h5 style="margin-bottom:1em;">
                    Description
                  </h5>
                  <div class="drilldown-card">
                    <div class="drilldown-content">
                      <div class="ui form">
                        <div class="fields">
                          <div class="twelve wide field">
                            <label style="font-weight:500;">Listing Name</label><input name="first-name" type="text" />
                          </div>
                          <div class="four wide field">
                            <label style="font-weight:500;">Price</label>
                            <div class="product-price" style="position:relative;">
                              <span style="position:absolute;top:0.5em;left:1em;">₱</span><input name="price" style="padding-left:2.5em;" type="text" />
                            </div>
                          </div>
                        </div>
                        <div class="three fields">
                          <div class="field">
                            <label style="font-weight:500;">Location</label><input name="location" type="text" />
                          </div>
                          <div class="field">
                            <label style="font-weight:500;">Category</label>
                            <div class="ui search selection dropdown simple" style="font-size:0.85em;">
                              <input name="category" type="hidden" /><i class="dropdown icon"></i>
                              <div class="default text">
                                Select category
                              </div>
                              <div class="menu">
                                <div class="item" data-value="kitchen">
                                  Kitchen
                                </div>
                                <div class="item" data-value="bath">
                                  Bath
                                </div>
                                <div class="item" data-value="powder-room">
                                  Powder Room
                                </div>
                                <div class="item" data-value="bedroom">
                                  Bedroom
                                </div>
                                <div class="item" data-value="master-bedroom">
                                  Master bedroom
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="field">
                            <label style="font-weight:500;">Style</label>
                            <div class="ui search dropdown simple selection" style="font-size:0.85em;">
                              <input name="style" type="hidden" /><i class="dropdown icon"></i>
                              <div class="default text">
                                Select style
                              </div>
                              <div class="menu">
                                <div class="item">
                                  Traditional
                                </div>
                                <div class="item">
                                  Modern
                                </div>
                                <div class="item">
                                  Vintage
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="drilldown-card">
                    <div class="drilldown-content">
                      <div class="ui form">
                        <div class="field">
                          <label style="font-weight:500;">About the project</label><textarea name="body" placeholder="Write your description here" rows="5" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="drilldown-card">
                    <div class="drilldown-content">
                      <div class="ui form">
                        <div class="field">
                          <label style="font-weight:500;">Tags</label><input name="tags" placeholder="Seperate by comma" type="text" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <button class="call-to-action" type="submit">Save</button>
                </div>
              </section>
            </form>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
