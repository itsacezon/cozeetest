@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full">
          <div class="profile-subsection-nav">
            <div class="fixed-nav">
              <div class="seller-section simple">
                <div class="user seller">
                  <div class="user-thumb">
                    <img class="square" src="/images/seller-page-profile.jpg" />
                  </div>
                  <div class="user-info">
                    <a class="name header" href="/seller-page">Artery</a>
                    <p class="location">
                      Makati, PH
                    </p>
                  </div>
                </div>
              </div>
              <div id="seller-nav-unfixed">
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/seller-page-profile.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name header" href="/seller-page">Artery</a>
                      <p class="location">
                        Makati, PH
                      </p>
                    </div>
                  </div>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Sections</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/seller-page">All items<span class="number">40</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Kitchen<span class="number">12</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Bedroom<span class="number">17</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Master bedroom<span class="number">7</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Living<span class="number">45</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Dining<span class="number">34</span></a>
                    </li>
                    <li class="sold-items">
                      <a class="section-nav-item" href="/seller-page">Sold items<span class="number">4</span></a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Info</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/seller-page-about">About</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-reviews">Reviews
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-suggestions">Suggestions</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-policies">Policies</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <div id="profile-cover-wrap">
              <div class="profile-cover" style="background-image:url('/images/seller-page-cover.jpg');"></div>
              <div class="profile-details">
                <div class="profile-info">
                  <div class="user">
                    <div class="user-info">
                      <p class="quote">
                        We search the world to give you the best in decor and lighting.
                      </p>
                    </div>
                  </div>
                  <div class="announcement">
                    <p>
                      Spell out your wishes! Artery provide 8/12/15/24 items for you. Turnaround time is 3-7 days. Your package will be delivered within 4 to 11 business days.
                    </p>
                  </div>
                </div>
                <div class="profile-actions">
                  <button class="light pull-right" style="margin-bottom:0.5em;"><span>Favorite</span><i class="icon ion-android-favorite"></i></button>
                </div>
              </div>
            </div>
            <div class="drilldown-full">
              <div class="drilldown-suggestion">
                <ul class="badges">
                  <li>
                    <a href="#"><i class="badge trusted">
                        <div class="icon ion-ios-checkmark-outline"></div>
                      </i><span>Trusted seller</span></a>
                  </li>
                  <li>
                    <a href="#"><i class="badge favorite">
                        <div class="icon ion-ios-circle-outline"></div>
                        <div class="icon ion-android-favorite"></div>
                      </i><span>Community Fave</span></a>
                  </li>
                  <li>
                    <a href="#"><i class="badge sold">
                        <div class="icon ion-ios-circle-outline"></div>
                        <div class="icon ion-android-star"></div>
                      </i><span>Sold 1000+ items</span></a>
                  </li>
                </ul>
              </div>
              <div id="filter-wrap">
                <div id="filter-wrap-inner">
                  <div class="filters">
                    <div class="filters-category">
                      <button class="light filter-category" id="filter-category"><span>Kitchen</span><i class="icon ion-android-close"></i></button><button class="light filter-category" id="filter-location"><span>Cubao, QC</span><i class="icon ion-android-close"></i></button><button class="light filter-category" id="filter-sort"><span>Most Recent</span><i class="icon ion-android-close"></i></button>
                    </div>
                    <div class="filters-misc">
                      <h5>
                        24 <span>products</span>
                      </h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid-4" data-columns="">
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-12.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Modi eum asperiores fugiat aut
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 8021</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-9.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Quas quas excepturi sint aspernatur
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 581</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-10.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Est accusamus dolores laborum omnis
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 5949</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-3.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Nobis est consequatur asperiores aut
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 5481</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-4.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Et explicabo voluptatem placeat aut
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 2727</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Et expedita et architecto id
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 2257</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-12.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Eaque qui natus laboriosam quia
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 969</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Voluptatem hic dolorum similique voluptas
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 9455</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-9.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Et saepe beatae velit error
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 12600</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-1.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Repudiandae dolorem repudiandae tenetur nulla
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 14252</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-5.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Quae quod accusamus possimus ratione
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 4594</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-8.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Et quibusdam et molestiae dolorem
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 7581</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-9.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Tempora rerum delectus dolorum delectus
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 9920</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-6.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Ut nam repudiandae ex qui
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 5244</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-6.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Incidunt ut blanditiis distinctio in
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 9044</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-3.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Quidem est sunt suscipit aut
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 9197</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-12.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      A officia voluptas cum nostrum
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 10731</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-6.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Voluptas molestias non optio esse
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 5703</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-14.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Ut earum quaerat perspiciatis ut
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 710</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-4.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Totam ab earum quis fugit
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 4289</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-6.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Et minima quibusdam et eum
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 3779</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-5.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Explicabo molestiae reprehenderit est natus
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 8108</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-9.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Ut sint blanditiis iusto dolores
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 1134</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-12.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Sed ea quo natus adipisci
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 9099</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Qui qui et ut nulla
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 11236</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Et laboriosam quam facere qui
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 518</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-2.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Placeat quia reprehenderit est voluptatem
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 9077</span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-2.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Explicabo ipsam non porro minus
                    </div>
                    <div class="card-author">
                      <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 7158</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop

@section('scripts')
  @parent

  <script src="/javascripts/vendor/scrolltofixed.js" type="text/javascript"></script>
  <script type="text/javascript">
    $('#seller-nav').scrollToFixed({
      marginTop: 24,
      limit: $('.footer').offset().top - $('#seller-nav').outerHeight(true) - 96,
      preFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      },
      postFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').removeClass('active');
          // $(this).removeClass('no-bg');
        });
      },
      preAbsolute: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      }
    });
  </script>
@stop
