@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full">
          <div class="profile-subsection-nav">
            <div class="fixed-nav">
              <div class="seller-section simple">
                <div class="user seller">
                  <div class="user-thumb">
                    <img class="square" src="/images/seller-page-profile.jpg" />
                  </div>
                  <div class="user-info">
                    <a class="name header" href="/seller-page">Artery</a>
                    <p class="location">
                      Makati, PH
                    </p>
                  </div>
                </div>
              </div>
              <div id="seller-nav-unfixed">
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/seller-page-profile.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name header" href="/seller-page">Artery</a>
                      <p class="location">
                        Makati, PH
                      </p>
                    </div>
                  </div>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Sections</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/seller-page">All items<span class="number">49</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Kitchen<span class="number">43</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Bedroom<span class="number">41</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Master bedroom<span class="number">17</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Living<span class="number">21</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Dining<span class="number">24</span></a>
                    </li>
                    <li class="sold-items">
                      <a class="section-nav-item" href="/seller-page">Sold items<span class="number">24</span></a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Info</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/seller-page-about">About</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-reviews">Reviews
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-suggestions">Suggestions</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-policies">Policies</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <div id="profile-cover-wrap">
              <div class="profile-cover" style="background-image:url('/images/seller-page-cover.jpg');"></div>
              <div class="profile-details">
                <div class="profile-info">
                  <div class="user">
                    <div class="user-info">
                      <p class="quote">
                        We search the world to give you the best in decor and lighting.
                      </p>
                    </div>
                  </div>
                  <div class="announcement">
                    <p>
                      Spell out your wishes! Artery provide 8/12/15/24 items for you. Turnaround time is 3-7 days. Your package will be delivered within 4 to 11 business days.
                    </p>
                  </div>
                </div>
                <div class="profile-actions">
                  <button class="light pull-right" style="margin-bottom:0.5em;"><span>Favorite</span><i class="icon ion-android-favorite"></i></button>
                </div>
              </div>
            </div>
            <div class="drilldown-full">
              <div class="drilldown-card">
                <div class="carousel">
                  <div>
                    <div class="seller-cover" style="background-image:url('/images/seller-page-cover-1.jpg');">
                      <div class="seller-caption">
                        <p>
                          Commodi voluptas dolores doloribus ipsa qui laboriosam itaque et repellat tempore esse ea
                        </p>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="seller-cover" style="background-image:url('/images/seller-page-cover-2.jpg');">
                      <div class="seller-caption">
                        <p>
                          Et facere ut repellendus
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <section class="no-bot">
                  <div class="seller-content">
                    <div class="seller-content-main">
                      <h5>
                        About Artery
                      </h5>
                      <p>
                        <strong style="font-weight:600;">Makati, Philippines</strong> <span>•</span> <span>Open since November 1991</span>
                      </p>
                      <p>
                        Artery is an artist-run space currently being developed into a studio, a store, a snack bar, exhibition area, artist residency, and rooftop lounge. Operating locally in Metro Manila, Artery is engaged worldwide on contemporary issues regarding art: on its theoretical investigation and formal practice, its presence and application in the market, its impact as cultural critique and on social awareness, its expressive autonomy, its role as entertainment value, its interactive potential as platform for dialogue and change, as source of reflection, and as an alternative to prevailing creatively restricting conditions.
                      </p>
                      <p>
                        Eaque quia labore nihil iusto dolorem quia consequatur aperiam dolorem voluptatem alias. sunt rerum alias officiis voluptatum veniam non voluptas autem ipsa. autem maxime esse distinctio quia dolores illo illo non accusantium laborum molestias libero beatae. quia perspiciatis sit velit dolores ipsa totam et est provident et et ex officia ad
                      </p>
                      <p>
                        Thank you for supporting our small business!
                      </p>
                    </div>
                    <div class="seller-content-misc">
                      <div class="related-links">
                        <p class="heading">
                          <strong style="font-weight:600;">Catch us on</strong>
                        </p>
                        <ul>
                          <li>
                            <a class="link-outside" href="#">Artery.ph</a>
                          </li>
                          <li>
                            <a class="link-outside" href="#">Facebook</a>
                          </li>
                          <li>
                            <a class="link-outside" href="#">Twitter</a>
                          </li>
                        </ul>
                      </div>
                      <div class="related-links">
                        <p class="heading">
                          <strong style="font-weight:600;">Payment Methods</strong>
                        </p>
                        <ul>
                          <li>
                            <p>
                              Cash on delivery
                            </p>
                          </li>
                          <li>
                            <p>
                              Paypal
                            </p>
                          </li>
                          <li>
                            <p>
                              Wire transfer
                            </p>
                          </li>
                          <li class="link-inside">
                            <a href="/seller-page-policies"><span>Other payment methods</span> <i class="icon ion-ios-arrow-right"></i></a>
                          </li>
                        </ul>
                      </div>
                      <div class="related-links">
                        <p class="heading">
                          <strong style="font-weight:600;">Shipping</strong>
                        </p>
                        <ul>
                          <li>
                            <p>
                              International shipping
                            </p>
                          </li>
                          <li>
                            <p>
                              UPS
                            </p>
                          </li>
                          <li>
                            <p>
                              FedEx Express
                            </p>
                          </li>
                          <li class="link-inside">
                            <a href="/seller-page-policies"><span>Shipping details</span> <i class="icon ion-ios-arrow-right"></i></a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </section>
                <section>
                  <div class="drilldown-related enclosed">
                    <h5>
                      Meet Artery
                    </h5>
                    <div class="grid-4" data-columns="" style="padding:0 1em;">
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img src="http://uifaces.com/faces/_twitter/blakesimkins_120.jpg" /><a class="name" href="/seller-page">Alexa Middleton</a>
                            <p>
                              Designer
                            </p>
                          </div>
                        </div>
                        <div class="toggleable">
                          <div class="user-bio">
                            <p>
                              Commodi voluptas repudiandae dolorem sapiente aspernatur id nobis ipsa ea consectetur in ratione repellendus laudantium
                            </p>
                            <a href="/seller-page">View profile</a>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img src="http://uifaces.com/faces/_twitter/ThisIsJohnBrown_120.jpg" /><a class="name" href="/seller-page">Nia Kennedy</a>
                            <p>
                              Manager
                            </p>
                          </div>
                        </div>
                        <div class="toggleable">
                          <div class="user-bio">
                            <p>
                              Rerum omnis rerum ducimus modi temporibus debitis
                            </p>
                            <a href="/seller-page">View profile</a>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img src="http://uifaces.com/faces/_twitter/damenleeturks_120.jpg" /><a class="name" href="/seller-page">Colm O'Brien</a>
                            <p>
                              Manager
                            </p>
                          </div>
                        </div>
                        <div class="toggleable">
                          <div class="user-bio">
                            <p>
                              Ut numquam ducimus perspiciatis non aliquid
                            </p>
                            <a href="/seller-page">View profile</a>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img src="http://uifaces.com/faces/_twitter/damenleeturks_120.jpg" /><a class="name" href="/seller-page">Nia May</a>
                            <p>
                              Designer
                            </p>
                          </div>
                        </div>
                        <div class="toggleable">
                          <div class="user-bio">
                            <p>
                              Expedita et blanditiis iste amet molestiae in sit quas voluptates
                            </p>
                            <a href="/seller-page">View profile</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section>
                  <span class="heading">
                    <p>
                      Featured items
                    </p>
                  </span>
                  <div class="grid-4" data-columns="">
                    <div class="card">
                      <a href="/shop-drilldown">
                        <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                      </a>
                      <div class="card-details">
                        <div class="card-header full">
                          Accusantium quia aut ut numquam
                        </div>
                        <div class="card-author">
                          <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 1560</span>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <a href="/shop-drilldown">
                        <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-4.jpg');"></div>
                      </a>
                      <div class="card-details">
                        <div class="card-header full">
                          Omnis eligendi aut tempora non
                        </div>
                        <div class="card-author">
                          <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 638</span>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <a href="/shop-drilldown">
                        <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-2.jpg');"></div>
                      </a>
                      <div class="card-details">
                        <div class="card-header full">
                          Debitis delectus nemo possimus odit
                        </div>
                        <div class="card-author">
                          <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 4031</span>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <a href="/shop-drilldown">
                        <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-12.jpg');"></div>
                      </a>
                      <div class="card-details">
                        <div class="card-header full">
                          Voluptate molestiae qui aut iusto
                        </div>
                        <div class="card-author">
                          <a class="cut-half" href="/seller-page">Artery</a><span class="price pull-right">₱ 8499</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop

@section('scripts')
  @parent

  <script src="/javascripts/vendor/scrolltofixed.js" type="text/javascript"></script>
  <script src="/javascripts/vendor/slick.js" type="text/javascript"></script>
  <script type="text/javascript">
    $('#seller-nav').scrollToFixed({
      marginTop: 24,
      limit: $('.footer').offset().top - $('#seller-nav').outerHeight(true) - 96,
      preFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      },
      postFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').removeClass('active');
          // $(this).removeClass('no-bg');
        });
      },
      preAbsolute: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      }
    });

    $('.carousel').slick({
      infinite: true,
      dots: true,
    });
  </script>
@stop
