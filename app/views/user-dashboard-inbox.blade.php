@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Inbox</span>
                  </div>
                  <ul class="seller-content">
                    <li class="sold-items">
                      <a class="section-nav-item" href="/user-dashboard-inbox">All messages<span class="number">41</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-inbox">Approved requests<span class="number">25</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-inbox">Denied requests<span class="number">22</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-inbox">Pending requests<span class="number">26</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-inbox">Archived<span class="number">28</span></a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <section class="main no-top">
              <div class="drilldown-full">
                <div class="drilldown-card simple">
                  <div class="comment">
                    <section>
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/temonehm_120.jpg" /><a href="/seller-page">Don</a>
                      </div>
                      <div class="comment-content" style="padding-bottom:0;">
                        <div class="rating-content">
                          <div class="rating-comment">
                            <a href="/user-dashboard-thread">
                              <p style="font-size:14px;">
                                Facilis consequatur adipisci consequatur error ut aut nesciunt sit consequatur similique odio. Sit alias alias quidem qui accusantium accusantium nobis a inventore
                              </p>
                            </a>
                            <p class="comment-detail">
                              Fri Oct 19, 2001
                            </p>
                          </div>
                          <div class="rating-item-wrap">
                            <div class="rating rating-item" style="margin-left:0;">
                              <div class="rating-image" style="background-image:url('/images/photos/prev14.jpg');"></div>
                              <p>
                                <a href="/shop-drilldown">Fugit rerum velit eum</a>
                              </p>
                            </div>
                            <p style="text-align:center;font-weight:500;margin-top:1em;color:#e00007;">
                              Declined
                            </p>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                  <div class="comment">
                    <section>
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/zulsdesign_120.jpg" /><a href="/seller-page">Jamal</a>
                      </div>
                      <div class="comment-content" style="padding-bottom:0;">
                        <div class="rating-content">
                          <div class="rating-comment">
                            <a href="/user-dashboard-thread">
                              <p style="font-size:14px;">
                                Consequuntur et accusamus temporibus magnam aut fuga sed quia maxime ratione repudiandae. Numquam sapiente consectetur qui ut atque earum molestiae et unde laudantium
                              </p>
                            </a>
                            <p class="comment-detail">
                              Thu Oct 26, 1995
                            </p>
                          </div>
                          <div class="rating-item-wrap">
                            <div class="rating rating-item" style="margin-left:0;">
                              <div class="rating-image" style="background-image:url('/images/photos/prev18.jpg');"></div>
                              <p>
                                <a href="/shop-drilldown">Repellat culpa quas quas</a>
                              </p>
                            </div>
                            <p style="text-align:center;font-weight:500;margin-top:1em;color:#e00007;">
                              Declined
                            </p>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                  <div class="comment">
                    <section>
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/mcmieras_120.jpg" /><a href="/seller-page">Freya</a>
                      </div>
                      <div class="comment-content" style="padding-bottom:0;">
                        <div class="rating-content">
                          <div class="rating-comment">
                            <a href="/user-dashboard-thread">
                              <p style="font-size:14px;">
                                Aliquid non et dolorem dolore sed earum pariatur inventore saepe fugit qui dolorum harum non. Aut ut soluta quia qui sed consequatur cum vel in accusantium corrupti nisi
                              </p>
                            </a>
                            <p class="comment-detail">
                              Sun Nov 16, 2003
                            </p>
                          </div>
                          <div class="rating-item-wrap">
                            <div class="rating rating-item" style="margin-left:0;">
                              <div class="rating-image" style="background-image:url('/images/photos/prev44.jpg');"></div>
                              <p>
                                <a href="/shop-drilldown">Et eos alias et</a>
                              </p>
                            </div>
                            <p style="text-align:center;font-weight:500;margin-top:1em;color:#e00007;">
                              Declined
                            </p>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                  <div class="comment">
                    <section>
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/gerwitz_120.jpg" /><a href="/seller-page">Lori</a>
                      </div>
                      <div class="comment-content" style="padding-bottom:0;">
                        <div class="rating-content">
                          <div class="rating-comment">
                            <a href="/user-dashboard-thread">
                              <p style="font-size:14px;">
                                Iure iste voluptates dolorem. Quisquam accusamus non quae ullam voluptatibus quasi animi occaecati officiis
                              </p>
                            </a>
                            <p class="comment-detail">
                              Thu Sep 14, 2006
                            </p>
                          </div>
                          <div class="rating-item-wrap">
                            <div class="rating rating-item" style="margin-left:0;">
                              <div class="rating-image" style="background-image:url('/images/photos/prev09.jpg');"></div>
                              <p>
                                <a href="/shop-drilldown">Dolores officia sint alias</a>
                              </p>
                            </div>
                            <p style="text-align:center;font-weight:500;margin-top:1em;color:#e00007;">
                              Declined
                            </p>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
