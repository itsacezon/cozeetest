@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="profile-nav">
          <section>
            <ul>
              <li>
                <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
              </li>
              <li>
                <a href="/user-dashboard">Dashboard</a>
              </li>
              <li>
                <a href="/user-dashboard-inbox">Inbox</a>
              </li>
              <li>
                <a href="/user-dashboard-listings">Listings</a>
              </li>
              <li>
                <a class="active" href="/user-dashboard-albums">Albums</a>
              </li>
              <li>
                <a href="/user-dashboard-storefront">Storefront</a>
              </li>
              <li>
                <a href="/user-dashboard-profile">Profile</a>
              </li>
            </ul>
          </section>
        </div>
        <div class="fixed-nav">
          <div id="seller-nav-unfixed">
          </div>
          <div id="seller-nav">
            <div class="seller-section simple slide-up">
              <div class="user">
                <div class="user-thumb smaller">
                  <img src="assets/images/forenheit.jpg" />
                </div>
                <div class="user-info">
                  <ul>
                    <li>
                      <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                    </li>
                    <li>
                      <a class="location" href="/edit-profile">Edit profile</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="drilldown-full" style="padding-top:4em;">
          <h5 style="margin-bottom:1em;">
            Your favorite sellers
          </h5>
        </div>
        <div class="drilldown-full">
          <section class="main no-top">
            <div class="grid-5" data-columns="">
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403546004/annies_cqot1m.jpg" /><a class="name" href="/seller-page">Ace Hardware</a>
                    <p>
                      21 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548448/santacruz_rbtf8a.jpg" /><a class="name" href="/seller-page">Lowes</a>
                    <p>
                      33 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="http://goodlogo.com/images/logos/home_depot_logo_2776.gif" /><a class="name" href="/seller-page">RealLiving PH</a>
                    <p>
                      15 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548282/naturespath_lq26v4.jpg" /><a class="name" href="/seller-page">Home Depot</a>
                    <p>
                      49 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548255/honest_uw37mr.jpg" /><a class="name" href="/seller-page">Eden</a>
                    <p>
                      44 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="http://goodlogo.com/images/logos/home_depot_logo_2776.gif" /><a class="name" href="/seller-page">Ace Hardware</a>
                    <p>
                      49 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403546004/annies_cqot1m.jpg" /><a class="name" href="/seller-page">Honest Tea</a>
                    <p>
                      17 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548166/eden_sanmoh.jpg" /><a class="name" href="/seller-page">Eden</a>
                    <p>
                      34 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548282/naturespath_lq26v4.jpg" /><a class="name" href="/seller-page">Honest Tea</a>
                    <p>
                      49 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548166/eden_sanmoh.jpg" /><a class="name" href="/seller-page">Home Depot</a>
                    <p>
                      32 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="http://www.campbridge.com/images/cwhomedepot-logo.png" /><a class="name" href="/seller-page">Nature's Path</a>
                    <p>
                      40 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548166/eden_sanmoh.jpg" /><a class="name" href="/seller-page">Honest Tea</a>
                    <p>
                      12 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403546004/annies_cqot1m.jpg" /><a class="name" href="/seller-page">Lowes</a>
                    <p>
                      12 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548255/honest_uw37mr.jpg" /><a class="name" href="/seller-page">Lowes</a>
                    <p>
                      48 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548255/honest_uw37mr.jpg" /><a class="name" href="/seller-page">Eden</a>
                    <p>
                      39 projects
                    </p>
                  </div>
                </div>
              </div>
              <div class="person">
                <div class="comment no-border" style="margin-bottom:0;">
                  <div class="comment-image">
                    <img class="square" src="http://vector-magz.com/wp-content/uploads/2013/07/ace-hardware-logo-vector.png" /><a class="name" href="/seller-page">RealLiving PH</a>
                    <p>
                      15 projects
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </section>
    </div>
  </div>
@stop
