@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="drilldown-related enclosed" style="padding:0;margin-bottom:1.5em;">
            <section>
              <h5 style="text-align:left;margin-top:0;margin-bottom:1em;">
                Statistics
              </h5>
              <p style="margin-bottom:0;">
                Vitae consequatur inventore sapiente illo deserunt natus sit et. Tempore quaerat temporibus dicta et vel aut aut sit ad ut repellendus qui ducimus. Repellat blanditiis et eum neque mollitia voluptates in. Architecto consequatur est eveniet occaecati soluta. Adipisci rerum est ut et nam autem reiciendis quia rem est nemo dolore est consectetur
              </p>
            </section>
          </div>
        </div>
        <div class="drilldown-full">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Your listings</span>
                  </div>
                  <ul class="seller-content">
                    <li class="sold-items">
                      <a class="section-nav-item" href="#">All listings<span class="number">44</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Jobs<span class="number">17</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Products<span class="number">14</span></a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Help</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="#">Tenetur aut consequatur</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Quia aut dolor</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Et est dicta</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Ratione minima rerum</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Sunt tempore ratione</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <section class="main no-top">
              <div class="drilldown-full">
                <a class="button call-to-action pull-right" href="/user-dashboard-listings-add" style="margin-bottom:1em;">Add a listing</a>
                <h5 class="pull-right" style="margin-top:0.15em;margin-right:1em;">
                  4 <span>services</span>
                </h5>
                <button class="light pull-left" style="margin-bottom:1em;margin-right:1em;" type="button"><span>Most Popular</span><i class="icon ion-ios-arrow-down"></i></button>
                <div class="ui form pull-left">
                  <div class="inline field">
                    <input name="search" placeholder="Search ..." type="text" />
                  </div>
                </div>
              </div>
              <div class="drilldown-full">
                <section class="main">
                  <div class="comment">
                    <div class="comment-content" style="padding-left:0;padding-bottom:0;">
                      <div class="rating-content" style="position:relative;">
                        <div class="rating-item-wrap">
                          <div class="card" style="box-shadow:none;">
                            <a href="/shop-drilldown">
                              <div class="card-image" style="height:120px;background-image:url('/images/photos/prev45.jpg');border:none;"></div>
                            </a>
                          </div>
                        </div>
                        <div class="rating-comment">
                          <a href="/services-drilldown" style="font-size:1em;font-weight:500;">Laborum eum rerum occaecati possimus</a>
                          <p style="color:rgba(97, 99, 103, 0.75);">
                            Animi aut voluptatum tenetur qui quo porro ullam velit dolorem iure et ratione aspern...
                          </p>
                          <p class="comment-detail">
                            Used 5 times
                          </p>
                          <a href="/user-dashboard-listings-drilldown" style="position:absolute;bottom:1.5em;">Edit this listing</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="comment">
                    <div class="comment-content" style="padding-left:0;padding-bottom:0;">
                      <div class="rating-content" style="position:relative;">
                        <div class="rating-item-wrap">
                          <div class="card" style="box-shadow:none;">
                            <a href="/shop-drilldown">
                              <div class="card-image" style="height:120px;background-image:url('/images/photos/prev28.jpg');border:none;"></div>
                            </a>
                          </div>
                        </div>
                        <div class="rating-comment">
                          <a href="/services-drilldown" style="font-size:1em;font-weight:500;">Inventore totam neque quia id</a>
                          <p style="color:rgba(97, 99, 103, 0.75);">
                            Reiciendis repellendus a aut in aut totam tempore aut maiores quia temporibus enim re...
                          </p>
                          <p class="comment-detail">
                            Used 4 times
                          </p>
                          <a href="/user-dashboard-listings-drilldown" style="position:absolute;bottom:1.5em;">Edit this listing</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="comment">
                    <div class="comment-content" style="padding-left:0;padding-bottom:0;">
                      <div class="rating-content" style="position:relative;">
                        <div class="rating-item-wrap">
                          <div class="card" style="box-shadow:none;">
                            <a href="/shop-drilldown">
                              <div class="card-image" style="height:120px;background-image:url('/images/photos/prev27.jpg');border:none;"></div>
                            </a>
                          </div>
                        </div>
                        <div class="rating-comment">
                          <a href="/services-drilldown" style="font-size:1em;font-weight:500;">Magni voluptatibus velit corrupti nisi</a>
                          <p style="color:rgba(97, 99, 103, 0.75);">
                            Et at consequatur minus nisi nobis fugiat voluptate molestiae ad
                          </p>
                          <p class="comment-detail">
                            Used 10 times
                          </p>
                          <a href="/user-dashboard-listings-drilldown" style="position:absolute;bottom:1.5em;">Edit this listing</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="comment">
                    <div class="comment-content" style="padding-left:0;padding-bottom:0;">
                      <div class="rating-content" style="position:relative;">
                        <div class="rating-item-wrap">
                          <div class="card" style="box-shadow:none;">
                            <a href="/shop-drilldown">
                              <div class="card-image" style="height:120px;background-image:url('/images/photos/prev21.jpg');border:none;"></div>
                            </a>
                          </div>
                        </div>
                        <div class="rating-comment">
                          <a href="/services-drilldown" style="font-size:1em;font-weight:500;">Et consectetur ea non recusandae</a>
                          <p style="color:rgba(97, 99, 103, 0.75);">
                            Magni veniam inventore ipsa doloribus
                          </p>
                          <p class="comment-detail">
                            Used 6 times
                          </p>
                          <a href="/user-dashboard-listings-drilldown" style="position:absolute;bottom:1.5em;">Edit this listing</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
