<!DOCTYPE html>
<html>


<head>
  <title>Cozee - Plan, shop and hire in one place.</title>
  <meta charset="UTF-8" />
  <meta content="" name="description" />
  <meta content="width=device-width" name="viewport" />

  @section('assets')
    <script src="/javascripts/vendor/modernizr/modernizr.js" type="text/javascript"></script>
    <link href="/stylesheets/main.css" rel="stylesheet" type="text/css" />
    <link href="/stylesheets/transition.css" rel="stylesheet" type="text/css" />
    <link href="/stylesheets/dimmer.css" rel="stylesheet" type="text/css" />
    <link href="/stylesheets/modal.css" rel="stylesheet" type="text/css" />
    <link href="/stylesheets/accordion.css" rel="stylesheet" type="text/css" />
    <link href="/stylesheets/table.css" rel="stylesheet" type="text/css" />
    <link href="/stylesheets/form.css" rel="stylesheet" type="text/css" />
    <link href="/stylesheets/dropdown.css" rel="stylesheet" type="text/css" />
    <link href="/stylesheets/checkbox.css" rel="stylesheet" type="text/css" />
    <link href="/stylesheets/selectize.css" rel="stylesheet" type="text/css" />
    <link href="/stylesheets/hover-min.css" rel="stylesheet" type="text/css" />
  @show

  <link href="/favicon.ico" rel="icon" type="image/ico" />
</head>


<body>
  @section('navbar')
    <div class="no-shadow" id="nav-wrap">
      <nav>
        <a class="logotype" href="/"><img src="images/logo_full.png" /></a>
        <ul class="pull-right">
          <li>
            <div class="ui simple dropdown">
              <div class="text">
                <div class="user">
                  <div class="user-thumb">
                    <img src="images/forenheit.jpg" />
                  </div>
                  <div class="user-info">
                    <i class="icon ion-ios-arrow-down"></i>
                  </div>
                </div>
              </div>
              <div class="menu">
                <a class="item" href="/user-dashboard" style="font-size:14px;">Dashboard</a><a class="item" href="/user-account" style="font-size:14px;">Account</a>
              </div>
            </div>
          </li>
        </ul>
        <ul class="main-nav">
          <li class="link" style="margin-right:0.5em;">
            <div class="ui simple dropdown">
              <div class="text">
                <a class="nav" style="padding-left:0;padding-right:0;">What's New<i class="icon ion-ios-arrow-down" style="margin-left:0.5em;"></i></a>
              </div>
              <div class="menu">
                <a class="item" href="#" style="font-size:14px;">Staff Picks</a><a class="item" href="#" style="font-size:14px;">Popular</a><a class="item" href="#" style="font-size:14px;">Promos</a>
              </div>
            </div>
          </li>
        </ul>
      </nav>
    </div>
    <button class="hollow dark scroll-to-top {{{ Request::path() == 'services' ? 'on-category' : '' }}}"><i class="icon ion-ios-arrow-up"></i></button>
  @show

  <div id="site-content" class={{{(Request::path() == '/') ? 'on-landing' : ''}}}>
    @yield('content')
  </div>

  @include('partials.footer')

  @section('scripts')
    <script src="/javascripts/all.js" type="text/javascript"></script>
  @show
</body>


</html>
