@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="profile-nav">
          <section>
            <ul>
              <li>
                <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
              </li>
              <li>
                <a href="/user-dashboard">Dashboard</a>
              </li>
              <li>
                <a href="/user-dashboard-inbox">Inbox</a>
              </li>
              <li>
                <a href="/user-dashboard-listings">Listings</a>
              </li>
              <li>
                <a href="/user-dashboard-albums">Albums</a>
              </li>
              <li>
                <a href="/user-dashboard-storefront">Storefront</a>
              </li>
              <li>
                <a href="/user-dashboard-profile">Profile</a>
              </li>
            </ul>
          </section>
        </div>
        <div class="fixed-nav">
          <div id="seller-nav-unfixed">
          </div>
          <div id="seller-nav">
            <div class="seller-section simple slide-up">
              <div class="user">
                <div class="user-thumb smaller">
                  <img src="/images/forenheit.jpg" />
                </div>
                <div class="user-info">
                  <ul>
                    <li>
                      <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                    </li>
                    <li>
                      <a class="location" href="/edit-profile">Edit profile</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="drilldown-full" style="padding-top:4em;">
          <h5 style="margin-bottom:1em;">
            Your favorite articles
          </h5>
        </div>
        <div class="drilldown-full">
          <section class="main no-top">
            <div class="grid-3" data-columns="">
              <div class="card">
                <a href="#">
                  <div class="card-image" style="background-image:url('/images/photos/prev36.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="http://goodlogo.com/images/logos/home_depot_logo_2776.gif" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;font-size:14px;"><span>Once a Barren Rooftop, Now a Sere...</span></a><br /><a class="location" href="/seller-page">Lowes</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="#">
                  <div class="card-image" style="background-image:url('/images/photos/prev44.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="http://static.bfads.net/stores/logo/Lowes_square_large.png" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;font-size:14px;"><span>Once a Barren Rooftop, Now a Sere...</span></a><br /><a class="location" href="/seller-page">Ace Hardware</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="#">
                  <div class="card-image" style="background-image:url('/images/photos/prev06.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="https://res.cloudinary.com/relay-foods/image/upload/v1403548282/naturespath_lq26v4.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;font-size:14px;"><span>Not a Big Cook? These Fun Kitchen...</span></a><br /><a class="location" href="/seller-page">Santa Cruz</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="#">
                  <div class="card-image" style="background-image:url('/images/photos/prev23.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="https://res.cloudinary.com/relay-foods/image/upload/v1403548255/honest_uw37mr.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;font-size:14px;"><span>Not a Big Cook? These Fun Kitchen...</span></a><br /><a class="location" href="/seller-page">Eden</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="#">
                  <div class="card-image" style="background-image:url('/images/photos/prev13.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="https://res.cloudinary.com/relay-foods/image/upload/v1403546004/annies_cqot1m.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;font-size:14px;"><span>Houzz Tour: Clever DIY Tricks War...</span></a><br /><a class="location" href="/seller-page">RealLiving PH</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="#">
                  <div class="card-image" style="background-image:url('/images/photos/prev47.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="http://vector-magz.com/wp-content/uploads/2013/07/ace-hardware-logo-vector.png" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;font-size:14px;"><span>We Can Dream: An Expansive Tennes...</span></a><br /><a class="location" href="/seller-page">Ace Hardware</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="#">
                  <div class="card-image" style="background-image:url('/images/photos/prev37.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="https://res.cloudinary.com/relay-foods/image/upload/v1403548166/eden_sanmoh.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;font-size:14px;"><span>Room of the Day: A Master Bath Em...</span></a><br /><a class="location" href="/seller-page">Ace Hardware</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="#">
                  <div class="card-image" style="background-image:url('/images/photos/prev50.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="https://res.cloudinary.com/relay-foods/image/upload/v1403548166/eden_sanmoh.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name" href="#" style="line-height:1.5em;font-size:14px;"><span>We Can Dream: An Expansive Tennes...</span></a><br /><a class="location" href="/seller-page">Santa Cruz</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </section>
    </div>
  </div>
@stop
