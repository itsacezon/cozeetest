@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full">
          <div class="profile-subsection-nav">
            <div class="fixed-nav">
              <div class="seller-section simple">
                <div class="user seller">
                  <div class="user-thumb">
                    <img class="square" src="/images/seller-page-profile.jpg" />
                  </div>
                  <div class="user-info">
                    <a class="name header" href="/seller-page">Artery</a>
                    <p class="location">
                      Makati, PH
                    </p>
                  </div>
                </div>
              </div>
              <div id="seller-nav-unfixed">
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/seller-page-profile.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name header" href="/seller-page">Artery</a>
                      <p class="location">
                        Makati, PH
                      </p>
                    </div>
                  </div>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Sections</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/seller-page">All items<span class="number">26</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Kitchen<span class="number">33</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Bedroom<span class="number">20</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Master bedroom<span class="number">44</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Living<span class="number">19</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Dining<span class="number">19</span></a>
                    </li>
                    <li class="sold-items">
                      <a class="section-nav-item" href="/seller-page">Sold items<span class="number">10</span></a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Info</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/seller-page-about">About</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-reviews">Reviews
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-suggestions">Suggestions</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-policies">Policies</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <div id="profile-cover-wrap">
              <div class="profile-cover" style="background-image:url('/images/seller-page-cover.jpg');"></div>
              <div class="profile-details">
                <div class="profile-info">
                  <div class="user">
                    <div class="user-info">
                      <p class="quote">
                        We search the world to give you the best in decor and lighting.
                      </p>
                    </div>
                  </div>
                  <div class="announcement">
                    <p>
                      Spell out your wishes! Artery provide 8/12/15/24 items for you. Turnaround time is 3-7 days. Your package will be delivered within 4 to 11 business days.
                    </p>
                  </div>
                </div>
                <div class="profile-actions">
                  <button class="light pull-right" style="margin-bottom:0.5em;"><span>Favorite</span><i class="icon ion-android-favorite"></i></button>
                </div>
              </div>
            </div>
            <div class="drilldown-full">
              <div class="drilldown-card">
                <section class="no-bot">
                  <h5 style="margin-bottom:1em;">
                    Suggested PROs
                  </h5>
                  <div class="drilldown-related">
                    <div class="grid-4" data-columns="">
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403546004/annies_cqot1m.jpg" /><a class="name" href="/seller-page">Ace Hardware</a>
                            <p>
                              37 projects
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548166/eden_sanmoh.jpg" /><a class="name" href="/seller-page">Ace Hardware</a>
                            <p>
                              39 projects
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548255/honest_uw37mr.jpg" /><a class="name" href="/seller-page">Home Depot</a>
                            <p>
                              24 projects
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548282/naturespath_lq26v4.jpg" /><a class="name" href="/seller-page">Eden</a>
                            <p>
                              43 projects
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548448/santacruz_rbtf8a.jpg" /><a class="name" href="/seller-page">RealLiving PH</a>
                            <p>
                              13 projects
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="http://static.bfads.net/stores/logo/Lowes_square_large.png" /><a class="name" href="/seller-page">Annie's Homegrown</a>
                            <p>
                              34 projects
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="http://goodlogo.com/images/logos/home_depot_logo_2776.gif" /><a class="name" href="/seller-page">Ace Hardware</a>
                            <p>
                              27 projects
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="http://www.campbridge.com/images/cwhomedepot-logo.png" /><a class="name" href="/seller-page">Nature's Path</a>
                            <p>
                              12 projects
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="http://vector-magz.com/wp-content/uploads/2013/07/ace-hardware-logo-vector.png" /><a class="name" href="/seller-page">Ace Hardware</a>
                            <p>
                              35 projects
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section class="no-bot">
                  <h5 style="margin-bottom:1em;">
                    Suggested sellers
                  </h5>
                  <div class="drilldown-related">
                    <div class="grid-4" data-columns="">
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403546004/annies_cqot1m.jpg" /><a class="name" href="/seller-page">Santa Cruz</a>
                            <p>
                              34 products
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548166/eden_sanmoh.jpg" /><a class="name" href="/seller-page">Lowes</a>
                            <p>
                              39 products
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548255/honest_uw37mr.jpg" /><a class="name" href="/seller-page">Nature's Path</a>
                            <p>
                              37 products
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548282/naturespath_lq26v4.jpg" /><a class="name" href="/seller-page">Lowes</a>
                            <p>
                              30 products
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548448/santacruz_rbtf8a.jpg" /><a class="name" href="/seller-page">Annie's Homegrown</a>
                            <p>
                              45 products
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="http://static.bfads.net/stores/logo/Lowes_square_large.png" /><a class="name" href="/seller-page">Ace Hardware</a>
                            <p>
                              22 products
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="person">
                        <div class="comment no-border" style="margin-bottom:0;">
                          <div class="comment-image">
                            <img class="square" src="http://goodlogo.com/images/logos/home_depot_logo_2776.gif" /><a class="name" href="/seller-page">Santa Cruz</a>
                            <p>
                              22 products
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop

@section('scripts')
  @parent

  <script src="/javascripts/vendor/scrolltofixed.js" type="text/javascript"></script>
  <script type="text/javascript">
    $('#seller-nav').scrollToFixed({
      marginTop: 24,
      limit: $('.footer').offset().top - $('#seller-nav').outerHeight(true) - 96,
      preFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      },
      postFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').removeClass('active');
          // $(this).removeClass('no-bg');
        });
      },
      preAbsolute: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      }
    });
  </script>
@stop
