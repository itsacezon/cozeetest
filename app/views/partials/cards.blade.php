@for ($i = 0; $i < $count; $i++)
  <div class="card">
    <a href="/{{{ $page_category }}}-drilldown">
      <div class="card-image" style="background-image:url('images/{{{ ($page_category == 'services') ? 'photos' : 'products' }}}/prev{{{ image_name() }}}.jpg');">
        <div class="card-hover">
          <button class="light add">
            <i class="icon ion-plus"></i>
          </button>
          <button class="light favorite">
            <i class="icon ion-android-favorite"></i>
          </button>
        </div>
      </div>
    </a>
    <div class="card-details">
      <div class="card-header">
        <a href="/{{{ $page_category }}}-drilldown">
          {{{ ucfirst(implode(' ', $faker->words(5))) }}}
        </a>
      </div>
      <div class="card-author">
        <a href="/seller-page">{{{ $faker->name }}}</a>
        @if ($page_category == 'shop')
          <span class="price pull-right">₱ {{{ rand(0, 15000) }}}</span>
        @endif
      </div>
    </div>
  </div>
@endfor
