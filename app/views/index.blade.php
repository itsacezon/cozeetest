@extends('layouts.master')

@section('navbar')
	<div id="nav-wrap" class="on-landing scrollme">
		<div class="animateme" data-when="exit" data-from="0" data-to="0.35" data-opacity="0" data-translatey="-250">
			<a href="/" class="logotype">
				<img src="/images/logo_full_w.png" />
			</a>
			<ul class="pull-right">
				<li class="link show-register">
					<a href="#">Register</a>
				</li>
				<li class="link show-signin">
					<a href="#">Sign in</a>
				</li>
			</ul>
		</div>
	</div>
@overwrite

@section('content')
	<div class="ui modal register small basic">
		<i class="close icon ion-android-close"></i>
		<div class="drilldown-full">
			<div class="drilldown-card" style="width:70%;margin:1.5em auto;">
				<div class="drilldown-content">
					<section>
						<div class="ui form">
							<div class="field">
								<button class="block call-to-action" style="padding:0.75em;font-size:16px;background-color:#3B5998;" type="submit"><span>Register with Facebook</span><i class="icon ion-social-facebook"></i></button>
							</div>
						</div>
						<hr style="margin:1em;" />
						<div class="ui form">
							<form action="/" method="POST">
								<div class="field">
									<input name="first_name" placeholder="First Name" type="text" />
								</div>
								<div class="field">
									<input name="last_name" placeholder="Last Name" type="text" />
								</div>
								<div class="field">
									<input name="location" placeholder="Location" type="text" />
								</div>
								<div class="field">
									<input name="email" placeholder="Email Address" type="email" />
								</div>
								<div class="field">
									<input name="password" placeholder="Password" type="password" />
								</div>
								<div class="field">
									<button class="block call-to-action" style="padding:0.75em;font-size:16px;" type="submit">Register</button>
								</div>
							</form>
						</div>
					</section>
				</div>
			</div>
			<div class="drilldown-card simple" style="width:70%;margin:1.5em auto;">
				<div class="pull-right">
					<span style="font-size:14px;">On a business?</span> <a href="/register-business" style="font-weight:500;">Get started here.</a>
				</div>
			</div>
		</div>
	</div>
	<div class="ui modal signin small basic">
		<i class="close icon ion-android-close"></i>
		<div class="drilldown-full">
			<div class="drilldown-card" style="width:70%;margin:1.5em auto;">
				<div class="drilldown-content">
					<section>
						<div class="ui form">
							<div class="field">
								<button class="block call-to-action" style="padding:0.75em;font-size:16px;background-color:#3B5998;" type="submit"><span>Sign in with Facebook</span><i class="icon ion-social-facebook"></i></button>
							</div>
						</div>
						<hr style="margin:1em;" />
						<div class="ui form">
							<form action="/" method="POST">
								<div class="field">
									<input name="email" placeholder="Email Address" type="email" />
								</div>
								<div class="field">
									<input name="password" placeholder="Password" type="password" />
								</div>
								<div class="inline field">
									<div class="ui checkbox">
										<input type="checkbox" /><label style="margin-bottom:0;font-weight:400;font-size:14px;line-height:1.5;">Remember me</label>
									</div>
									<a href="/forgot-password" style="float:right;line-height:2;color:#03a9f4;">I forgot my password</a>
								</div>
								<div class="field">
									<button class="block call-to-action" style="padding:0.75em;font-size:16px;" type="submit">Sign In</button>
								</div>
							</form>
						</div>
					</section>
				</div>
			</div>
			<div class="drilldown-card simple" style="width:70%;margin:1.5em auto;">
				<div class="pull-right">
					<span style="font-size:14px;">New user?</span> <a class="get-started" href="#" style="font-weight:500;">Get started here.</a>
				</div>
			</div>
		</div>
	</div>

	<div id="content-wrap">
		<div class="panel" id="hero-panel">
			<section class="main">
				<div class="landing-section with-bg hero">
					<h1>
						Create your home<br/>the way you want it
					</h1>
					<div class="landing-container" style="width:65%;margin:2em auto;">
						<div class="ui form">
							<div class="fields">
								<div class="ten wide field" style="padding:0;">
									<input name="search-query" placeholder="What do you want for your home?" style="height:40px;border-top-right-radius:0;border-bottom-right-radius:0;border-right:none;" type="text" />
								</div>
								<div class="four wide field" style="padding:0;">
									<div class="ui simple selection dropdown" style="height:40px;padding:0.7em 1em;border-radius:0;">
										<div class="text">
											<a class="nav" style="padding-left:0;padding-right:0;font-weight:500;">Services<i class="icon ion-ios-arrow-down" style="margin-left:0.5em;"></i></a>
										</div>
										<div class="menu">
											<a class="item" href="#" style="font-size:14px;">Shop</a><a class="item" href="#" style="font-size:14px;">Articles</a><a class="item" href="#" style="font-size:14px;">Conversations</a>
										</div>
									</div>
								</div>
								<div class="two wide field" style="padding:0;">
									<button class="call-to-action" style="border-top-left-radius:0;border-bottom-left-radius:0;margin:0;padding:0 1em;height:40px;font-size:14px;width:100%;" type="button">Search</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div id="find-out-more">
				<button><i class="icon ion-ios-arrow-down"></i></button>
			</div>
		</div>
		<div class="panel" id="find-out-more-wrapper">
			<section class="main no-bot">
				<div class="landing-section scrollme">
					<div class="animateme" data-easing="easeinout" data-from="0.9" data-opacity="1" data-to="0" data-translatey="-90" data-when="enter" style="opacity:1;-webkit-transform:translate3d(0px,-90px,0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1,1,1);">
						<div class="scrollme">
							<div class="animateme" data-easing="easeinout" data-from="1" data-opacity="0" data-to="0" data-translatey="60" data-when="enter" style="margin-bottom:0.5em;opacity:0;-webkit-transform:translate3d(0px,60px,0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1,1,1);">
								<h2>
									Find everything you need for your home.
								</h2>
							</div>
						</div>
						<section class="main no-bot">
							<div class="landing-categories">
								<a class="landing-category" data-content="services" href="#">Services</a><a class="landing-category" data-content="shop" href="#">Shop</a><a class="landing-category" data-content="articles" href="#">Articles</a><a class="landing-category" data-content="conversations" href="#">Conversations</a>
							</div>
							<div class="landing-category-content-wrapper">
								<div class="landing-category-content active" data-content="services">
									<div class="grid-3" data-columns="">
										<div class="card active">
											<div class="card-overlay card-overlay-top" style="border:none;">
												<div class="card-header">
													<a href="/services-drilldown">Voluptatem aut corporis voluptas facere</a>
												</div>
											</div>
											<a href="/services-drilldown">
												<div class="card-image" style="background-image:url('/images/photos/prev36.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/bradenhamm_120.jpg" />
														</div>
														<div class="user-info" style="width:100%;">
															<a class="name" href="/seller-page">Makenzie Con...</a><br /><a class="location" href="/seller-page">5 projects</a>
															<div class="rating pull-right">
																<i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
										<div class="card active">
											<div class="card-overlay card-overlay-top" style="border:none;">
												<div class="card-header">
													<a href="/services-drilldown">Et est accusantium aut voluptas</a>
												</div>
											</div>
											<a href="/services-drilldown">
												<div class="card-image" style="background-image:url('/images/photos/prev09.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/danielhaim_120.jpg" />
														</div>
														<div class="user-info" style="width:100%;">
															<a class="name" href="/seller-page">Kaleigh Floyd</a><br /><a class="location" href="/seller-page">19 projects</a>
															<div class="rating pull-right">
																<i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
										<div class="card active">
											<div class="card-overlay card-overlay-top" style="border:none;">
												<div class="card-header">
													<a href="/services-drilldown">Beatae qui quam debitis vel</a>
												</div>
											</div>
											<a href="/services-drilldown">
												<div class="card-image" style="background-image:url('/images/photos/prev37.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/nckjrvs_120.jpg" />
														</div>
														<div class="user-info" style="width:100%;">
															<a class="name" href="/seller-page">Rogelio Glass</a><br /><a class="location" href="/seller-page">16 projects</a>
															<div class="rating pull-right">
																<i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
									<div class="landing-view-all" style="text-align:center;">
										<a class="button light" href="/services" style="margin-top:0.75em;"><span>See all services</span><i class="icon ion-ios-arrow-right"></i></a>
									</div>
								</div>
								<div class="landing-category-content" data-content="shop">
									<div class="grid-3" data-columns="">
										<div class="card">
											<div class="card-price">
												<span class="price pull-right">₱ 13164</span>
											</div>
											<div class="card-overlay card-overlay-top" style="border:none;">
												<div class="card-header">
													<a href="/shop-drilldown">Soluta maxime possimus est amet</a>
												</div>
											</div>
											<a href="/shop-drilldown">
												<div class="card-image" style="background-image:url('/images/products/prev25.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user pull-left">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/renbyrd_120.jpg" />
														</div>
														<div class="user-info">
															<a class="name" href="/seller-page">Dalton Schro...</a><br /><a class="location" href="/seller-page">5 projects</a>
														</div>
													</div>
													<div class="rating pull-right" style="margin-top:1.5em;">
														<i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i>
													</div>
												</section>
											</div>
										</div>
										<div class="card">
											<div class="card-price">
												<span class="price pull-right">₱ 10745</span>
											</div>
											<div class="card-overlay card-overlay-top" style="border:none;">
												<div class="card-header">
													<a href="/shop-drilldown">Voluptatem in voluptas dolor tempore</a>
												</div>
											</div>
											<a href="/shop-drilldown">
												<div class="card-image" style="background-image:url('/images/products/prev08.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user pull-left">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/deimler_120.jpg" />
														</div>
														<div class="user-info">
															<a class="name" href="/seller-page">Toby May</a><br /><a class="location" href="/seller-page">6 projects</a>
														</div>
													</div>
													<div class="rating pull-right" style="margin-top:1.5em;">
														<i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
													</div>
												</section>
											</div>
										</div>
										<div class="card">
											<div class="card-price">
												<span class="price pull-right">₱ 13708</span>
											</div>
											<div class="card-overlay card-overlay-top" style="border:none;">
												<div class="card-header">
													<a href="/shop-drilldown">Hic nam sed et est</a>
												</div>
											</div>
											<a href="/shop-drilldown">
												<div class="card-image" style="background-image:url('/images/products/prev27.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user pull-left">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/Fitehal_120.jpg" />
														</div>
														<div class="user-info">
															<a class="name" href="/seller-page">inley McNamara</a><br /><a class="location" href="/seller-page">16 projects</a>
														</div>
													</div>
													<div class="rating pull-right" style="margin-top:1.5em;">
														<i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
													</div>
												</section>
											</div>
										</div>
									</div>
									<div class="landing-view-all" style="text-align:center;">
										<a class="button light" href="/shop"><span>See all products</span><i class="icon ion-ios-arrow-right"></i></a>
									</div>
								</div>
								<div class="landing-category-content" data-content="articles">
									<div class="grid-3" data-columns="">
										<div class="card">
											<a href="/articles-drilldown">
												<div class="card-image" style="background-image:url('/images/photos/prev31.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/jwphillips_120.jpg" />
														</div>
														<div class="user-info">
															<a class="name" href="/articles-drilldown" style="line-height:1.5em;"><span>We Can Dream: An Expansive ...</span></a><br /><a class="location" href="/seller-page">Real Living PH</a>
														</div>
													</div>
												</section>
											</div>
										</div>
										<div class="card">
											<a href="/articles-drilldown">
												<div class="card-image" style="background-image:url('/images/photos/prev03.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/jcarlosweb_120.jpg" />
														</div>
														<div class="user-info">
															<a class="name" href="/articles-drilldown" style="line-height:1.5em;"><span>Houzz Tour: Clever DIY Tric...</span></a><br /><a class="location" href="/seller-page">Real Living PH</a>
														</div>
													</div>
												</section>
											</div>
										</div>
										<div class="card">
											<a href="/articles-drilldown">
												<div class="card-image" style="background-image:url('/images/photos/prev12.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/NastyaVZ_120.jpg" />
														</div>
														<div class="user-info">
															<a class="name" href="/articles-drilldown" style="line-height:1.5em;"><span>Room of the Day: A Master B...</span></a><br /><a class="location" href="/seller-page">Real Living PH</a>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
									<div class="landing-view-all" style="text-align:center;">
										<a class="button light" href="/articles"><span>See all articles</span><i class="icon ion-ios-arrow-right"></i></a>
									</div>
								</div>
								<div class="landing-category-content" data-content="conversations">
									<div class="grid-3" data-columns="">
										<div class="card">
											<a href="/conversations-drilldown">
												<div class="card-image" style="background-image:url('/images/photos/prev41.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/decarola_120.jpg" />
														</div>
														<div class="user-info">
															<a class="name" href="#" style="line-height:1.5em;"><span>Need more help with frustra...</span></a><br /><a class="location" href="/seller-page">Terry Gray</a>
														</div>
													</div>
												</section>
											</div>
										</div>
										<div class="card">
											<a href="/conversations-drilldown">
												<div class="card-image" style="background-image:url('/images/photos/prev32.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/gilbertglee_120.jpg" />
														</div>
														<div class="user-info">
															<a class="name" href="#" style="line-height:1.5em;"><span>Kitchen and (me) need help ...</span></a><br /><a class="location" href="/seller-page">Phil Fischer</a>
														</div>
													</div>
												</section>
											</div>
										</div>
										<div class="card">
											<a href="/conversations-drilldown">
												<div class="card-image" style="background-image:url('/images/photos/prev31.jpg');border:none;"></div>
											</a>
											<div class="card-details">
												<section>
													<div class="user">
														<div class="user-thumb smaller">
															<img src="http://uifaces.com/faces/_twitter/Anotherdagou_120.jpg" />
														</div>
														<div class="user-info">
															<a class="name" href="#" style="line-height:1.5em;"><span>Need help with a narrow liv...</span></a><br /><a class="location" href="/seller-page">Arturo Goldman</a>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
									<div class="landing-view-all" style="text-align:center;">
										<a class="button light" href="/conversations"><span>See all conversations</span><i class="icon ion-ios-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</section>
		</div>
		<div class="panel">
			<section class="main no-top" style="margin-top:0.75em;">
				<div class="landing-section with-bg banner" style="background-image:url('/images/landing/landing_9.jpg');background-position: center bottom;">
					<h3 class="pull-left" style="text-align:left;">
						Build your dream house now.
					</h3>
					<a class="pull-right button call-to-action" href="#" style="margin-top:1em;padding:1em 3em;">Register</a>
				</div>
			</section>
		</div>
		<div class="panel">
			<section class="main no-top">
				<div class="landing-section scrollme" style="margin-bottom:1.5em;">
					<div class="animateme" data-easing="easeinout" data-from="0.9" data-opacity="1" data-to="0" data-translatey="-90" data-when="enter" style="opacity:1;-webkit-transform:translate3d(0px,-90px,0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1,1,1);">
						<div class="scrollme">
							<div class="animateme" data-easing="easeinout" data-from="1" data-opacity="0" data-to="0" data-translatey="60" data-when="enter" style="margin-bottom:0.5em;opacity:0;-webkit-transform:translate3d(0px,60px,0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1,1,1);">
								<h2>
									Discover categories suited for every room
								</h2>
							</div>
						</div>
						<section class="main increased-space">
							<div class="grid-uneven">
								<div class="grid-column size-1of3">
									<div class="card">
										<a href="/services-category">
											<div class="card-image card-featured-image" style="border:none;background-image:url('http://cdn.sheknows.com/articles/2012/10/sarah_parenting/cleaning.jpg');">
												<div class="overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,0.1);">
													<h2 style="position:absolute;width:100%;text-align:center;top:45%;color:white;font-weight:400;text-shadow:0 1px 6px rgba(0,0,0,0.65);">
														House Cleaning
													</h2>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="grid-column size-2of3">
									<div class="card">
										<a href="/services-category">
											<div class="card-image card-featured-image" style="border:none;background-image:url('http://upload.wikimedia.org/wikipedia/commons/3/30/Quezon_City_Circle_Monument.jpg');">
												<h2 style="position:absolute;width:100%;text-align:left;top:45%;left:1em;color:white;font-weight:400;text-shadow:0 1px 6px rgba(0,0,0,0.65);">
													Quezon City
												</h2>
												<button class="pewter" style="position:absolute;right:18.5em;bottom:1.5em;" type="button">27 pros</button>
											</div>
										</a>
										<div class="card-details card-details--right">
											<div class="card-header full" style="margin-bottom:1em;">
												<h5>
													<i class="icon ion-map" style="display:block;font-size:2.5em;"></i>City Data
												</h5>
											</div>
											<div class="card-copy">
												<p class="heading" style="font-weight:600;">
													Popular services
												</p>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">House Cleaning,</a> </span>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Landscaping,</a> </span>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Interior Design,</a> </span>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Handyman,</a> </span>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Electrical,</a> </span>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">TV Mounting,</a> </span>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Junk Removal,</a> </span>
												<p class="heading" style="margin-top:0.75em;font-weight:600;">
													Popular products
												</p>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Kitchen,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Bath,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Powder Room,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Bedroom,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Master bedroom,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Living,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Dining,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Kids,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Loft,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Laundry and Utility,</a> </span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="grid-3">
								<div class="grid-column size-1of3">
									<div class="card">
										<a href="/shop-category">
											<div class="card-image card-featured-image" style="border:none;background-image:url('http://homemydesign.com/wp-content/uploads/2012/10/storage-dining-room-ideas.jpg');">
												<div class="overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,0.1);">
													<div class="field" style="position:absolute;width:100%;text-align:center;top:40%;color:white;">
														<h2 style="font-weight:400;text-shadow:0 1px 6px rgba(0,0,0,0.35);">
															Dining
														</h2>
														<button class="yellow" style="margin-top:1em;" type="button">14 items</button>
													</div>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="grid-column size-1of3">
									<div class="card">
										<a href="/services-category">
											<div class="card-image card-featured-image" style="border:none;background-image:url('http://reo.trulynobleservices.com/files/2010/12/Landscaping3.jpg');">
												<div class="overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,0.1);">
													<h2 style="position:absolute;width:100%;text-align:center;top:45%;color:white;font-weight:400;text-shadow:0 1px 6px rgba(0,0,0,0.65);">
														Landscaping
													</h2>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="grid-column size-1of3">
									<div class="card">
										<a href="/shop-category">
											<div class="card-image card-featured-image" style="border:none;background-image:url('http://uktv.co.uk/images/standarditem/EX1/24995_EX1.jpg');">
												<div class="overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,0.1);">
													<div class="field" style="position:absolute;width:100%;text-align:center;top:40%;color:white;">
														<h2 style="font-weight:400;text-shadow:0 1px 6px rgba(0,0,0,0.65);">
															Bathroom
														</h2>
														<button class="yellow" style="margin-top:1em;" type="button">25 items</button>
													</div>
												</div>
											</div>
										</a>
									</div>
								</div>
							</div>
							<div class="grid-uneven">
								<div class="grid-column size-2of3">
									<div class="card">
										<a href="/services-category">
											<div class="card-image card-featured-image" style="border:none;background-image:url('http://fc04.deviantart.net/fs26/i/2008/031/b/4/MAKATI_CITY_SKYLINE_by_astroworks.jpg');">
												<h2 style="position:absolute;width:100%;text-align:left;top:45%;left:1em;color:white;font-weight:400;text-shadow:0 1px 6px rgba(0,0,0,0.65);">
													Mandaluyong City
												</h2>
												<button class="pewter" style="position:absolute;right:18.5em;bottom:1.5em;" type="button">42 pros</button>
											</div>
										</a>
										<div class="card-details card-details--right">
											<div class="card-header full" style="margin-bottom:1em;">
												<h5>
													<i class="icon ion-map" style="display:block;font-size:2.5em;"></i>City Data
												</h5>
											</div>
											<div class="card-copy">
												<p class="heading" style="font-weight:600;">
													Popular services
												</p>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Aquarium Services,</a> </span>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Asbestos Removal,</a> </span>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Attic Remodel,</a> </span>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Basement Remodel,</a> </span>
												<span><a href="/services-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Bed Bug Extermination,</a> </span>
												<p class="heading" style="margin-top:0.75em;font-weight:600;">
													Popular products
												</p>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Art Zines,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Collage & Mixed Media,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Custom Portraits,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Decorative Arts,</a> </span>
												<span><a href="/shop-category" style="color:#525457;margin-bottom:1em;font-size:12px;line-height:1.5;">Drawing & Illustration,</a> </span>
											</div>
										</div>
									</div>
								</div>
								<div class="grid-column size-1of3">
									<div class="card">
										<a href="/shop-category">
											<div class="card-image card-featured-image" style="border:none;background-image:url('http://cdn.decorenable.com/images/homemydesign.com/wp-content/uploads/2013/03/colorful-kids-playroom-ideas.jpg');">
												<div class="overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,0.1);">
													<div class="field" style="position:absolute;width:100%;text-align:center;top:40%;color:white;">
														<h2 style="font-weight:400;text-shadow:0 1px 6px rgba(0,0,0,0.65);">
															Kids
														</h2>
														<button class="yellow" style="margin-top:1em;" type="button">9 items</button>
													</div>
												</div>
											</div>
										</a>
									</div>
								</div>
							</div>
							<div class="grid-3">
								<div class="grid-column size-1of3">
									<div class="card">
										<a href="/services-category">
											<div class="card-image card-featured-image" style="border:none;background-image:url('http://rockymountainenergy.net/foto/big/electrical.jpg');">
												<div class="overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,0.1);">
													<h2 style="position:absolute;width:100%;text-align:center;top:45%;color:white;font-weight:400;text-shadow:0 1px 6px rgba(0,0,0,0.65);">
														Electrical
													</h2>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="grid-column size-1of3">
									<div class="card">
										<a href="/shop-category">
											<div class="card-image card-featured-image" style="border:none;background-image:url('http://c0263062.cdn.cloudfiles.rackspacecloud.com/content/images/closet-storage-your-laundry-room_a2711e2b03e75b0a76bbbced3c0e26fb.jpg');">
												<div class="overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,0.1);">
													<div class="field" style="position:absolute;width:100%;text-align:center;top:40%;color:white;">
														<h2 style="font-weight:400;text-shadow:0 1px 6px rgba(0,0,0,0.35);">
															Storage and Closets
														</h2>
														<button class="yellow" style="margin-top:1em;" type="button">4 items</button>
													</div>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="grid-column size-1of3">
									<div class="card">
										<a href="/services-category">
											<div class="card-image card-featured-image" style="border:none;background-image:url('http://www.oregon-homesforsale.com/wp-content/uploads/2012/11/Handyman-Services.jpg');">
												<div class="overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,0.1);">
													<h2 style="position:absolute;width:100%;text-align:center;top:45%;color:white;font-weight:400;text-shadow:0 1px 6px rgba(0,0,0,0.65);">
														Handyman
													</h2>
												</div>
											</div>
										</a>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</section>
		</div>
		<div class="panel panel-light panel--800" id="featured-section">
			<a class="button light" href="#" id="panel-accordion" style="z-index:3;transition:450ms;position:absolute;top:2em;right:2em;"><i class="icon ion-android-arrow-back"></i></a>
			<div class="accordion">
				<div class="landing-section with-bg" style="position:relative;height:100%;">
					<section>
						<a class="accordion-nav-item" href="/seller-page-about">About</a><a class="accordion-nav-item" href="/seller-page-about">Meet the team</a><a class="accordion-nav-item" href="/seller-page-reviews">Reviews
							<div class="rating pull-right">
								<i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
							</div>
						</a>
					</section>
					<section>
						<a class="button yellow block" href="/seller-page" style="margin-top:0.25em">Go to Artery's page</a>
					</section>
					<section style="text-align:left;padding:2.5em;width:100%;position:absolute;margin:0 auto;left:0;top:14em;">
						<a class="accordion-nav-item" href="/seller-page-suggestions" style="font-weight:600;margin-bottom:0.5em;">
							Artery recommends
						</a>
						<ul class="products">
							@for($i = 0; $i < 6; $i++)
								<li class="product" style="display:inline-block;">
									<div class="comment no-border">
										<div class="comment-image" style="padding-right:0.5em;padding-bottom:0;">
											<img class="square" src="/images/seller-products/seller-product-{{ $i }}.jpg" style="height:54px;width:54px;" />
										</div>
									</div>
								</li>
							@endfor
						</ul>
					</section>
					<section style="text-align:left;padding:2.5em;width:100%;position:absolute;margin:0 auto;left:0;bottom:16em;">
						<p class="accordion-nav-item" style="font-weight:600;">
							Payment
						</p>
					</section>
					<section style="text-align:left;padding:2.5em;width:100%;position:absolute;margin:0 auto;left:0;bottom:13.5em;">
						<p class="accordion-nav-item" style="font-weight:600;">
							Shipping
						</p>
					</section>
					<section style="text-align:left;padding:2.5em;width:100%;position:absolute;margin:0 auto;left:0;bottom:11em;">
						<p class="accordion-nav-item" style="font-weight:600;">
							FAQs
						</p>
					</section>
				</div>
			</div>
			<div class="panel-half" id="featured-seller"></div>
			<div class="panel-half" id="featured-seller-desc">
				<div class="landing-section with-bg">
					<section style="position:absolute;top:25%;padding:2em;">
						<a href="/seller-page">
							<div class="user" style="margin:0 auto;margin-bottom:1em;">
								<div class="user-thumb solo huge">
									<img src="/images/seller-page-profile.jpg" style="border:2px white solid;" />
								</div>
							</div>
						</a>
						<h2 style="font-size:2.5em;font-weight:400;">
							Meet Artery
						</h2>
						<div class="card-description">
							<p style="padding:1em 2em;color:rgba(255,255,255,0.85);font-size:18px;font-weight:400;line-height:26px;">
								Artery is an artist-run space currently being developed into a studio, a store, a snack bar, exhibition area, artist residency, and rooftop lounge.
							</p>
						</div>
					</section>
				</div>
			</div>
		</div>
		<div class="panel">
			<section class="main increased-space">
				<div class="landing-section scrollme" style="margin-bottom:1em;">
					<div class="animateme" data-easing="easeinout" data-from="0.9" data-opacity="1" data-to="0" data-translatey="-80" data-when="enter" style="opacity:1;-webkit-transform:translate3d(0px,-80px,0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1,1,1);">
						<div class="scrollme">
							<div class="animateme" data-easing="easeinout" data-from="1" data-opacity="0" data-to="0" data-translatey="70" data-when="enter" style="margin-bottom:0.5em;opacity:0;-webkit-transform:translate3d(0px,70px,0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1,1,1);">
								<h2>
									Know how Cozee works
								</h2>
							</div>
						</div>
						<section class="main increased-space no-bot">
							<div class="landing-section-full">
								<div class="landing-subsection" style="background-image:url('/images/landing/landing_section_1.jpg');">
									<div class="landing-subsection-content trust">
										<h3>
											Trust
										</h3>
										<p>
											Fugiat laudantium recusandae vel ipsum dolores. Maiores odio perspiciatis rerum
										</p>
									</div>
								</div>
								<div class="landing-subsection discover" style="background-image:url('/images/landing/landing_section_2.jpg');">
									<div class="landing-subsection-content discover">
										<h3>
											Discover
										</h3>
										<p>
											Vel fugiat vitae enim natus ut corrupti eveniet iusto. Voluptatem praesentium iure accusantium
										</p>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</section>
		</div>
	</div>
@stop

@section('scripts')
	@parent
	<script src="/javascripts/vendor/scrollme.js" type="text/javascript"></script>
	<script src="/javascripts/form.js" type="text/javascript"></script>
	<script src="/javascripts/checkbox.js" type="text/javascript"></script>
	<script src="/javascripts/dropdown.js" type="text/javascript"></script>
	<script src="/javascripts/transition.js" type="text/javascript"></script>
	<script src="/javascripts/dimmer.js" type="text/javascript"></script>
	<script src="/javascripts/modal.js" type="text/javascript"></script>

	<script type="text/javascript">
		$('.scroll-to-top').hide();

		$('#find-out-more').click(function() {
			$("html, body").animate({ scrollTop: $('#find-out-more-wrapper').offset().top }, 1500);
		});

		$('#panel-accordion').click(function(e) {
			e.preventDefault();
			var panelButton = $(this);
			if ( $('#featured-section').css("left") == "0px") {
				$('#featured-section').css({
					"left": -280
				});
				panelButton.find('i').removeClass('ion-android-arrow-back').addClass('ion-android-arrow-forward')
			}
			else {
				$('#featured-section').css({
					"left": 0
				});
				panelButton.find('i').removeClass('ion-android-arrow-forward').addClass('ion-android-arrow-back')
			}
		});

		var categoriesWrapper = $('.landing-category-content-wrapper'),
				mainCategories = $('.landing-categories a');
		$('.landing-category').click(function(e) {
			e.preventDefault();
			var selectedItem = $(this);
			if( !selectedItem.hasClass('active') ) {
				var category = selectedItem.data('content'),
						selectedContent = categoriesWrapper.find('.landing-category-content[data-content="' + category + '"]');

				mainCategories.addClass('inactive').removeClass('active');
				selectedItem.removeClass('inactive').addClass('active');
				selectedContent.addClass('active').siblings('.landing-category-content').removeClass('active').find('.card').removeClass('active');
				selectedContent.find('.card').addClass('active');
			}
		});

		$(window).scroll(function() {
			var windowContainer = $(this);
			var scrollThresh = 150;

			if (windowContainer.scrollTop() > scrollThresh) {
				var activeContent = categoriesWrapper.find('.landing-category-content.active'),
						activeItem = activeContent.data('content');
				mainCategories.addClass('inactive');
				$('.landing-category[data-content="' + activeItem + '"]').addClass('active').removeClass('inactive');
			}
			else {
				$('.landing-category').removeClass('active').siblings('.landing-category').removeClass('inactive');
			}
		});

		$('.ui.dropdown').dropdown();
		$('.ui.checkbox').checkbox();

		$('.get-started').click(function() {
			$('.ui.modal.signin').modal('close');
			$('.ui.modal.register').modal('show');
		});

		$('.show-signin').click(function() {
			$('.ui.modal.signin').modal('show');
		});

		$('.show-register').click(function() {
			$('.ui.modal.register').modal('show');
		});
	</script>
@stop
