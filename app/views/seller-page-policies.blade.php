@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full">
          <div class="profile-subsection-nav">
            <div class="fixed-nav">
              <div class="seller-section simple">
                <div class="user seller">
                  <div class="user-thumb">
                    <img class="square" src="/images/seller-page-profile.jpg" />
                  </div>
                  <div class="user-info">
                    <a class="name header" href="/seller-page">Artery</a>
                    <p class="location">
                      Makati, PH
                    </p>
                  </div>
                </div>
              </div>
              <div id="seller-nav-unfixed">
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/seller-page-profile.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name header" href="/seller-page">Artery</a>
                      <p class="location">
                        Makati, PH
                      </p>
                    </div>
                  </div>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Sections</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/seller-page">All items<span class="number">25</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Kitchen<span class="number">30</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Bedroom<span class="number">41</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Master bedroom<span class="number">25</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Living<span class="number">50</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Dining<span class="number">23</span></a>
                    </li>
                    <li class="sold-items">
                      <a class="section-nav-item" href="/seller-page">Sold items<span class="number">12</span></a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Info</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/seller-page-about">About</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-reviews">Reviews
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-suggestions">Suggestions</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-policies">Policies</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <div id="profile-cover-wrap">
              <div class="profile-cover" style="background-image:url('/images/seller-page-cover.jpg');"></div>
              <div class="profile-details">
                <div class="profile-info">
                  <div class="user">
                    <div class="user-info">
                      <p class="quote">
                        We search the world to give you the best in decor and lighting.
                      </p>
                    </div>
                  </div>
                  <div class="announcement">
                    <p>
                      Spell out your wishes! Artery provide 8/12/15/24 items for you. Turnaround time is 3-7 days. Your package will be delivered within 4 to 11 business days.
                    </p>
                  </div>
                </div>
                <div class="profile-actions">
                  <button class="light pull-right" style="margin-bottom:0.5em;"><span>Favorite</span><i class="icon ion-android-favorite"></i></button>
                </div>
              </div>
            </div>
            <div class="drilldown-full">
              <div class="drilldown-card">
                <section>
                  <h5 style="margin-bottom:1em;">
                    Shop Policies
                  </h5>
                  <div class="profile-policy">
                    <div class="profile-policy-header">
                      <strong>Payment</strong>
                    </div>
                    <div class="profile-policy-content">
                      <p>
                        I accept Paypal instant payment and payment through Etsy. For all custom orders, i require full payment before your project commences.
                      </p>
                      <p>
                        For Etsy custom orders, request a custom order and i will create a listing outlining the terms and specs we've discussed.
                      </p>
                      <p>
                        For Paypal payment, i will send a detailed invoice outlining the custom terms we've discussed.
                      </p>
                    </div>
                  </div>
                  <div class="profile-policy">
                    <div class="profile-policy-header">
                      <strong>Shipping</strong>
                    </div>
                    <div class="profile-policy-content">
                      <p>
                        For smaller items i use UPS ground. Tracking numbers updated via etsy on item shipment.
                      </p>
                      <p>
                        For larger items, i ship via USHIP blanket service or will ship a crated item. I will notify you about shipping times and provide you with information about your carrier and estimated delivery time.
                      </p>
                      <p>
                        Uship "blanket service" will deliver directly into your residence, and is considered a white glove service.
                      </p>
                      <p>
                        "Crating" will be provided via a freight carrier and is normally a curbside delivery. I will provide you with any neccessary information about this process in the event a crate is a more appropriate shipment scenario.
                      </p>
                    </div>
                  </div>
                  <div class="profile-policy">
                    <div class="profile-policy-header">
                      <strong>Refunds and Exchanges</strong>
                    </div>
                    <div class="profile-policy-content">
                      <p>
                        I stand behind my furniture and the workmanship involved. My goal is happy and repeat customers. There are two elements to that puzzle; trust and customer satisfaction. I hope to exceed your expectations on both of these fronts and offer the following warranty terms:
                      </p>
                      <p>
                        I guarantee my products for a period of 90 days in respect to any workmanship related defects. I will repair or replace items during this period, but the buyer is responsible for shipping expenses.
                      </p>
                      <p>
                        This does not include cracking/splitting of wood as this is out of my control, but glue joints and screw joints are covered, as well as finish related issues. Warping, cupping, bowing are wood movement issues and environmental issues and are also not covered under this warranty.
                      </p>
                      <p>
                        Normal wear and tear is not covered under this warranty.
                      </p>
                      <p>
                        Refunds are not available for custom order items. You will be involved in every step of the custom order fabrication, so it is very unlikely that there will be an element to your piece you're not expecting. If there is something you would like addressed after receiving your piece, i will work with you to address any issues you may have.
                      </p>
                      <p>
                        Refunds are not available for wood color, tone, knots or naturally occurring inclusions. Please be aware we are dealing with a natural material and no two pieces are alike, i try my best to match the color(s) in my listing but it is impossible to match a listing photo exactly. You should expect your piece to vary in color and tone to the any and all listing photos.
                      </p>
                      <p>
                        In unlikely event damage is sustained by the carrier during transit, i will work with the carrier to submit a claim for the damage incurred which i will pass on to you.
                      </p>
                      <p>
                        If a repair is deemed necessary, i will repair it at no cost. Buyer is responsible for any and all shipping related charges.
                      </p>
                    </div>
                  </div>
                  <div class="profile-policy">
                    <div class="profile-policy-header">
                      <strong>Additional Policies and FAQs</strong>
                    </div>
                    <div class="profile-policy-content">
                      <p>
                        My favorite customers are those which are passionate about furniture. I welcome your input in the custom order process, and your input and ideas will be implemented into the piece. This is one of the great benefits of the Etsy platform that huge furniture retailers can't offer...a one of kind piece with your own personal touch.
                      </p>
                    </div>
                  </div>
                  <div class="profile-policy">
                    <div class="profile-policy-header">
                      <strong>Seller Information</strong>
                    </div>
                    <div class="profile-policy-content">
                      <p>
                        Handcrafted in the USA in Cleveland, Ohio
                      </p>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop

@section('scripts')
  @parent

  <script src="/javascripts/vendor/scrolltofixed.js" type="text/javascript"></script>
  <script type="text/javascript">
    $('#seller-nav').scrollToFixed({
      marginTop: 24,
      limit: $('.footer').offset().top - $('#seller-nav').outerHeight(true) - 96,
      preFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      },
      postFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').removeClass('active');
          // $(this).removeClass('no-bg');
        });
      },
      preAbsolute: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      }
    });
  </script>
@stop
