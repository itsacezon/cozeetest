@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Your dashboard</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-services">Services</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-orders">Orders</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-purchases">Purchases<span class="number">21</span></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <div class="drilldown-full">
              <section class="main no-top">
                <h5 style="margin-bottom:1em;">
                  Notifications
                </h5>
                <div class="drilldown-card">
                  <div class="comment" style="margin-bottom:0;">
                    <section>
                      <div class="comment-content" style="padding-left:0;padding-bottom:0;">
                        <p class="comment-detail">
                          Sun Dec 19, 1999
                        </p>
                        <a href="/user-dashboard-thread">
                          <p style="font-size:14px;">
                            Dolorum perferendis iusto eum voluptatem ut nesciunt aut et excepturi ipsa voluptatibus eligendi in. Possimus voluptatum ad molestiae at molestiae voluptatibus earum nam modi tenetur exercitationem reiciendis
                          </p>
                        </a>
                      </div>
                    </section>
                  </div>
                  <div class="comment" style="margin-bottom:0;">
                    <section>
                      <div class="comment-content" style="padding-left:0;padding-bottom:0;">
                        <p class="comment-detail">
                          Sun Nov 27, 2005
                        </p>
                        <a href="/user-dashboard-thread">
                          <p style="font-size:14px;">
                            Id officia voluptatibus saepe molestiae nostrum possimus. Et sed ut excepturi quo doloribus
                          </p>
                        </a>
                      </div>
                    </section>
                  </div>
                  <div class="comment" style="margin-bottom:0;">
                    <section>
                      <div class="comment-content" style="padding-left:0;padding-bottom:0;">
                        <p class="comment-detail">
                          Sat Jun 05, 1993
                        </p>
                        <a href="/user-dashboard-thread">
                          <p style="font-size:14px;">
                            Facilis earum asperiores voluptas aperiam suscipit nisi et voluptate quidem et consequatur ut quas amet. Voluptas rem voluptas dignissimos quis temporibus est ipsa mollitia consequuntur assumenda quod praesentium
                          </p>
                        </a>
                      </div>
                    </section>
                  </div>
                </div>
              </section>
              <section class="main">
                <h5 style="margin-bottom:1em;">
                  Recent favorites
                </h5>
                <div class="grid-4" data-columns="">
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('images/seller-products/seller-product-9.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header pull-left">
                        <span class="cut-half pull-left">Suscipit dolores porro laudantium laborum</span><span class="price pull-right">₱ 4960</span>
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Jackie Golden</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/services-drilldown">
                      <div class="card-image card-small" style="background-image:url('images/photos/prev03.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Voluptatum nihil assumenda assumenda dignissimos
                      </div>
                      <div class="card-author">
                        <a href="/seller-page">Gordon Connolly</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('images/seller-products/seller-product-11.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header pull-left">
                        <span class="cut-half pull-left">Eos sint sed rerum provident</span><span class="price pull-right">₱ 2331</span>
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Anahi Dougherty</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/services-drilldown">
                      <div class="card-image card-small" style="background-image:url('images/photos/prev03.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Quisquam dolores molestiae error culpa
                      </div>
                      <div class="card-author">
                        <a href="/seller-page">Moses Bowman</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/services-drilldown">
                      <div class="card-image card-small" style="background-image:url('images/photos/prev15.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Perspiciatis eius ex est officiis
                      </div>
                      <div class="card-author">
                        <a href="/seller-page">Lexi Whitaker</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/shop-drilldown">
                      <div class="card-image card-small" style="background-image:url('images/seller-products/seller-product-12.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header pull-left">
                        <span class="cut-half pull-left">Doloribus sed in ipsam ea</span><span class="price pull-right">₱ 11775</span>
                      </div>
                      <div class="card-author">
                        <a class="cut-half" href="/seller-page">Sadie Boykin</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/services-drilldown">
                      <div class="card-image card-small" style="background-image:url('images/photos/prev08.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Accusantium quia nemo autem architecto
                      </div>
                      <div class="card-author">
                        <a href="/seller-page">Kailey Holloway</a>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <a href="/services-drilldown">
                      <div class="card-image card-small" style="background-image:url('images/photos/prev30.jpg');"></div>
                    </a>
                    <div class="card-details">
                      <div class="card-header">
                        Molestiae et dolores sint mollitia
                      </div>
                      <div class="card-author">
                        <a href="/seller-page">Reese Byrd</a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="main">
                <h5 style="margin-bottom:1em;">
                  Favorite shops
                </h5>
                <div class="drilldown-related">
                  <div class="grid-4" data-columns="">
                    <div class="person">
                      <div class="comment no-border" style="margin-bottom:0;">
                        <div class="comment-image">
                          <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548448/santacruz_rbtf8a.jpg" /><a class="name" href="/seller-page">Pete Hodge</a>
                          <p>
                            42 projects
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="person">
                      <div class="comment no-border" style="margin-bottom:0;">
                        <div class="comment-image">
                          <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403546004/annies_cqot1m.jpg" /><a class="name" href="/seller-page">Freya Barr</a>
                          <p>
                            49 projects
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="person">
                      <div class="comment no-border" style="margin-bottom:0;">
                        <div class="comment-image">
                          <img class="square" src="http://www.campbridge.com/images/cwhomedepot-logo.png" /><a class="name" href="/seller-page">Manuel Farrell</a>
                          <p>
                            20 projects
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="person">
                      <div class="comment no-border" style="margin-bottom:0;">
                        <div class="comment-image">
                          <img class="square" src="https://res.cloudinary.com/relay-foods/image/upload/v1403548282/naturespath_lq26v4.jpg" /><a class="name" href="/seller-page">Gayle Welch</a>
                          <p>
                            46 projects
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop

@section('scripts')
  @parent

  <script src="/javascripts/vendor/scrolltofixed.js" type="text/javascript"></script>
  <script src="/javascripts/dropdown.js" type="text/javascript"></script>

  <script type="text/javascript">
    $('#seller-nav').scrollToFixed({
      marginTop: 24,
      limit: $('.footer').offset().top - $('#seller-nav').outerHeight(true) - 96,
      preFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      },
      postFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').removeClass('active');
          // $(this).removeClass('no-bg');
        });
      },
      preAbsolute: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      }
    });
  </script>
@stop
