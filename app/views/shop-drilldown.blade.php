@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section>
        <div class="drilldown-full">
          <div class="drilldown-breadcrumbs">
            <ul>
              <li>
                <a href="/shop">Shop</a>
              </li>
              <li>
                <a href="/shop-category">Lighting</a>
              </li>
              <li>
                <a href="/shop-category">Indoor Lighting</a>
              </li>
            </ul>
          </div>
          <div class="drilldown-heading">
            <div class="drilldown-content">
              <h3>
                Lighting Items and other Home Decoration
              </h3>
              <p style="margin-bottom:0;">
                <span>by</span> <a href="/seller-page">Artery</a>
              </p>
            </div>
          </div>
        </div>
        <div class="drilldown-full">
          <div class="drilldown-misc">
            <div class="drilldown-card">
              <div class="drilldown-content">
                <div class="drilldown-gallery">
                  <button class="light favorite"><i class="icon ion-android-favorite"></i></button><button class="light add"><i class="icon ion-plus"></i></button>
                  <ul id="image-gallery">
                    <li class="image" data-thumb="/images/seller-products/seller-product-2.jpg" style="background-image:url(/images/seller-products/seller-product-2.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-11.jpg" style="background-image:url(/images/seller-products/seller-product-11.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-9.jpg" style="background-image:url(/images/seller-products/seller-product-9.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-11.jpg" style="background-image:url(/images/seller-products/seller-product-11.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-3.jpg" style="background-image:url(/images/seller-products/seller-product-3.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-2.jpg" style="background-image:url(/images/seller-products/seller-product-2.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-11.jpg" style="background-image:url(/images/seller-products/seller-product-11.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-12.jpg" style="background-image:url(/images/seller-products/seller-product-12.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-11.jpg" style="background-image:url(/images/seller-products/seller-product-11.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-14.jpg" style="background-image:url(/images/seller-products/seller-product-14.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-3.jpg" style="background-image:url(/images/seller-products/seller-product-3.jpg);"></li>
                    <li class="image" data-thumb="/images/seller-products/seller-product-2.jpg" style="background-image:url(/images/seller-products/seller-product-2.jpg);"></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="drilldown-misc-2">
            <div class="drilldown-tabs">
              <nav>
                <ul class="drilldown-tabs-nav">
                  <li class="shop">
                    <a class="selected" data-content="description" href="#0">Description</a>
                  </li>
                  <li class="shop">
                    <a data-content="reviews" href="#0">Reviews</a>
                  </li>
                </ul>
              </nav>
              <ul class="drilldown-tabs-content">
                <li class="selected" data-content="description">
                  <p>
                    Vitae error aut non laborum esse qui deserunt pariatur voluptatem consequatur deleniti quibusdam ut qui. dicta in voluptas nihil ab corporis consequuntur autem reiciendis delectus fugit consectetur consequatur. aspernatur dignissimos expedita sint reiciendis molestiae in consequuntur et ad dolore porro cupiditate provident. ducimus et odit iste id alias quis ut rerum laborum possimus. delectus beatae tempore maiores dolore et. sint molestiae omnis qui mollitia quia voluptatem mollitia debitis et quos et
                  </p>
                  <p>
                    Et est dolores error. sunt ut natus aut exercitationem voluptate unde ut blanditiis qui sapiente provident omnis. dolores recusandae vitae et quia non et suscipit nesciunt vitae in repellat ipsam amet. ab itaque voluptatem omnis nam ipsa similique eius libero. et modi voluptatum quae mollitia illo aperiam qui cum at vitae repellat. saepe vero magnam in quo voluptas est est maiores cum dolorum est. sit error qui eveniet
                  </p>
                  <div class="drilldown-card bordered" style="margin-bottom:0;margin-top:1.5em;">
                    <div class="drilldown-content">
                      <section>
                        <div class="ui form">
                          <div class="two fields">
                            <div class="field">
                              <label style="margin-bottom:0;font-weight:600;">Price</label>
                              <p style="margin-bottom:0;font-size:14px;font-weight:500;">
                                <span class="price">₱ 2432</span> <span style="font-size:12px;">(per bundle)</span>
                              </p>
                            </div>
                            <div class="field">
                              <label style="margin-bottom:0;font-weight:600;">Quantity</label><select class="pull-left" style="margin-bottom:0;width:50%;font-size:12px;">
                                <option>
                                  1
                                </option>
                                <option>
                                  2
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="two fields">
                            <div class="field">
                              <label style="margin-bottom:0;font-weight:600;">Style</label>
                              <p style="margin-bottom:0;font-size:14px;">
                                Traditional
                              </p>
                            </div>
                            <div class="field">
                              <label style="margin-bottom:0;font-weight:600;">Category</label>
                              <p style="margin-bottom:0;font-size:14px;">
                                Lighting Items
                              </p>
                            </div>
                          </div>
                          <div class="field actions">
                            <button class="block call-to-action">Add to cart</button>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                </li>
                <li data-content="reviews">
                  <h5>
                    6 reviews
                  </h5>
                  <ul class="comments">
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/jonsuh_120.jpg" /><a href="/user-profile">Tonia</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                        </div>
                        <p>
                          Error qui dignissimos alias ut perferendis repudiandae dolorum nobis aliquam. Temporibus est quidem praesentium velit quia sequi et labore et
                        </p>
                      </div>
                    </li>
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/toodlenoodle_120.jpg" /><a href="/user-profile">Orlaith</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
                        </div>
                        <p>
                          Officia est molestiae rerum ipsa est suscipit quod inventore maiores officiis molestias eveniet. Non fugiat et sapiente architecto et
                        </p>
                      </div>
                    </li>
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/mds_120.jpg" /><a href="/user-profile">Joey</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                        </div>
                        <p>
                          Beatae pariatur consequuntur veritatis magni. Labore quibusdam voluptas tenetur ipsam ad omnis excepturi minima aut aliquid nihil temporibus aperiam impedit
                        </p>
                      </div>
                    </li>
                  </ul>
                  <div class="landing-view-all" style="text-align:center;">
                    <a class="button light" href="/seller-page-reviews" id="view-reviews-button" style="margin-top:2em;padding-left:2em;padding-right:2em;"><span>View all reviews</span><i class="icon ion-ios-arrow-right"></i></a>
                  </div>
                </li>
              </ul>
            </div>
            <div class="drilldown-card secondary">
              <section>
                <div class="actions">
                  <button class="light" style="background-color:#3B5998;color:white;margin-right:0.5em;"><span>Share</span><i class="icon ion-social-facebook"></i></button><button class="light" style="background-color:#55ACEE;color:white;margin-right:0.5em;"><span>Tweet</span><i class="icon ion-social-twitter"></i></button><button class="light" style="background-color:#C92228;color:white;margin-right:0.5em;"><span>Pin it</span><i class="icon ion-social-pinterest"></i></button>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <div class="panel panel-light">
        <section>
          <div class="drilldown-full">
            <div class="drilldown-heading">
              <section class="main no-bot">
                <h5 class="pull-left">
                  More from Artery
                </h5>
              </section>
            </div>
          </div>
          <div class="drilldown-full">
            <div class="drilldown-related">
              <div class="grid-3" data-columns="">
                <div class="card">
                  <div class="cards-carousel">
                    <div class="carousel-overlay" data-content="carousel-0">
                      <div class="card-image" style="background-image:url('/images/products/prev19.jpg');"></div>
                      <div class="card-image" style="background-image:url('/images/products/prev47.jpg');"></div>
                      <div class="card-image" style="background-image:url('/images/products/prev07.jpg');"></div>
                    </div>
                  </div>
                  <div class="card-details">
                    <section>
                      <div class="user">
                        <div class="user-info" style="width:100%;">
                          <a class="name" href="/shop-drilldown">Iusto quod id similique rep...</a><br /><a class="location" href="/shop-drilldown">20 photos</a>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
                <div class="card">
                  <div class="cards-carousel">
                    <div class="carousel-overlay" data-content="carousel-1">
                      <div class="card-image" style="background-image:url('/images/products/prev40.jpg');"></div>
                      <div class="card-image" style="background-image:url('/images/products/prev16.jpg');"></div>
                      <div class="card-image" style="background-image:url('/images/products/prev16.jpg');"></div>
                    </div>
                  </div>
                  <div class="card-details">
                    <section>
                      <div class="user">
                        <div class="user-info" style="width:100%;">
                          <a class="name" href="/shop-drilldown">Doloribus perferendis sit o...</a><br /><a class="location" href="/shop-drilldown">18 photos</a>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
                <div class="card">
                  <div class="cards-carousel">
                    <div class="carousel-overlay" data-content="carousel-2">
                      <div class="card-image" style="background-image:url('/images/products/prev04.jpg');"></div>
                      <div class="card-image" style="background-image:url('/images/products/prev48.jpg');"></div>
                      <div class="card-image" style="background-image:url('/images/products/prev46.jpg');"></div>
                    </div>
                  </div>
                  <div class="card-details">
                    <section>
                      <div class="user">
                        <div class="user-info" style="width:100%;">
                          <a class="name" href="/shop-drilldown">Sit odio aut expedita quo</a><br /><a class="location" href="/shop-drilldown">3 photos</a>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
              <div class="landing-view-all" style="text-align:center;">
                <a class="button light" href="/articles" style="padding-left:2em;padding-right:2em;"><span>View more items</span><i class="icon ion-ios-arrow-right"></i></a>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="panel">
        <section class="no-bot">
          <div class="drilldown-full">
            <div class="drilldown-heading">
              <section class="main no-bot">
                <h4 class="pull-left">
                  Related
                </h4>
              </section>
            </div>
          </div>
          <div class="drilldown-full">
            <section class="main no-top">
              <div class="list-side-by-side">
                <ul>
                  <li>
                    <button class="tag">bedroom</button>
                  </li>
                  <li>
                    <button class="tag">dining</button>
                  </li>
                  <li>
                    <button class="tag">storage-and-closets</button>
                  </li>
                  <li>
                    <button class="tag">bath</button>
                  </li>
                  <li>
                    <button class="tag">bedroom</button>
                  </li>
                  <li>
                    <button class="tag">living</button>
                  </li>
                  <li>
                    <button class="tag">kitchen</button>
                  </li>
                  <li>
                    <button class="tag">dining</button>
                  </li>
                  <li>
                    <button class="tag">apartment</button>
                  </li>
                  <li>
                    <button class="tag">laundry-and-utility</button>
                  </li>
                  <li>
                    <button class="tag">kids</button>
                  </li>
                  <li>
                    <button class="tag">bedroom</button>
                  </li>
                </ul>
              </div>
            </section>
          </div>
        </section>
        <section class="no-bot">
          <div class="drilldown-full">
            <div class="drilldown-heading">
              <section class="main no-bot">
                <h5 class="pull-left">
                  Articles
                </h5>
              </section>
            </div>
          </div>
          <div class="drilldown-related">
            <div class="grid-2" data-columns="">
              <div class="card">
                <a href="#">
                  <div class="card-image" style="background-image:url('/images/photos/prev43.jpg');"></div>
                </a>
                <div class="card-details">
                  <section>
                    <div class="user">
                      <div class="user-thumb smaller">
                        <img src="https://res.cloudinary.com/relay-foods/image/upload/v1403548448/santacruz_rbtf8a.jpg" />
                      </div>
                      <div class="user-info">
                        <a class="name" href="#" style="line-height:1.5em;"><span>Not a Big Cook? These Fun Kitchen Ideas Are f...</span></a><br /><a class="location" href="/seller-page">Lowes</a>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
              <div class="card">
                <a href="#">
                  <div class="card-image" style="background-image:url('/images/photos/prev11.jpg');"></div>
                </a>
                <div class="card-details">
                  <section>
                    <div class="user">
                      <div class="user-thumb smaller">
                        <img src="https://res.cloudinary.com/relay-foods/image/upload/v1403548448/santacruz_rbtf8a.jpg" />
                      </div>
                      <div class="user-info">
                        <a class="name" href="#" style="line-height:1.5em;"><span>Once a Barren Rooftop, Now a Serene City Getaway</span></a><br /><a class="location" href="/seller-page">Home Depot</a>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
            <div class="landing-view-all" style="text-align:center;">
              <a class="button light" href="/articles" style="padding-left:2em;padding-right:2em;"><span>See more articles</span><i class="icon ion-ios-arrow-down"></i></a>
            </div>
          </div>
        </section>
        <section>
          <div class="drilldown-full">
            <div class="drilldown-heading">
              <section class="main no-bot">
                <h5 class="pull-left">
                  Items
                </h5>
                <button class="light pull-right" style="margin-bottom:1em;" type="button"><span>Quezon City</span><i class="icon ion-android-close"></i></button><button class="light pull-right" style="margin-bottom:1em;margin-right:1em;" type="button"><span>Sort by</span><i class="icon ion-ios-arrow-down"></i></button>
              </section>
            </div>
          </div>
          <div class="drilldown-related">
            <div class="grid-4" data-columns="">
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev44.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Aut sed est quibusdam officia</span><span class="price pull-right">₱ 5801</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Mitzi Christensen</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev29.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Aut nam dolor et quod</span><span class="price pull-right">₱ 927</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Mya McNamara</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev48.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Inventore voluptas reprehenderit autem et</span><span class="price pull-right">₱ 9183</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Adrianna Fox</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev24.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Optio eveniet quo et nihil</span><span class="price pull-right">₱ 5118</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Isabel Zhang</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev14.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Non voluptatibus eligendi explicabo voluptatibus</span><span class="price pull-right">₱ 12787</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Pete Snyder</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev07.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Doloremque fugiat dolorum quia beatae</span><span class="price pull-right">₱ 2891</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Colton Black</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev44.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Et libero consequuntur quo cumque</span><span class="price pull-right">₱ 3972</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Kaleigh Merritt</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev38.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Eius laborum mollitia et sapiente</span><span class="price pull-right">₱ 4228</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Hector Rich</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev43.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Et porro harum et porro</span><span class="price pull-right">₱ 9452</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Gordon Abbott</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev29.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Sit impedit quia et esse</span><span class="price pull-right">₱ 4839</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Kailey Shaffer</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev11.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Est suscipit quod ab dolorum</span><span class="price pull-right">₱ 32</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Kelvin Love</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev36.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Ut ad nobis consequatur hic</span><span class="price pull-right">₱ 13487</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Eoin Bowman</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev43.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Pariatur animi soluta deserunt eum</span><span class="price pull-right">₱ 14281</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Shirley Gray</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev04.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Placeat adipisci minus quam aspernatur</span><span class="price pull-right">₱ 3360</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Lori Hinton</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev10.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Dolores quo vel quibusdam animi</span><span class="price pull-right">₱ 9694</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Fatima Watts</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev35.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Dolores dignissimos quam natus et</span><span class="price pull-right">₱ 11598</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Allen Perkins</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev09.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Doloribus non ea pariatur quo</span><span class="price pull-right">₱ 6795</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Uriel McLaughlin</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev02.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Quod et quia totam sunt</span><span class="price pull-right">₱ 10828</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Harvey Case</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev04.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Dolorem ipsa aut porro non</span><span class="price pull-right">₱ 12623</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Colm Chung</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev03.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Enim repellendus perferendis quas et</span><span class="price pull-right">₱ 5739</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Isabel Rowe</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev02.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Qui laboriosam dolor velit quos</span><span class="price pull-right">₱ 3917</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Billie Rich</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev14.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Eaque ad aliquam minus vitae</span><span class="price pull-right">₱ 14886</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Dulce Christensen</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev02.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Ipsum eius dolorum veniam voluptates</span><span class="price pull-right">₱ 56</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Harvey Jones</a>
                  </div>
                </div>
              </div><div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image" style="background-image:url('/images/products/prev04.jpg');">
                    <div class="card-hover">
                      <button class="light add"><i class="icon ion-plus"></i></button><button class="light favorite"><i class="icon ion-android-favorite"></i></button>
                    </div>
                  </div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Eum ipsa magnam ea eligendi</span><span class="price pull-right">₱ 12652</span>
                  </div>
                  <div class="card-author">
                    <a href="/seller-page">Jason Golden</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  @parent
  <script src="/javascripts/vendor/lightslider.js" type="text/javascript"></script>
  <script src="/javascripts/tabs.js" type="text/javascript"></script>
  <script src="/javascripts/vendor/slick.js" type="text/javascript"></script>
  <script src="/javascripts/dropdown.js" type="text/javascript"></script>

  <script type="text/javascript">
    $('#image-gallery').lightSlider({
      gallery: true,
      item: 1,
      slideMargin: 0,
      currentPagerPosition: 'left',
      thumbItem: 8,
      loop: true
    });
    $('.carousel-overlay').slick({
      //arrows: false,
      dots: true,
    });
  </script>
@stop
