@extends('layouts.master')

@section('content')
  <div id="header-wrap-test">
    <header class="main-filters">
      <div class="header-section">
        <div class="header-page-category">
          <h1 id="trigger-overlay">
            Conversations<i class="icon ion-ios-arrow-down"></i>
          </h1>
        </div>
        <div class="header-search">
          <div class="ui form">
            <input class="search-box" name="search-query" placeholder="Filter" type="text"><i class="icon ion-ios-search-strong"></i></input>
          </div>
        </div>
      </div>
      <div class="category-carousel">
        <div class="header-category" style="background-image:url('images/services/landscaping-header.jpg');">
          <div class="header-category-caption-container">
            <div class="header-category-caption">
              <a href="/services-category"><span>Landscaping</span> <span class="small">(580 pros)</span></a>
            </div>
          </div>
        </div>
        <div class="header-category" style="background-image:url('images/services/house-cleaning-header.jpg');">
          <div class="header-category-caption-container">
            <div class="header-category-caption">
              <a href="/services-category"><span>House Cleaning</span> <span class="small">(414 pros)</span></a>
            </div>
          </div>
        </div>
        <div class="header-category" style="background-image:url('images/services/handyman-header.jpg');">
          <div class="header-category-caption-container">
            <div class="header-category-caption">
              <a href="/services-category"><span>Handyman</span> <span class="small">(962 pros)</span></a>
            </div>
          </div>
        </div>
      </div>
    </header>
  </div>
  <div class="overlay overlay-hugeinc">
    <nav>
      <div class="drilldown-card">
        <i class="icon ion-android-close overlay-close"></i>
        <ul class="drilldown-tabs-nav" style="padding:0;">
          <li style="margin-bottom:0;">
            <a class="hvr-wobble-vertical selected" data-content="services" href="/services">Services</a>
          </li>
          <li style="margin-bottom:0;">
            <a class="hvr-wobble-vertical" data-content="shop" href="/shop">Shop</a>
          </li>
          <li style="margin-bottom:0;">
            <a class="hvr-wobble-vertical" data-content="articles" href="/articles">Articles</a>
          </li>
          <li style="margin-bottom:0;">
            <a class="hvr-wobble-vertical" data-content="conversations" href="/conversations">Conversations</a>
          </li>
        </ul>
        <div class="drilldown-full">
          <div class="overlay-main-categories">
            <ul class="main-list">
              <li>
                <a class="overlay-main-category more active" data-content="general_home_improvement" href="#0">General Home Improvement<i class="icon ion-ios-arrow-right"></i></a>
                <ul class="link-list">
                  <li>
                    <a class="overlay-main-category back" href="#0" style="color:#3a8ef0 !important;"><i class="icon ion-ios-arrow-left" style="position:static;margin-right:1em;"></i>Back</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Carpentry</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Cleaning Services</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Electrical Services</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Green Home Improvements</a>
                  </li>
                </ul>
              </li>
              <li>
                <a class="overlay-main-category more" data-content="indoor" href="#0">Indoor<i class="icon ion-ios-arrow-right"></i></a>
                <ul class="link-list">
                  <li>
                    <a class="overlay-main-category back" href="#0" style="color:#3a8ef0 !important;"><i class="icon ion-ios-arrow-left" style="position:static;margin-right:1em;"></i>Back</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Carpentry</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Cleaning Services</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Electrical Services</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Green Home Improvements</a>
                  </li>
                </ul>
              </li>
              <li>
                <a class="overlay-main-category more" data-content="landscaping_and_gardens" href="#0">Landscaping And Gardens<i class="icon ion-ios-arrow-right"></i></a>
                <ul class="link-list">
                  <li>
                    <a class="overlay-main-category back" href="#0" style="color:#3a8ef0 !important;"><i class="icon ion-ios-arrow-left" style="position:static;margin-right:1em;"></i>Back</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Carpentry</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Cleaning Services</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Electrical Services</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Green Home Improvements</a>
                  </li>
                </ul>
              </li>
              <li>
                <a class="overlay-main-category more" data-content="outdoor" href="#0">Outdoor<i class="icon ion-ios-arrow-right"></i></a>
                <ul class="link-list">
                  <li>
                    <a class="overlay-main-category back" href="#0" style="color:#3a8ef0 !important;"><i class="icon ion-ios-arrow-left" style="position:static;margin-right:1em;"></i>Back</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Carpentry</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Cleaning Services</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Electrical Services</a>
                  </li>
                  <li>
                    <a class="overlay-main-category">Green Home Improvements</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <div class="overlay-categories">
            <div class="overlay-categories-list active" data-content="general_home_improvement">
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Appliance Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Appliance Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Bathroom Remodel</a>
                  </li>
                  <li>
                    <a href="/services-category">Blacksmithing</a>
                  </li>
                  <li>
                    <a href="/services-category">Cabinetry</a>
                  </li>
                  <li>
                    <a href="/services-category">Circuit Breaker Panel or Fuse Box Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Circuit Breaker Panel or Fuse Box Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Commercial Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Door Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Door Repair Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Electrical and Wiring Issues</a>
                  </li>
                  <li>
                    <a href="/services-category">General Carpentry</a>
                  </li>
                  <li>
                    <a href="/services-category">General Contracting</a>
                  </li>
                </ul>
              </div>
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Handyman</a>
                  </li>
                  <li>
                    <a href="/services-category">Kitchen Remodel</a>
                  </li>
                  <li>
                    <a href="/services-category">Lamp Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Locksmith</a>
                  </li>
                  <li>
                    <a href="/services-category">Metalwork</a>
                  </li>
                  <li>
                    <a href="/services-category">Millwork</a>
                  </li>
                  <li>
                    <a href="/services-category">Muralist</a>
                  </li>
                  <li>
                    <a href="/services-category">Odor Removal</a>
                  </li>
                  <li>
                    <a href="/services-category">Paint Removal</a>
                  </li>
                  <li>
                    <a href="/services-category">Plumbing Pipe Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Plumbing</a>
                  </li>
                  <li>
                    <a href="/services-category">Property Management</a>
                  </li>
                  <li>
                    <a href="/services-category">Railing Installation and Remodel</a>
                  </li>
                </ul>
              </div>
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Railing Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Real Estate Agent</a>
                  </li>
                  <li>
                    <a href="/services-category">Real Estate Appraisal</a>
                  </li>
                  <li>
                    <a href="/services-category">Real Estate Inspection</a>
                  </li>
                  <li>
                    <a href="/services-category">Real Estate Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Stained Glass</a>
                  </li>
                  <li>
                    <a href="/services-category">Stair and Staircase Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Switch and Outlet Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Switch and Outlet Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Tiling</a>
                  </li>
                  <li>
                    <a href="/services-category">Welding</a>
                  </li>
                  <li>
                    <a href="/services-category">Wiring</a>
                  </li>
                  <li>
                    <a href="/services-category">Fine Woodworking</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="overlay-categories-list" data-content="indoor">
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Aquarium Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Asbestos Removal</a>
                  </li>
                  <li>
                    <a href="/services-category">Attic Remodel</a>
                  </li>
                  <li>
                    <a href="/services-category">Basement Remodel</a>
                  </li>
                  <li>
                    <a href="/services-category">Bed Bug Extermination</a>
                  </li>
                  <li>
                    <a href="/services-category">Boiler Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Central Air Conditioning Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Framing Carpentry</a>
                  </li>
                  <li>
                    <a href="/services-category">Carpet Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Carpet Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Carpet Repair or Partial Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">Caulking</a>
                  </li>
                  <li>
                    <a href="/services-category">Ceiling Fan</a>
                  </li>
                  <li>
                    <a href="/services-category">Child Proofing</a>
                  </li>
                  <li>
                    <a href="/services-category">Closet Organizers</a>
                  </li>
                  <li>
                    <a href="/services-category">Closets</a>
                  </li>
                  <li>
                    <a href="/services-category">Concrete Flooring Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Countertop Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Countertop Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Dishwasher Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Dishwasher Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Drapery Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Drapery Installation or Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">Drapery Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Drywall Installation and Hanging</a>
                  </li>
                  <li>
                    <a href="/services-category">Drywall Repair and Texturing</a>
                  </li>
                  <li>
                    <a href="/services-category">Duct and Vent Cleaning or Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Energy Efficiency Remodel</a>
                  </li>
                  <li>
                    <a href="/services-category">Exercise Equipment Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Fan Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Fan Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Sink and Faucet Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Finish Carpentry, Trim, and Crown Molding</a>
                  </li>
                  <li>
                    <a href="/services-category">Fire Extinguisher Inspection</a>
                  </li>
                  <li>
                    <a href="/services-category">Fireplace and Chimney Cleaning or Inspection</a>
                  </li>
                  <li>
                    <a href="/services-category">Fireplace and Chimney Maintenance or Repair</a>
                  </li>
                </ul>
              </div>
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Fixtures</a>
                  </li>
                  <li>
                    <a href="/services-category">Flooring</a>
                  </li>
                  <li>
                    <a href="/services-category">Furnace and Heating System Installation or Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">Furnace and Heating System Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Furniture Assembly</a>
                  </li>
                  <li>
                    <a href="/services-category">Furniture Refinishing</a>
                  </li>
                  <li>
                    <a href="/services-category">Generator Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Geothermal Energy Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Hardwood Floor Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Hardwood Floor Repair or Partial Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">Hardwood Floor Refinishing</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Automation</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Energy Auditing</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Modification for Disabled Persons</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Modification for Seniors</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Security and Alarms Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Security and Alarm Repair and Modification</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Staging</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Theater Component Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Theater Construction</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Theater Surround Sound System Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Theater System Installation or Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Theater System Repair or Service</a>
                  </li>
                  <li>
                    <a href="/services-category">House Cleaning (Recurring)</a>
                  </li>
                  <li>
                    <a href="/services-category">House Cleaning (One Time)</a>
                  </li>
                  <li>
                    <a href="/services-category">IKEA Furniture Assembly</a>
                  </li>
                  <li>
                    <a href="/services-category">Install, Repair, or Conceal Home Theater System Wiring</a>
                  </li>
                  <li>
                    <a href="/services-category">Insulation</a>
                  </li>
                  <li>
                    <a href="/services-category">Interior Design</a>
                  </li>
                  <li>
                    <a href="/services-category">Interior Painting</a>
                  </li>
                  <li>
                    <a href="/services-category">Vinyl or Linoleum Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Vinyl or Linoleum Repair or Partial Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">Lighting Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Mold Inspection and Removal</a>
                  </li>
                  <li>
                    <a href="/services-category">Mold Remediation</a>
                  </li>
                  <li>
                    <a href="/services-category">New Home Construction</a>
                  </li>
                </ul>
              </div>
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Oven and Stove Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Pest Control Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Picture Framing</a>
                  </li>
                  <li>
                    <a href="/services-category">Picture Hanging and Art Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Refrigerator Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Refrigerator Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Remodel a Room</a>
                  </li>
                  <li>
                    <a href="/services-category">Rug Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Shower and Bathtub Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Shutters</a>
                  </li>
                  <li>
                    <a href="/services-category">Skylights</a>
                  </li>
                  <li>
                    <a href="/services-category">Sound Proofing</a>
                  </li>
                  <li>
                    <a href="/services-category">Steam Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Stone or Tile Flooring Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Stone or Tile Flooring Repair or Partial Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">TV Mounting</a>
                  </li>
                  <li>
                    <a href="/services-category">TV Repair Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Telephone System Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Thermostats</a>
                  </li>
                  <li>
                    <a href="/services-category">Tile and Grout Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Tile Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Tile Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Upholstery and Furniture Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Vacuum Cleaner Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Wallpapering</a>
                  </li>
                  <li>
                    <a href="/services-category">Wallpaper Removal</a>
                  </li>
                  <li>
                    <a href="/services-category">Wallpaper Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Washing Machine Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Washing Machine Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Water Damage Cleanup and Restoration</a>
                  </li>
                  <li>
                    <a href="/services-category">Water Heater Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Water Treatment System Install</a>
                  </li>
                  <li>
                    <a href="/services-category">Window Blinds Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Window Blinds Installation or Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">Window Blinds Repair</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="overlay-categories-list" data-content="landscaping_and_gardens">
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Water Features</a>
                  </li>
                  <li>
                    <a href="/services-category">Landscape Design</a>
                  </li>
                </ul>
              </div>
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Grottos</a>
                  </li>
                  <li>
                    <a href="/services-category">Green Roofs</a>
                  </li>
                </ul>
              </div>
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Walkways</a>
                  </li>
                  <li>
                    <a href="/services-category">Gazebos</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="overlay-categories-list" data-content="outdoor">
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Above Ground Swimming Pool Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Air Conditioning Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Architectural Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Asphalt Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Asphalt Repair and Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Awning Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Awning Repair and Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Backhoe Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Barbecue and Grill Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Boulder Placement</a>
                  </li>
                  <li>
                    <a href="/services-category">Cleaning Out</a>
                  </li>
                  <li>
                    <a href="/services-category">Concrete Delivery</a>
                  </li>
                  <li>
                    <a href="/services-category">Concrete Driveway Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Concrete Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Concrete Removal</a>
                  </li>
                  <li>
                    <a href="/services-category">Concrete Sawing</a>
                  </li>
                  <li>
                    <a href="/services-category">Concrete Repair and Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Deck Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Deck, Porch, Balcony, and Patio Remodel or Addition</a>
                  </li>
                  <li>
                    <a href="/services-category">Deck, Porch, Balcony, and Patio Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Demolition Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Plumbing Pipe Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Exterior Painting</a>
                  </li>
                  <li>
                    <a href="/services-category">Fence Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Fence Repair and Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Flat Roofing</a>
                  </li>
                  <li>
                    <a href="/services-category">Foundation Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Foundation Raising</a>
                  </li>
                </ul>
              </div>
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Foundation Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Garage Door Installation or Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">Garage Door Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Glass Blocks</a>
                  </li>
                  <li>
                    <a href="/services-category">Greenhouse Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Gutter Installation or Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">Gutter Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Holiday Lighting Installation and Removal</a>
                  </li>
                  <li>
                    <a href="/services-category">Hot Tub and Spa Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Hot Tub and Spa Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Hot Tub and Spa Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">In-Ground Swimming Pool Construction</a>
                  </li>
                  <li>
                    <a href="/services-category">Junk Removal</a>
                  </li>
                  <li>
                    <a href="/services-category">Land Leveling and Grading - Large Scale (more than 1 acre)</a>
                  </li>
                  <li>
                    <a href="/services-category">Land Leveling and Grading - Small Scale (less than 1 acre)</a>
                  </li>
                  <li>
                    <a href="/services-category">Land Surveying</a>
                  </li>
                  <li>
                    <a href="/services-category">Masonry Construction Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Masonry Repair and Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Metal Roofing</a>
                  </li>
                  <li>
                    <a href="/services-category">Mudjacking</a>
                  </li>
                  <li>
                    <a href="/services-category">Outdoor Lighting</a>
                  </li>
                  <li>
                    <a href="/services-category">Outdoor Kitchen Remodel or Addition</a>
                  </li>
                  <li>
                    <a href="/services-category">Patio Cover Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Patio Cover Repair and Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Plastering</a>
                  </li>
                  <li>
                    <a href="/services-category">Porch Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Pressure Washing</a>
                  </li>
                  <li>
                    <a href="/services-category">Property Cleanup</a>
                  </li>
                </ul>
              </div>
              <div class="overlay-categories-list-group">
                <ul>
                  <li>
                    <a href="/services-category">Roof Installation or Replacement</a>
                  </li>
                  <li>
                    <a href="/services-category">Roof Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Satellite Dish Services</a>
                  </li>
                  <li>
                    <a href="/services-category">Sawmilling</a>
                  </li>
                  <li>
                    <a href="/services-category">Seismic Retrofitting</a>
                  </li>
                  <li>
                    <a href="/services-category">Septic System Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Septic System Repair or Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Siding Installation, Repair, or Removal</a>
                  </li>
                  <li>
                    <a href="/services-category">Slate Roofing</a>
                  </li>
                  <li>
                    <a href="/services-category">Snow Plowing</a>
                  </li>
                  <li>
                    <a href="/services-category">Solar Panel Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Solar Panel Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Solar Panel Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Storm Windows</a>
                  </li>
                  <li>
                    <a href="/services-category">Stucco Application</a>
                  </li>
                  <li>
                    <a href="/services-category">Swimming Pool Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Swimming Pool Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Swimming Pool Repair</a>
                  </li>
                  <li>
                    <a href="/services-category">Tile Roofing</a>
                  </li>
                  <li>
                    <a href="/services-category">Water Feature Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Water Feature Repair and Maintenance</a>
                  </li>
                  <li>
                    <a href="/services-category">Home Waterproofing</a>
                  </li>
                  <li>
                    <a href="/services-category">Weatherization</a>
                  </li>
                  <li>
                    <a href="/services-category">Well System Work</a>
                  </li>
                  <li>
                    <a href="/services-category">Window Cleaning</a>
                  </li>
                  <li>
                    <a href="/services-category">Window Installation</a>
                  </li>
                  <li>
                    <a href="/services-category">Window Screen Work</a>
                  </li>
                  <li>
                    <a href="/services-category">Window Treatments Work</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
  <div id="content-wrap">
    <div class="panel">
      <section class="main no-bot">
        <div class="landing-section with-bg no-margin" style="background-image:url('/images/landing/landing_9.jpg');background-position:center bottom;">
          <span>In Spotlight</span>
          <h2>
            Poll: Best Gardens in Makati
          </h2>
          <a class="button hollow" href="/seller-page" style="margin-top:1em;">Join the discussion</a>
        </div>
      </section>
    </div>
    <div class="panel show-back-to-top">
      <section class="main no-top">
        <div id="filter-wrap">
          <div id="filter-wrap-inner">
            <div class="filters">
              <div class="filters-category">
              </div>
              <div class="filters-misc">
                <h5>
                  24 <span>conversations</span>
                </h5>
              </div>
            </div>
          </div>
        </div>
        <div class="cards">
          <div class="card card-full">
            <div class="card-details">
              <a href="/conversations-drilldown">
                <div class="card-header">
                  <span>Need more help with frustrating small bedroom</span>
                </div>
              </a>
              <div class="card-copy">
                <p>
                  <span>Qui impedit libero autem ab dicta. Culpa et dicta maxime et soluta eum repudiandae perferendis deserunt aspernatur soluta sed doloremque amet. Omnis et et excepturi quis placeat et optio dolores. Consequatur velit atque officia rerum...</span> <a href="#">Read more</a>
                </p>
              </div>
              <a class="card-user" href="#"><img src="http://uifaces.com/faces/_twitter/zulsdesign_120.jpg" /><span>Kerry Hanna</span></a>
            </div>
            <a href="/conversations-drilldown">
              <div class="card-image" style="background-image:url('/images/photos/prev10.jpg');"></div>
            </a>
          </div>
          <div class="card card-full">
            <div class="card-details">
              <a href="/conversations-drilldown">
                <div class="card-header">
                  <span>Kitchen and (me) need help for this frustrating design</span>
                </div>
              </a>
              <div class="card-copy">
                <p>
                  <span>Repellendus quibusdam voluptatem quaerat velit ea quisquam in. Rerum labore error et adipisci modi et maxime necessitatibus sit ducimus at autem et aliquam. Quas neque aliquid occaecati laudantium necessitatibus quasi et asperiores numquam eos. Reprehenderit pariatur numquam laboriosam quia ratione doloribus...</span> <a href="#">Read more</a>
                </p>
              </div>
              <a class="card-user" href="#"><img src="http://uifaces.com/faces/_twitter/decarola_120.jpg" /><span>Alondra Cassidy</span></a>
            </div>
            <a href="/conversations-drilldown">
              <div class="card-image" style="background-image:url('/images/photos/prev45.jpg');"></div>
            </a>
          </div>
          <div class="card card-full">
            <div class="card-details">
              <a href="/conversations-drilldown">
                <div class="card-header">
                  <span>Need help with a narrow living room without much wall space and lots i</span>
                </div>
              </a>
              <div class="card-copy">
                <p>
                  <span>Sunt fuga quam aut temporibus error qui fugit reprehenderit illo expedita ut dolorem doloribus. Animi ipsa qui aspernatur itaque distinctio accusantium perspiciatis non totam consequuntur quam et. Ipsum in totam nam qui vel. Ea et velit ab nemo rerum minima amet...</span> <a href="#">Read more</a>
                </p>
              </div>
              <a class="card-user" href="#"><img src="http://uifaces.com/faces/_twitter/jayman_120.jpg" /><span>Ewan Hardin</span></a>
            </div>
            <a href="/conversations-drilldown">
              <div class="card-image" style="background-image:url('/images/photos/prev38.jpg');"></div>
            </a>
          </div>
          <div class="card card-full">
            <div class="card-details">
              <a href="/conversations-drilldown">
                <div class="card-header">
                  <span>How will I be able to buy products with sellers not from my current area?</span>
                </div>
              </a>
              <div class="card-copy">
                <p>
                  <span>Rerum veritatis consequatur ratione eos architecto quas qui ipsum veritatis vel tempore et. Quas doloribus ad neque vel nulla aperiam in recusandae. Maxime velit culpa asperiores in tempora quia quasi provident repudiandae. Dolorem dolore amet praesentium et aut...</span> <a href="#">Read more</a>
                </p>
              </div>
              <a class="card-user" href="#"><img src="http://uifaces.com/faces/_twitter/bradenhamm_120.jpg" /><span>Carley Solomon</span></a>
            </div>
            <a href="/conversations-drilldown">
              <div class="card-image" style="background-image:url('/images/photos/prev23.jpg');"></div>
            </a>
          </div>
          <div class="card card-full">
            <div class="card-details">
              <a href="/conversations-drilldown">
                <div class="card-header">
                  <span>Look at my new den after consulting cozee PROs!</span>
                </div>
              </a>
              <div class="card-copy">
                <p>
                  <span>Amet fugiat nihil unde. Soluta omnis nesciunt ut sit in placeat maxime ratione nihil facilis. Consectetur alias quo vero aspernatur quisquam. Ipsam sint rerum et et sequi exercitationem ut atque et enim a magni ipsum...</span> <a href="#">Read more</a>
                </p>
              </div>
              <a class="card-user" href="#"><img src="http://uifaces.com/faces/_twitter/AngelZxxWingZ_120.jpg" /><span>Joey Jennings</span></a>
            </div>
            <a href="/conversations-drilldown">
              <div class="card-image" style="background-image:url('/images/photos/prev14.jpg');"></div>
            </a>
          </div>
          <div class="card card-full">
            <div class="card-details">
              <a href="/conversations-drilldown">
                <div class="card-header">
                  <span>I can't get over this design problem I'm having on my bedroom... pls help</span>
                </div>
              </a>
              <div class="card-copy">
                <p>
                  <span>Ut rerum non cum et. Quia eligendi nostrum aut numquam voluptatibus libero adipisci repellendus voluptatum sint voluptatibus rerum ex ut. Quas voluptatem nam vel quibusdam non ex et id quia nobis sunt sed corporis. Dolorem aspernatur ipsa quam non vero...</span> <a href="#">Read more</a>
                </p>
              </div>
              <a class="card-user" href="#"><img src="http://uifaces.com/faces/_twitter/Fubaruba_120.jpg" /><span>Constance Petersen</span></a>
            </div>
            <a href="/conversations-drilldown">
              <div class="card-image" style="background-image:url('/images/photos/prev05.jpg');"></div>
            </a>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop

@section('scripts')
  @parent
  <script src="/javascripts/vendor/scrolltofixed.js" type="text/javascript"></script>
  <script src="/javascripts/vendor/slick.js" type="text/javascript"></script>
  <script src="/javascripts/selectize.js" type="text/javascript"></script>
  <script src="/javascripts/filter.js" type="text/javascript"></script>
  <script src="/javascripts/header.js" type="text/javascript"></script>
  <script src="/javascripts/fullscreen-overlay-menu.js" type="text/javascript"></script>

  <script type="text/javascript">
    $('#filter-wrap-inner').scrollToFixed({
      fixed: function() {
        $(this).addClass('on-fixed');
        $('.filter-category').addClass('on-fixed');
      },
      preUnfixed: function() {
        $(this).removeClass('on-fixed');
        $('.filter-category').removeClass('on-fixed');
      }
    });

    $('.overlay-main-category.more').click(function() {
      $('.main-list').toggleClass('hide');
      $(this).siblings('.link-list').toggleClass('show');
    });

    $('.overlay-main-category.back').click(function() {
      $(this).parents('.link-list').removeClass('show');
      $('.main-list').toggleClass('hide');
    });

    $('.category-carousel').slick({
      infinite: true,
    });

    $('#main-search .search-box').selectize({
      plugins: ['remove_button'],
      delimiter: ',',
      persist: false,
      create: function(input) {
        return {
          value: input,
          text: input
        }
      }
    });
  </script>
@stop
