@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Info and Appearance</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-storefront">Edit storefront</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-storefront-about">About your shop</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-storefront-policies">Policies</a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Options</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="#">Storefront options</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Vacation Mode</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Web Analytics</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Close shop</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <section class="main no-top">
              <div class="drilldown-full">
                <h5 style="margin-bottom:1em;">
                  About your shop
                </h5>
                <form>
                  <div class="drilldown-card">
                    <div class="drilldown-content">
                      <div class="ui form">
                        <div class="field">
                          <label style="font-weight:500;">Shop Photos</label>
                          <div class="card card-form upload-image" style="border-color:rgba(39, 41, 43, 0.15);">
                            <div class="card-image" style="width:100%;">
                              <p style="padding-top:0.5em;">
                                Click to upload photos
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Shop Bio</label><textarea name="body" placeholder="Describe your shop." rows="5" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="drilldown-card">
                    <div class="drilldown-content">
                      <label style="color:rgba(0,0,0,0.8);font-weight:500;font-size:14px;">Members</label>
                      <div class="ui form">
                        <div class="comment" style="padding-left:1em;padding-top:1em;padding-bottom:1.5em;">
                          <div class="three fields">
                            <div class="field" style="width:96px;">
                              <label style="font-weight:500;">Portrait</label>
                              <div class="card card-form upload-image" style="height:96px;border-color:rgba(39, 41, 43, 0.15);">
                                <div class="card-image" style="position:relative;width:100%;height:100%;">
                                  <i class="icon ion-ios-plus" style="position:absolute;top:25%;left:28%;font-size:48px;color:rgba(39, 41, 43, 0.15);"></i>
                                </div>
                              </div>
                            </div>
                            <div class="field">
                              <div class="field">
                                <label style="font-weight:500;">Name</label><input name="name" placeholder="Enter your first name" type="text" />
                              </div>
                              <div class="field">
                                <label style="font-weight:500;">Role</label>
                                <div class="ui search dropdown simple selection" style="font-size:0.85em;">
                                  <input name="role" type="hidden" /><i class="dropdown icon"></i>
                                  <div class="default text">
                                    Select role
                                  </div>
                                  <div class="menu">
                                    <div class="item">
                                      Owner
                                    </div>
                                    <div class="item">
                                      Designer
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="field">
                              <label style="font-weight:500;">Bio</label><textarea name="body" placeholder="Describe the member." rows="6" type="text"></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="comment" style="padding-left:1em;padding-top:1em;padding-bottom:1.5em;">
                          <div class="three fields">
                            <div class="field" style="width:96px;">
                              <label style="font-weight:500;">Portrait</label>
                              <div class="card card-form upload-image" style="height:96px;border-color:rgba(39, 41, 43, 0.15);">
                                <div class="card-image" style="position:relative;width:100%;height:100%;">
                                  <i class="icon ion-ios-plus" style="position:absolute;top:25%;left:28%;font-size:48px;color:rgba(39, 41, 43, 0.15);"></i>
                                </div>
                              </div>
                            </div>
                            <div class="field">
                              <div class="field">
                                <label style="font-weight:500;">Name</label><input name="name" placeholder="Enter your first name" type="text" />
                              </div>
                              <div class="field">
                                <label style="font-weight:500;">Role</label>
                                <div class="ui search dropdown simple selection" style="font-size:0.85em;">
                                  <input name="role" type="hidden" /><i class="dropdown icon"></i>
                                  <div class="default text">
                                    Select role
                                  </div>
                                  <div class="menu">
                                    <div class="item">
                                      Owner
                                    </div>
                                    <div class="item">
                                      Designer
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="field">
                              <label style="font-weight:500;">Bio</label><textarea name="body" placeholder="Describe the member." rows="6" type="text"></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="comment" style="padding-left:1em;padding-top:1em;padding-bottom:1.5em;">
                          <div class="three fields">
                            <div class="field" style="width:96px;">
                              <label style="font-weight:500;">Portrait</label>
                              <div class="card card-form upload-image" style="height:96px;border-color:rgba(39, 41, 43, 0.15);">
                                <div class="card-image" style="position:relative;width:100%;height:100%;">
                                  <i class="icon ion-ios-plus" style="position:absolute;top:25%;left:28%;font-size:48px;color:rgba(39, 41, 43, 0.15);"></i>
                                </div>
                              </div>
                            </div>
                            <div class="field">
                              <div class="field">
                                <label style="font-weight:500;">Name</label><input name="name" placeholder="Enter your first name" type="text" />
                              </div>
                              <div class="field">
                                <label style="font-weight:500;">Role</label>
                                <div class="ui search dropdown simple selection" style="font-size:0.85em;">
                                  <input name="role" type="hidden" /><i class="dropdown icon"></i>
                                  <div class="default text">
                                    Select role
                                  </div>
                                  <div class="menu">
                                    <div class="item">
                                      Owner
                                    </div>
                                    <div class="item">
                                      Designer
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="field">
                              <label style="font-weight:500;">Bio</label><textarea name="body" placeholder="Describe the member." rows="6" type="text"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                      <button class="light">Add member</button>
                    </div>
                  </div>
                  <div class="drilldown-card">
                    <div class="drilldown-content">
                      <label style="color:rgba(0,0,0,0.8);font-weight:500;font-size:14px;">Featured items</label>
                      <div class="comments">
                        <div class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-item-wrap" style="width:18%;">
                                <div class="card" style="box-shadow:none;border:none;">
                                  <a href="/shop-drilldown">
                                    <div class="card-image" style="border:none;height:90px;background-image:url('/images/photos/prev22.jpg');"></div>
                                  </a>
                                </div>
                              </div>
                              <div class="rating-comment" style="width:78%;">
                                <p class="price pull-right">
                                  ₱ 12963
                                </p>
                                <a href="/services-drilldown">
                                  <p style="font-size:1em;">
                                    Sint molestiae et voluptatem maiores
                                  </p>
                                </a>
                                <p style="color:rgba(97, 99, 103, 0.75);">
                                  Minus temporibus itaque placeat eos iusto maxime
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-item-wrap" style="width:18%;">
                                <div class="card" style="box-shadow:none;border:none;">
                                  <a href="/shop-drilldown">
                                    <div class="card-image" style="border:none;height:90px;background-image:url('/images/photos/prev05.jpg');"></div>
                                  </a>
                                </div>
                              </div>
                              <div class="rating-comment" style="width:78%;">
                                <p class="price pull-right">
                                  ₱ 4021
                                </p>
                                <a href="/services-drilldown">
                                  <p style="font-size:1em;">
                                    Dolor id explicabo assumenda velit
                                  </p>
                                </a>
                                <p style="color:rgba(97, 99, 103, 0.75);">
                                  Quaerat omnis eum ipsa et blanditiis sequi rem
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-item-wrap" style="width:18%;">
                                <div class="card" style="box-shadow:none;border:none;">
                                  <a href="/shop-drilldown">
                                    <div class="card-image" style="border:none;height:90px;background-image:url('/images/photos/prev02.jpg');"></div>
                                  </a>
                                </div>
                              </div>
                              <div class="rating-comment" style="width:78%;">
                                <p class="price pull-right">
                                  ₱ 7235
                                </p>
                                <a href="/services-drilldown">
                                  <p style="font-size:1em;">
                                    Illo et sed modi facere
                                  </p>
                                </a>
                                <p style="color:rgba(97, 99, 103, 0.75);">
                                  Est quo vel provident sapiente ipsam voluptatibus facilis in dolor ea et ut vel molestias
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <button class="light" style="margin-top:1em;">Add item</button>
                    </div>
                  </div>
                  <button class="call-to-action">Save</button>
                </form>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
