@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div class="seller-section simple">
                <div class="user seller">
                  <div class="user-thumb">
                    <img class="square" src="/images/forenheit.jpg" />
                  </div>
                  <div class="user-info">
                    <a class="name header" href="/seller-page">Forenheit Studio/Architecture</a>
                  </div>
                </div>
                <ul class="seller-actions">
                  <li>
                    <a class="seller-link" href="/user-profile">View profile</a>
                  </li>
                </ul>
              </div>
              <div id="seller-nav-unfixed">
                <div class="seller-section">
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-profile">Edit profile</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-profile-trust">Trust and verification</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-reviews">Reviews<span class="number">9</span></a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <section class="main no-top">
              <div class="drilldown-full">
                <div class="drilldown-tabs simple">
                  <nav>
                    <ul class="drilldown-tabs-nav">
                      <li>
                        <a class="selected" data-content="reviews-about-you" href="#0">Reviews about you</a>
                      </li>
                      <li>
                        <a data-content="reviews-you-made" href="#0">Reviews you made</a>
                      </li>
                    </ul>
                  </nav>
                  <ul class="drilldown-tabs-content">
                    <li class="selected" data-content="reviews-about-you">
                      <ul class="comments">
                        <li class="comment">
                          <div class="comment-image">
                            <img src="http://uifaces.com/faces/_twitter/antoniopratas_120.jpg" /><a href="/user-profile">Wendell</a>
                          </div>
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-comment">
                                <div class="rating">
                                  <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                                </div>
                                <p>
                                  Laborum accusamus veniam explicabo reprehenderit enim rerum. ut voluptates deserunt occaecati impedit. accusamus vel corrupti et facilis numquam minima. reiciendis at architecto culpa magni quia nam praesentium deserunt quo pariatur alias provident possimus dolore
                                </p>
                              </div>
                              <div class="rating-item-wrap">
                                <div class="rating rating-item pull-right">
                                  <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-1.jpg');"></div>
                                  <p>
                                    <a href="/shop-drilldown">Earum doloremque odio quod</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="comment">
                          <div class="comment-image">
                            <img src="http://uifaces.com/faces/_twitter/cameronmoll_120.jpg" /><a href="/user-profile">Randal</a>
                          </div>
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-comment">
                                <div class="rating">
                                  <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i>
                                </div>
                                <p>
                                  Velit dolorem id perspiciatis. eius ducimus voluptatum exercitationem fuga voluptatum blanditiis blanditiis dolor exercitationem molestiae consequatur ut. error dolor et aspernatur excepturi similique. rerum voluptas et molestiae deserunt repudiandae porro repellat consectetur et ut cumque saepe sunt officia. nihil magnam voluptatibus autem delectus laudantium cumque similique ullam repellat. rerum voluptatem iste architecto repellat delectus rerum ea cum deserunt
                                </p>
                              </div>
                              <div class="rating-item-wrap">
                                <div class="rating rating-item pull-right">
                                  <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-2.jpg');"></div>
                                  <p>
                                    <a href="/shop-drilldown">Tempora nisi sed modi</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="comment">
                          <div class="comment-image">
                            <img src="http://uifaces.com/faces/_twitter/iamlouisbullock_120.jpg" /><a href="/user-profile">Preston</a>
                          </div>
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-comment">
                                <div class="rating">
                                  <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
                                </div>
                                <p>
                                  Sit impedit et id cumque ab accusamus qui quisquam molestiae nihil quo soluta dolore quia. atque ad qui quo autem vel aliquam repellat nemo error qui et totam. odit illo totam aut accusantium et. nobis ad tempora est. quis non accusantium enim eaque et
                                </p>
                              </div>
                              <div class="rating-item-wrap">
                                <div class="rating rating-item pull-right">
                                  <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                                  <p>
                                    <a href="/shop-drilldown">Fuga et voluptas natus</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="comment">
                          <div class="comment-image">
                            <img src="http://uifaces.com/faces/_twitter/benefritz_120.jpg" /><a href="/user-profile">Omar</a>
                          </div>
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-comment">
                                <div class="rating">
                                  <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
                                </div>
                                <p>
                                  Eos provident aut nostrum dolorem vero vitae cumque nisi laboriosam animi voluptatibus omnis architecto. reiciendis quos non hic eos eius possimus dolores optio maiores consequatur aut quis velit nisi. provident est aut omnis consequatur maxime et ut delectus libero omnis dolorem sunt ut corporis. aut nisi perferendis quia vel eligendi et nisi nihil ratione doloremque nihil. delectus eum magnam odio vero quod dignissimos qui laborum ut eos quis molestias id. accusamus repellendus et necessitatibus ipsa et quo odio repellat et qui rerum enim
                                </p>
                              </div>
                              <div class="rating-item-wrap">
                                <div class="rating rating-item pull-right">
                                  <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-9.jpg');"></div>
                                  <p>
                                    <a href="/shop-drilldown">Voluptatem aliquid sunt aspernatur</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="comment">
                          <div class="comment-image">
                            <img src="http://uifaces.com/faces/_twitter/AngelZxxWingZ_120.jpg" /><a href="/user-profile">Fatima</a>
                          </div>
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-comment">
                                <div class="rating">
                                  <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
                                </div>
                                <p>
                                  Id harum nam minima assumenda et illum nemo est nihil fugit error nulla corrupti. in aut temporibus similique deserunt dolore suscipit doloribus impedit voluptatem voluptate. optio ut consequatur delectus adipisci pariatur esse ut nihil. expedita dolores molestias deleniti nam alias. ut amet qui inventore nemo consequuntur. a id aliquam autem et. nostrum omnis accusamus maiores placeat perferendis qui
                                </p>
                              </div>
                              <div class="rating-item-wrap">
                                <div class="rating rating-item pull-right">
                                  <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-11.jpg');"></div>
                                  <p>
                                    <a href="/shop-drilldown">Voluptatem aperiam aliquid consequuntur</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </li>
                    <li data-content="reviews-you-made">
                      <ul class="comments">
                        <li class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-comment">
                                <div class="rating">
                                  <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
                                </div>
                                <p>
                                  Et velit eveniet debitis. sunt aut alias consequatur illo et voluptate libero id in ratione officia labore eveniet aut. qui velit consequuntur qui. impedit fugit itaque et dicta ab nihil quia sunt. voluptate laboriosam voluptas voluptates excepturi beatae quidem quas consectetur. quis quaerat qui ipsam amet exercitationem vel illo similique amet quibusdam
                                </p>
                              </div>
                              <div class="rating-item-wrap">
                                <div class="rating rating-item pull-right">
                                  <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                                  <p>
                                    <a href="/shop-drilldown">Voluptas facilis et voluptas</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-comment">
                                <div class="rating">
                                  <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                                </div>
                                <p>
                                  Autem autem sed excepturi saepe dolorum expedita accusamus. et numquam mollitia maiores dolores labore dolores ut aut incidunt ex alias praesentium voluptatem. eligendi vero minima ipsam qui sunt velit nisi libero corporis delectus voluptas dolorum harum repudiandae. quaerat vel quas facere. et voluptas est et culpa reiciendis quia ex quia praesentium
                                </p>
                              </div>
                              <div class="rating-item-wrap">
                                <div class="rating rating-item pull-right">
                                  <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-10.jpg');"></div>
                                  <p>
                                    <a href="/shop-drilldown">Omnis temporibus et consequuntur</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-comment">
                                <div class="rating">
                                  <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                                </div>
                                <p>
                                  Saepe voluptas maxime rerum possimus sunt harum sunt et et. eum similique ut qui nam dolorum laboriosam dolore impedit dolor occaecati nihil nostrum. corrupti neque est assumenda animi doloremque quo voluptates alias culpa corporis sed
                                </p>
                              </div>
                              <div class="rating-item-wrap">
                                <div class="rating rating-item pull-right">
                                  <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-3.jpg');"></div>
                                  <p>
                                    <a href="/shop-drilldown">Voluptatem consequuntur in veritatis</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-comment">
                                <div class="rating">
                                  <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i>
                                </div>
                                <p>
                                  Facilis necessitatibus dolore quia totam officiis. ut eligendi vel velit. vitae voluptates consectetur dolorem omnis. sint autem in repellat quisquam veritatis ullam et ea aut sequi incidunt blanditiis. minima debitis recusandae est eius labore beatae maxime inventore quod
                                </p>
                              </div>
                              <div class="rating-item-wrap">
                                <div class="rating rating-item pull-right">
                                  <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-2.jpg');"></div>
                                  <p>
                                    <a href="/shop-drilldown">Natus ut fugit laboriosam</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-comment">
                                <div class="rating">
                                  <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
                                </div>
                                <p>
                                  Qui tempore veritatis sit minus aspernatur quaerat asperiores reiciendis rerum quisquam provident non. qui sunt ut officiis qui voluptate. maiores eum explicabo id et commodi
                                </p>
                              </div>
                              <div class="rating-item-wrap">
                                <div class="rating rating-item pull-right">
                                  <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-12.jpg');"></div>
                                  <p>
                                    <a href="/shop-drilldown">Omnis non facere doloribus</a>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
