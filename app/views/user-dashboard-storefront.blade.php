@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Info and Appearance</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-storefront">Edit storefront</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-storefront-about">About your shop</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-storefront-policies">Policies</a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Options</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="#">Storefront options</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Vacation Mode</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Web Analytics</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Close shop</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <section class="main no-top">
              <div class="drilldown-full">
                <h5 style="margin-bottom:1em;">
                  Edit your storefront
                </h5>
                <div class="drilldown-card">
                  <div class="drilldown-content">
                    <div class="ui form">
                      <form>
                        <div class="field">
                          <label style="font-weight:500;">Shop Name</label><input type="text" />
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Shop Title</label><input type="text" />
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Shop Banner</label>
                          <div class="card card-form upload-image" style="border-color:rgba(39, 41, 43, 0.15);">
                            <div class="card-image" style="width:100%;">
                              <p style="padding-top:0.5em;">
                                Click to upload
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Link accounts</label>
                          <div class="two fields">
                            <div class="field">
                              <p style="font-size:14px;">
                                Aperiam modi ut fuga est suscipit nisi. Modi assumenda tenetur saepe velit et corporis veritatis sit ea enim eaque est
                              </p>
                            </div>
                            <div class="field">
                              <button class="call-to-action" style="display:block;margin-bottom:0.5em;padding:0.75em;font-size:14px;background-color:#3B5998;">Facebook</button><button class="call-to-action" style="display:block;padding:0.75em;font-size:14px;background-color:#dd4b39;">Google</button>
                            </div>
                          </div>
                        </div>
                        <div class="field">
                          <label style="font-weight:500;">Shop Announcement</label><textarea name="body" placeholder="This will appear on your banner." rows="3" type="text"></textarea>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <button class="call-to-action">Save</button>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
