@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full">
          <div class="profile-subsection-nav">
            <div class="fixed-nav">
              <div class="seller-section simple">
                <div class="user seller">
                  <div class="user-thumb">
                    <img class="square" src="/images/seller-page-profile.jpg" />
                  </div>
                  <div class="user-info">
                    <a class="name header" href="/seller-page">Artery</a>
                    <p class="location">
                      Makati, PH
                    </p>
                  </div>
                </div>
              </div>
              <div id="seller-nav-unfixed">
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/seller-page-profile.jpg" />
                    </div>
                    <div class="user-info">
                      <a class="name header" href="/seller-page">Artery</a>
                      <p class="location">
                        Makati, PH
                      </p>
                    </div>
                  </div>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Sections</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/seller-page">All items<span class="number">31</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Kitchen<span class="number">5</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Bedroom<span class="number">23</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Master bedroom<span class="number">15</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Living<span class="number">50</span></a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page">Dining<span class="number">39</span></a>
                    </li>
                    <li class="sold-items">
                      <a class="section-nav-item" href="/seller-page">Sold items<span class="number">44</span></a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Info</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/seller-page-about">About</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-reviews">Reviews
                        <div class="rating">
                          <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i>
                        </div>
                      </a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-suggestions">Suggestions</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/seller-page-policies">Policies</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <div id="profile-cover-wrap">
              <div class="profile-cover" style="background-image:url('/images/seller-page-cover.jpg');"></div>
              <div class="profile-details">
                <div class="profile-info">
                  <div class="user">
                    <div class="user-info">
                      <p class="quote">
                        We search the world to give you the best in decor and lighting.
                      </p>
                    </div>
                  </div>
                  <div class="announcement">
                    <p>
                      Spell out your wishes! Artery provide 8/12/15/24 items for you. Turnaround time is 3-7 days. Your package will be delivered within 4 to 11 business days.
                    </p>
                  </div>
                </div>
                <div class="profile-actions">
                  <button class="light pull-right" style="margin-bottom:0.5em;"><span>Favorite</span><i class="icon ion-android-favorite"></i></button>
                </div>
              </div>
            </div>
            <div class="drilldown-full">
              <div class="drilldown-card">
                <section>
                  <h5 style="margin-bottom:1em;">
                    Reviews summary <span>(6)</span>
                  </h5>
                  <div class="rating-summary">
                    <div class="rating-summary-main">
                      <div class="rating rating-item rating-main">
                        <h5>
                          4 out of 5
                        </h5>
                        <span>Average reviews</span>
                      </div>
                    </div>
                    <div class="rating-summary-breakdown">
                      <div class="list-side-by-side">
                        <dl>
                          <dt>
                            Accuracy
                          </dt>
                          <dd>
                            <div class="rating">
                              <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i>
                            </div>
                          </dd>
                          <dt>
                            Communication
                          </dt>
                          <dd>
                            <div class="rating">
                              <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i>
                            </div>
                          </dd>
                          <dt>
                            Delivery
                          </dt>
                          <dd>
                            <div class="rating">
                              <i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
                            </div>
                          </dd>
                          <dt>
                            Value
                          </dt>
                          <dd>
                            <div class="rating">
                              <i class="icon ion-android-star active"></i><i class="icon ion-android-star active"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i><i class="icon ion-android-star"></i>
                            </div>
                          </dd>
                        </dl>
                      </div>
                    </div>
                  </div>
                </section>
                <section>
                  <h5 style="margin-bottom:1em;">
                    Reviews from customers <span>(3)</span>
                  </h5>
                  <ul class="comments">
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/mambows_120.jpg" /><a href="/user-profile">Samuel</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating-content">
                          <div class="rating-comment">
                            <p>
                              Repellat pariatur voluptatum cumque tempore. Fuga consequatur asperiores dignissimos. Asperiores voluptatem aut inventore qui praesentium. Modi soluta cupiditate occaecati velit et molestiae. Sit architecto dolorem animi aut soluta quas illo
                            </p>
                          </div>
                          <div class="rating-item-wrap">
                            <div class="rating rating-item pull-right">
                              <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-13.jpg');"></div>
                              <p>
                                <a href="/shop-drilldown">Dolorem quasi rem inventore</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/sajtoo_120.jpg" /><a href="/user-profile">Rafael</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating-content">
                          <div class="rating-comment">
                            <p>
                              Eius quibusdam quia totam optio ut et iure recusandae vel voluptas fugiat ab maxime. Expedita praesentium atque quibusdam eum sit aperiam animi qui nemo dolorem repudiandae eligendi. Expedita omnis nam suscipit dolor sint sit dolorum est libero voluptatem. Temporibus molestiae ut consequatur provident voluptatum iste tempore qui voluptatem. Dignissimos architecto id molestiae at quae facilis nihil dolorem
                            </p>
                          </div>
                          <div class="rating-item-wrap">
                            <div class="rating rating-item pull-right">
                              <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-10.jpg');"></div>
                              <p>
                                <a href="/shop-drilldown">Iste voluptatem vel ut</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/gabediaz_120.jpg" /><a href="/user-profile">Ashton</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating-content">
                          <div class="rating-comment">
                            <p>
                              Itaque nesciunt amet et dolorem quo id culpa non atque assumenda impedit sunt doloremque. Ipsam molestias ut rerum repellendus ipsum. Quibusdam voluptatem culpa alias fuga maiores sapiente. Odit laudantium cumque architecto ipsa est provident at molestias accusamus voluptatem rerum beatae laborum corporis. Id excepturi ea nostrum hic necessitatibus nobis est quae dolorem odit accusamus nobis sit et
                            </p>
                          </div>
                          <div class="rating-item-wrap">
                            <div class="rating rating-item pull-right">
                              <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-1.jpg');"></div>
                              <p>
                                <a href="/shop-drilldown">Alias dolor sed dolor</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </section>
                <section>
                  <h5 style="margin-bottom:1em;">
                    Reviews from PROs <span>(3)</span>
                  </h5>
                  <ul class="comments">
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/designer_dean_120.jpg" /><a href="/user-profile">Omar</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating-content">
                          <div class="rating-comment">
                            <p>
                              Quia reprehenderit quia voluptatem molestiae animi dolores quis totam. Ut facilis odit enim. Deleniti voluptatem suscipit fugiat molestiae vitae laboriosam fugiat omnis est voluptas voluptatum. Excepturi ipsum quos corporis quo rem nostrum mollitia. Omnis illum tempora atque rerum eaque et ab
                            </p>
                          </div>
                          <div class="rating-item-wrap">
                            <div class="rating rating-item pull-right">
                              <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-1.jpg');"></div>
                              <p>
                                <a href="/shop-drilldown">Optio consectetur ut asperiores</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/alek_djuric_120.jpg" /><a href="/user-profile">Ida</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating-content">
                          <div class="rating-comment">
                            <p>
                              Recusandae vitae et nisi sint repellendus repudiandae. Voluptatem ex voluptatem odio deserunt vero vel perspiciatis aliquid quod. Voluptates facere laborum nisi velit quae. Aliquam atque dignissimos id maiores quia ratione. Officia earum est quae
                            </p>
                          </div>
                          <div class="rating-item-wrap">
                            <div class="rating rating-item pull-right">
                              <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                              <p>
                                <a href="/shop-drilldown">Modi dolores ipsa at</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="comment">
                      <div class="comment-image">
                        <img src="http://uifaces.com/faces/_twitter/ripplemdk_120.jpg" /><a href="/user-profile">Carmen</a>
                      </div>
                      <div class="comment-content">
                        <div class="rating-content">
                          <div class="rating-comment">
                            <p>
                              Accusamus fugiat illo rerum eos quam. Dolorem iste enim totam libero excepturi. Et vitae vitae necessitatibus quidem ut vel at. Alias quas ut quis voluptatem incidunt velit est. Magni quis fuga et suscipit voluptas pariatur enim qui
                            </p>
                          </div>
                          <div class="rating-item-wrap">
                            <div class="rating rating-item pull-right">
                              <div class="rating-image" style="background-image:url('/images/seller-products/seller-product-5.jpg');"></div>
                              <p>
                                <a href="/shop-drilldown">Placeat doloremque rerum quia</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </section>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop

@section('scripts')
  @parent

  <script src="/javascripts/vendor/scrolltofixed.js" type="text/javascript"></script>
  <script type="text/javascript">
    $('#seller-nav').scrollToFixed({
      marginTop: 24,
      limit: $('.footer').offset().top - $('#seller-nav').outerHeight(true) - 96,
      preFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      },
      postFixed: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').removeClass('active');
          // $(this).removeClass('no-bg');
        });
      },
      preAbsolute: function() {
        $('#seller-nav .seller-section').not('.slide-up').each(function() {
          $('.slide-up').addClass('active');
          // $(this).addClass('no-bg');
        });
      }
    });
  </script>
@stop
