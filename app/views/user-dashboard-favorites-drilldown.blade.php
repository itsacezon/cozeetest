@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="profile-nav">
          <section>
            <ul>
              <li>
                <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
              </li>
              <li>
                <a href="/user-dashboard">Dashboard</a>
              </li>
              <li>
                <a href="/user-dashboard-inbox">Inbox</a>
              </li>
              <li>
                <a href="/user-dashboard-listings">Listings</a>
              </li>
              <li>
                <a class="active" href="/user-dashboard-albums">Albums</a>
              </li>
              <li>
                <a href="/user-dashboard-storefront">Storefront</a>
              </li>
              <li>
                <a href="/user-dashboard-profile">Profile</a>
              </li>
            </ul>
          </section>
        </div>
        <div class="fixed-nav">
          <div id="seller-nav-unfixed">
          </div>
          <div id="seller-nav">
            <div class="seller-section simple slide-up">
              <div class="user">
                <div class="user-thumb smaller">
                  <img src="/images/forenheit.jpg" />
                </div>
                <div class="user-info">
                  <ul>
                    <li>
                      <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                    </li>
                    <li>
                      <a class="location" href="/edit-profile">Edit profile</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="drilldown-full" style="padding-top:4em;">
          <h5 style="margin-bottom:1em;">
            Your favorite items
          </h5>
        </div>
        <div class="drilldown-full">
          <section class="main no-top">
            <div class="grid-4" data-columns="">
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-12.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Ipsam velit tenetur et dicta</span><span class="price pull-right">₱ 5320</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Dirk Carver</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-12.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Asperiores iure sequi molestias in</span><span class="price pull-right">₱ 1837</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Summer Abbott</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-2.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Debitis doloremque optio autem mollitia</span><span class="price pull-right">₱ 7789</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Margarita Monroe</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Omnis magnam non dolor totam</span><span class="price pull-right">₱ 897</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Harriet Alexander</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-5.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Sed quam quo unde non</span><span class="price pull-right">₱ 10687</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Yehudi Kennedy</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Deserunt dicta aut voluptatem est</span><span class="price pull-right">₱ 2767</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Earl Weiner</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-3.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Illo nobis deleniti illo sapiente</span><span class="price pull-right">₱ 7473</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Ana Wilkerson</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-1.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Vel dolores quo laudantium ducimus</span><span class="price pull-right">₱ 12802</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Carla Moon</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-6.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Dolorum aut provident sit et</span><span class="price pull-right">₱ 3128</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Ty Teague</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-3.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Optio fuga vero corrupti unde</span><span class="price pull-right">₱ 14176</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Agnes Hatcher</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-7.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Ab in velit debitis sed</span><span class="price pull-right">₱ 2963</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Omar Heath</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-0.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Sequi dignissimos vel laboriosam odit</span><span class="price pull-right">₱ 947</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Preston Barr</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-12.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Sequi voluptatibus illo et ut</span><span class="price pull-right">₱ 9079</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Leland Heath</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-11.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Laborum sequi recusandae maxime ipsam</span><span class="price pull-right">₱ 9601</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Kellie Bowman</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-0.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Corporis et optio enim dolorum</span><span class="price pull-right">₱ 10034</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Antonio Schroeder</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-9.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Pariatur voluptate hic consequatur aut</span><span class="price pull-right">₱ 5736</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Jamal Hill</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <a href="/shop-drilldown">
                  <div class="card-image card-small" style="background-image:url('/images/seller-products/seller-product-5.jpg');"></div>
                </a>
                <div class="card-details">
                  <div class="card-header">
                    <span class="cut-half">Voluptates quas voluptas dicta iusto</span><span class="price pull-right">₱ 8765</span>
                  </div>
                  <div class="card-author">
                    <a class="cut-half" href="/seller-page">Preston Petersen</a>
                  </div>
                  <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                    <div class="ui form">
                      <div class="field">
                        <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </section>
    </div>
  </div>
@stop
