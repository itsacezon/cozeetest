@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="profile-nav">
          <section>
            <ul>
              <li>
                <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
              </li>
              <li>
                <a href="/user-dashboard">Dashboard</a>
              </li>
              <li>
                <a href="/user-dashboard-inbox">Inbox</a>
              </li>
              <li>
                <a href="/user-dashboard-listings">Listings</a>
              </li>
              <li>
                <a class="active" href="/user-dashboard-albums">Albums</a>
              </li>
              <li>
                <a href="/user-dashboard-storefront">Storefront</a>
              </li>
              <li>
                <a href="/user-dashboard-profile">Profile</a>
              </li>
            </ul>
          </section>
        </div>
        <div class="fixed-nav">
          <div id="seller-nav-unfixed">
          </div>
          <div id="seller-nav">
          </div>
        </div>
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="drilldown-full" style="text-align:center;">
            <h4>
              My Album Name
            </h4>
            <a class="button light pull-right" href="/user-dashboard-albums-drilldown-list" style="margin-top:-1px;"><i class="icon ion-android-menu"></i></a><a class="button pewter pull-right" href="/user-dashboard-albums-drilldown" style="margin-right:0.5em;"><i class="icon ion-grid"></i></a><button class="light pull-left" type="button"><span>Most Popular</span><i class="icon ion-ios-arrow-down"></i></button>
          </div>
          <div class="drilldown-full">
            <section class="main">
              <div class="grid-4" data-columns="">
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/photos/prev49.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Accusamus occaecati sequi quidem quo
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Tomas Baker</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/products/prev37.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      <span class="cut-half">Ipsum optio dignissimos possimus qui</span><span class="price pull-right">₱ 1293</span>
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Caleb Monroe</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/photos/prev16.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Et dolores eius ut reiciendis
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Jamie Black</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/photos/prev21.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Dolore ut excepturi nihil est
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">George Singer</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/photos/prev36.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Deserunt consequatur dolorum hic voluptate
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Andres Sykes</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/photos/prev33.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Quia ut qui natus veniam
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Gordon Branch</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/products/prev20.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      <span class="cut-half">Totam distinctio ea cupiditate molestias</span><span class="price pull-right">₱ 14315</span>
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Katie Choi</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/products/prev35.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      <span class="cut-half">Fuga est repudiandae voluptas a</span><span class="price pull-right">₱ 957</span>
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Penny Monroe</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/products/prev28.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      <span class="cut-half">Velit aut voluptatem et rerum</span><span class="price pull-right">₱ 48</span>
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Hudson Walton</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/photos/prev28.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Odit corporis optio quia exercitationem
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Skyler Willis</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/photos/prev04.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Facere sint deserunt et aspernatur
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Kerry McLaughlin</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/products/prev10.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      <span class="cut-half">Voluptates nemo doloribus quia quibusdam</span><span class="price pull-right">₱ 11908</span>
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Lynette Rosenthal</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/photos/prev32.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Consectetur est ut asperiores ut
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Courtney Christensen</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/photos/prev05.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      Rerum et molestias et laborum
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Casey Puckett</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/photos/prev27.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      At consectetur molestias autem facilis
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Daryl Morse</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <a href="/shop-drilldown">
                    <div class="card-image card-small" style="background-image:url('/images/products/prev17.jpg');"></div>
                  </a>
                  <div class="card-details">
                    <div class="card-header">
                      <span class="cut-half">Perspiciatis cupiditate omnis delectus ipsum</span><span class="price pull-right">₱ 9005</span>
                    </div>
                    <div class="card-author">
                      <a href="/seller-page">Kaleigh Brady</a>
                    </div>
                    <div class="card-input" style="margin-top:0.75em;margin-bottom:0.5em;">
                      <div class="ui form">
                        <div class="field">
                          <textarea name="body" placeholder="Write your notes here" rows="3" type="text"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
