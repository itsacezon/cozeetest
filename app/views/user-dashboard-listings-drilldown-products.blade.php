@extends('layouts.master')

@section('content')
  <div id="content-wrap">
    <div class="panel">
      <section class="main">
        <div class="drilldown-full" style="padding-top:4em;">
          <div class="profile-subsection-nav">
            <div class="profile-nav">
              <section>
                <ul>
                  <li>
                    <a class="seller-name" href="/user-profile"><span style="margin-right:1em;">Forenheit Studio/Architecture</span><i class="icon ion-ios-arrow-right pull-right" style="padding-top:3px;"></i></a>
                  </li>
                  <li>
                    <a href="/user-dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-inbox">Inbox</a>
                  </li>
                  <li>
                    <a class="active" href="/user-dashboard-listings">Listings</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-albums">Albums</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-storefront">Storefront</a>
                  </li>
                  <li>
                    <a href="/user-dashboard-profile">Profile</a>
                  </li>
                </ul>
              </section>
            </div>
            <div class="fixed-nav">
              <div id="seller-nav-unfixed">
                <div class="seller-section highlighted">
                  <div class="seller-section-header">
                    <span>Edit listing</span>
                  </div>
                  <div class="seller-content">
                    <div class="user" style="padding:0 0.75em;padding-bottom:0.5em;">
                      <div class="user-thumb smaller">
                        <img class="square" src="/images/photos/prev12.jpg" />
                      </div>
                      <div class="user-info">
                        <a class="name" href="/services-drilldown" style="font-size:13px;">Expedita ullam quos quis unde</a>
                      </div>
                    </div>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-listings-drilldown">Description</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-listings-drilldown-photos">Photos</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="/user-dashboard-listings-drilldown-products">Products</a>
                    </li>
                  </ul>
                </div>
                <div class="seller-section">
                  <div class="seller-section-header">
                    <span>Help</span>
                  </div>
                  <ul class="seller-content">
                    <li>
                      <a class="section-nav-item" href="#">Eligendi non voluptatem</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Ut maiores animi</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Et officiis enim</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Rerum asperiores esse</a>
                    </li>
                    <li>
                      <a class="section-nav-item" href="#">Neque molestiae provident</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="seller-nav">
                <div class="seller-section simple slide-up">
                  <div class="user">
                    <div class="user-thumb smaller">
                      <img src="/images/forenheit.jpg" />
                    </div>
                    <div class="user-info">
                      <ul>
                        <li>
                          <a class="name header" href="/user-dashboard">Forenheit Studio/...</a>
                        </li>
                        <li>
                          <a class="location" href="/edit-profile">Edit profile</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="profile-subsection-content">
            <form action="/pro-page-my-services" method="POST">
              <section class="main no-top">
                <div class="drilldown-full">
                  <a class="button light pull-right" href="/user-dashboard-listings-add" style="margin-bottom:1.5em;">Add product</a>
                  <h5 class="pull-right" style="margin-top:0.15em;margin-right:1em;">
                    4 <span>products</span>
                  </h5>
                  <h5 style="margin-bottom:1em;">
                    Products
                  </h5>
                </div>
                <div class="drilldown-full">
                  <div class="drilldown-card">
                    <div class="drilldown-content">
                      <div class="comments">
                        <div class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-item-wrap" style="width:18%;">
                                <div class="card" style="box-shadow:none;border:none;">
                                  <a href="/shop-drilldown">
                                    <div class="card-image" style="border:none;height:90px;background-image:url('/images/photos/prev48.jpg');"></div>
                                  </a>
                                </div>
                              </div>
                              <div class="rating-comment" style="width:78%;">
                                <p class="price pull-right">
                                  ₱ 8807
                                </p>
                                <a href="/services-drilldown">
                                  <p style="font-size:1em;">
                                    Consequatur autem commodi commodi nihil
                                  </p>
                                </a>
                                <p style="color:rgba(97, 99, 103, 0.75);">
                                  Corrupti omnis eos repellat animi ut qui
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-item-wrap" style="width:18%;">
                                <div class="card" style="box-shadow:none;border:none;">
                                  <a href="/shop-drilldown">
                                    <div class="card-image" style="border:none;height:90px;background-image:url('/images/photos/prev01.jpg');"></div>
                                  </a>
                                </div>
                              </div>
                              <div class="rating-comment" style="width:78%;">
                                <p class="price pull-right">
                                  ₱ 10455
                                </p>
                                <a href="/services-drilldown">
                                  <p style="font-size:1em;">
                                    Nihil est quam sequi possimus
                                  </p>
                                </a>
                                <p style="color:rgba(97, 99, 103, 0.75);">
                                  Rerum dolore similique tempora repellendus culpa placeat voluptas omnis error veritatis in facilis ea quo
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="comment">
                          <div class="comment-content" style="padding-left:0;">
                            <div class="rating-content">
                              <div class="rating-item-wrap" style="width:18%;">
                                <div class="card" style="box-shadow:none;border:none;">
                                  <a href="/shop-drilldown">
                                    <div class="card-image" style="border:none;height:90px;background-image:url('/images/photos/prev08.jpg');"></div>
                                  </a>
                                </div>
                              </div>
                              <div class="rating-comment" style="width:78%;">
                                <p class="price pull-right">
                                  ₱ 9918
                                </p>
                                <a href="/services-drilldown">
                                  <p style="font-size:1em;">
                                    Qui possimus ullam porro aliquid
                                  </p>
                                </a>
                                <p style="color:rgba(97, 99, 103, 0.75);">
                                  Fugit et repudiandae excepturi
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </form>
          </div>
        </div>
      </section>
    </div>
  </div>
@stop
